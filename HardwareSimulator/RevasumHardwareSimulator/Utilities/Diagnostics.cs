﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevasumHardwareSimulator.Utilities
{
    /// <summary>
    /// Container class that contains driver diagnostics for the ADS Client
    /// </summary>
    public class Diagnostics
    {
        public long MaxListSize = 1000000;

        private object m_DiagnosticsLock = new object();

        private Stopwatch m_Stopwatch = new Stopwatch();

        private Queue<long> m_TimeQueue = new Queue<long>();

        public long MaxTime { get; private set; }

        public long ErrorCount { get; set; }

        public long RetryCount { get; set; }

        public long UpdateCount { get; set; }

        /// <summary>
        /// Starts the stopwatch timer
        /// </summary>
        public void StartRecording()
        {
            lock (m_DiagnosticsLock)
            {
                m_Stopwatch.Restart();
            }
        }

        /// <summary>
        /// Stops the stopwatch timer and updates the diagnostics information
        /// </summary>
        public void StopRecording()
        {
            lock (m_DiagnosticsLock)
            {
                m_Stopwatch.Stop();
            }

            UpdateDiagnostics();

        }

        /// <summary>
        /// Updates the diagnostics information
        /// </summary>
        private void UpdateDiagnostics()
        {
            lock (m_DiagnosticsLock)
            {
                MaxTime = Math.Max(MaxTime, m_Stopwatch.ElapsedMilliseconds);

                while (m_TimeQueue.Count > (MaxListSize - 1))
                {
                    // Remove the oldest values
                    m_TimeQueue.Dequeue();
                }

                m_TimeQueue.Enqueue(m_Stopwatch.ElapsedMilliseconds);
            }
        }

        /// <summary>
        /// Retrieves the average time in milliseconds
        /// </summary>
        /// <returns></returns>
        public long GetAverageInMilliseconds()
        {
            double average = 0;

            lock (m_DiagnosticsLock)
            {
                average = (m_TimeQueue?.Count == 0) ? 0 : m_TimeQueue.Average();
            }

            return (long)average;
        }

        /// <summary>
        /// Retrieves the number of time counts in the queue
        /// </summary>
        /// <returns></returns>
        public long GetNumberOfTimeCounts()
        {
            long count = 0;

            lock (m_DiagnosticsLock)
            {
                count = m_TimeQueue.Count;
            }

            return count;
        }

    }
}
