﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;

//using RevasumHardwareSimulator.Model;
//using RevasumHardwareSimulator.Dialogs;
//using RevasumHardwareSimulator.mvp_Models;

using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        ApplicationPresenter m_presenter;
        public MainWindow()
        {

            int[] m_array = new int[20];

            InitializeComponent();

            //using (new RevasumHardwareSimulator.Global.WaitCursor())
            //{
            //    System.Threading.Thread.Sleep(2000);

            //}

            string file = RevasumHardwareSimulator.Global.FileManager.GetPathForBitmap("John");
            m_presenter = new ApplicationPresenter(this);
            DataContext = m_presenter;



            //RevasumHardwareSimulator.IOPoints.IOPoint_AxisDataPLC2NC axisData = new IOPoints.IOPoint_AxisDataPLC2NC(null);
            //axisData.Test();



            //RevasumHardwareSimulator.Global.MemoryLayoutNC2PLC n2p = new Global.MemoryLayoutNC2PLC { };

            //int size2a = System.Runtime.InteropServices.Marshal.SizeOf(n2p);


        }

        private void WriteToArray(int[] anArray, int pos, int value)
        {
            anArray[pos] = value;

        }

        public void AddTab<T>(PresenterBase<T> presenter)
        {
        }

        public void RemoveTab<T>(PresenterBase<T> presenter)
        {
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            m_presenter.ClosingApplication();
            //e.Cancel = RevasumHardwareSimulator.UserControls.AssociationTab.Current.ClosingApplication();
            //RevasumHardwareSimulator.UserControls.MonitorTab.Current.ClosingApplication();
        }

        private string ChooseFileToSave(string ProposedName)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = ProposedName;
            dlg.DefaultExt = ".XML";
            dlg.Filter = "XML Files (.xml)|*.xml";
            string baseDir = AppDomain.CurrentDomain.BaseDirectory + "Configuration";
            System.IO.Directory.CreateDirectory(baseDir);
            dlg.InitialDirectory = baseDir;
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
                return dlg.FileName;
            else
                return "";
        }

        private string ChooseFileToLoad(string ProposedName)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.FileName = ProposedName;
            dlg.DefaultExt = ".XML";
            dlg.Filter = "XML Files (.xml)|*.xml";
            string baseDir = AppDomain.CurrentDomain.BaseDirectory + "Configuration";
            System.IO.Directory.CreateDirectory(baseDir);
            dlg.InitialDirectory = baseDir;
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
                return dlg.FileName;
            else
                return "";
        }


    }
}
