﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;


namespace RevasumHardwareSimulator.Global
{
    public class WaitCursor : IDisposable
    {

        private Cursor m_previousCursor;

        public WaitCursor()
        {
            m_previousCursor = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
        }

        public void Dispose()
        {
            Mouse.OverrideCursor = m_previousCursor;
        }

        // usage

        //using (new RevasumHardwareSimulator.Global.WaitCursor())
        //{
        //    System.Threading.Thread.Sleep(2000);

        //}


    }
}
