﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using System.Runtime.InteropServices;

namespace RevasumHardwareSimulator.Global
{

    // --------------------------------------------------------------------------------------------------
   
    #region Memory Layout

    // --------------------------------------------------------------------------------------------------

    //[StructLayout(LayoutKind.Sequential, Pack = 0)]
    //public class MemoryLayoutPLC2NC
    //{

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 ControlDWord;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 Override;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 AxisModeRequest;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 AxisModeDWord;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double AxisModeLReal;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double PositionCorrection;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ExtSetPos;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ExtSetVelo;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ExtSetAcc;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 ExtSetDirection;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 _reserved1;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ExtControllerOutput;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double GearRatio1;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double GearRatio2;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double GearRatio3;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double GearRatio4;

    //    [MarshalAs(UnmanagedType.I1)]
    //    public bool MapState;

    //    [MarshalAs(UnmanagedType.I1)]
    //    public byte PlcCycleControl;

    //    [MarshalAs(UnmanagedType.I1)]
    //    public byte PlcCycleCount;

    //    // NOTE: the alternative way to initialize the array would be to define the size (see commented code)
    //    //      , but this would require us to compile as unsafe.  The drawback of our used solution is that the arrays
    //    //      will not be initialized, but since they are only fillers ... who cares?

    //    //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 21, ArraySubType = UnmanagedType.U1)]
    //    //public unsafe fixed byte _reserved2[21];

    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 21, ArraySubType = UnmanagedType.U1)]
    //    public byte[] _reserved2;

    //}

    // --------------------------------------------------------------------------------------------------

    //[StructLayout(LayoutKind.Sequential, Pack = 0)]
    //public class MemoryLayoutNC2PLC
    //{

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 StateDWord;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 ErrorCode;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 AxisState;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 AxisModeConfirmation;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 HomingState;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 CoupleState;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 SvbEntries;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 SafEntries;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 AxisId;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 OpModeDWord;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ActPos;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ModuloActPos;

    //    [MarshalAs(UnmanagedType.U2)]
    //    public UInt16 ActiveConntrolLoopIndex;

    //    [MarshalAs(UnmanagedType.U2)]
    //    public UInt16 ConntrolLoopIndex;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 ModuloActTurns;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ActVelo;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double PosDiff;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double SetPos;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double SetVelo;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double SetAcc;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double TargetPos;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ModuloSetPos;

    //    [MarshalAs(UnmanagedType.I4)]
    //    public Int32 ModuloSetTurns;

    //    [MarshalAs(UnmanagedType.U2)]
    //    public UInt16 CmdNo;

    //    [MarshalAs(UnmanagedType.U2)]
    //    public UInt16 CmdState;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double SetJerk;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double SetTorque;

    //    [MarshalAs(UnmanagedType.R8)]
    //    public Double ActTorque;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 StateDWord2;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 StateDWord3;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 TouchProbeState;

    //    [MarshalAs(UnmanagedType.U4)]
    //    public UInt32 TouchProbeCounter;

    //    // The ADS data type USINTARR8 is really an array of 8 bytes. We packing it as one 8 byte unsigned ... hope it works
    //    //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8, ArraySubType = UnmanagedType.U8)]
    //    //public byte[] CamCouplingState;
    //    //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8, ArraySubType = UnmanagedType.U1)]
    //    //public byte[] CamCouplingTableID;

    //    [MarshalAs(UnmanagedType.U8)]
    //    public ulong CamCouplingState;

    //    [MarshalAs(UnmanagedType.U8)]
    //    public ulong CamCouplingTableID;

    //    // This array will not be initializd - see the comment in MemoryLayoutPLC2NC class. 
    //    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 72, ArraySubType = UnmanagedType.U1)]
    //    public byte[] _reserved1;

    //}

    // --------------------------------------------------------------------------------------------------

    #endregion

    // --------------------------------------------------------------------------------------------------

    public delegate void WriteValue<T>(T aValue);
    //public delegate T ReadValue<T>(RevasumHardwareSimulator.mvp_Models.IOPoints.MemoryLayoutPLC2NC memory);
    public delegate T CaptureValue<T>(RevasumHardwareSimulator.mvp_Models.IOPoints.MemoryLayoutNC2PLC memory);

    //public class AxisStructureDataItem<T>
    //{
    //    string m_name;
    //    string m_qualifiedName;
    //    WriteValue<T> m_writeDelegate;
    //    T m_value;

    //    public AxisStructureDataItem(string qualifiedName, string name,  WriteValue<T> aDelegate)
    //    {
    //        m_name = name;
    //        m_qualifiedName = qualifiedName + "." + name;
    //        //m_value = initialValue;
    //        m_writeDelegate = aDelegate;
    //    }

    //    public void WriteValue(T aValue)
    //    {
    //        m_value = aValue;
    //        m_writeDelegate(m_value);
    //    }
    //}


    #region Unused

    // A cool idea, but cannot use

    //public class AxisStructureDataItem
    //{
    //    string m_name;
    //    string m_qualifiedName;
    //    RevasumDataTypes m_revasumDataType;
    //    int m_position;
    //    int m_size;

    //    public AxisStructureDataItem(string ownerName, string name, RevasumDataTypes dataType, int position, int size)
    //    {
    //        m_name = name;
    //        m_qualifiedName = ownerName + "." + name;
    //        m_revasumDataType = dataType;
    //        m_position = position;
    //        m_size = size;
    //    }
    //}


    //public class AxisStructure
    //{
    //    static Dictionary<RevasumDataTypes, int> s_axisDataSizes;
    //    protected List<AxisStructureDataItem> m_items = new List<AxisStructureDataItem>();
    //    string m_name;
    //    int m_usedBytes = 0;


    //    static AxisStructure()
    //    {
    //        s_axisDataSizes = new Dictionary<RevasumDataTypes, int>();
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_UDINT, 4);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_DINT, 4);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_LREAL, 8);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_DWORD, 4);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_UINT, 2);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_USINTARR8, 8);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisInput_Filler72, 72);

    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_UDINT, 4);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_DWORD, 4);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_LREAL, 8);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_BYTE, 1);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_BOOL, 1);
    //        s_axisDataSizes.Add(RevasumDataTypes.AxisOutput_Filler21, 21);

    //    }

    //    public AxisStructure(string name)
    //    {
    //        m_name = name;
    //    }

    //    public int UsedBytes
    //    {
    //        get { return m_usedBytes; }
    //    }

    //    protected void AddItem(string itemName, RevasumDataTypes dataType)
    //    {
    //        int theSize;
    //        if (s_axisDataSizes.TryGetValue(dataType, out theSize))
    //        {
    //            m_items.Add(new AxisStructureDataItem(m_name, itemName, dataType, m_usedBytes, theSize));
    //            m_usedBytes += theSize;
    //        }
    //    }
    //}


    //public class AxisNC2PLC : AxisStructure
    //{

    //    public AxisNC2PLC(string name)
    //        : base(name)
    //    {
    //        AddItem("StateDWord", RevasumDataTypes.AxisInput_DWORD);
    //        AddItem("ErrorCode", RevasumDataTypes.AxisInput_UDINT);
    //        AddItem("AxisState", RevasumDataTypes.AxisInput_UDINT);                         // Axis moving state
    //        AddItem("AxisModeConfirmation", RevasumDataTypes.AxisInput_UDINT);
    //        AddItem("HomingState", RevasumDataTypes.AxisInput_UDINT);
    //        AddItem("CoupleState", RevasumDataTypes.AxisInput_UDINT);
    //        AddItem("SvbEntries", RevasumDataTypes.AxisInput_UDINT);                        // Set Preparation Task
    //        AddItem("SafEntries", RevasumDataTypes.AxisInput_UDINT);                        // Set Execution Task
    //        AddItem("AxisId", RevasumDataTypes.AxisInput_UDINT);

    //        AddItem("OpModeDWord", RevasumDataTypes.AxisInput_DWORD);

    //        AddItem("ActPos", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("ModuloActPos", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("ActiveControlLoopIndex", RevasumDataTypes.AxisInput_UINT);
    //        AddItem("ControlLoopIndex", RevasumDataTypes.AxisInput_UINT);
    //        AddItem("ModuloActTurns", RevasumDataTypes.AxisInput_DINT);
    //        AddItem("ActVelo", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("PosDiff", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("SetPos", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("SetVelo", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("SetAcc", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("TargetPos", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("ModuloSetPos", RevasumDataTypes.AxisInput_LREAL);

    //        AddItem("ModuloSetTurns", RevasumDataTypes.AxisInput_DINT);
    //        AddItem("CmdNo", RevasumDataTypes.AxisInput_UINT);
    //        AddItem("CmdState", RevasumDataTypes.AxisInput_UINT);
    //        AddItem("SetJerk", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("SetTorque", RevasumDataTypes.AxisInput_LREAL);
    //        AddItem("ActTorque", RevasumDataTypes.AxisInput_LREAL);

    //        AddItem("StateDWord2", RevasumDataTypes.AxisInput_DWORD);
    //        AddItem("StateDWord3", RevasumDataTypes.AxisInput_DWORD);
    //        AddItem("TouchProbeState", RevasumDataTypes.AxisInput_DWORD);
    //        AddItem("TouchProbeCounter", RevasumDataTypes.AxisInput_DWORD);

    //        AddItem("CamCouplingState", RevasumDataTypes.AxisInput_USINTARR8);
    //        AddItem("CamCouplingTableID", RevasumDataTypes.AxisInput_USINTARR8);
    //        AddItem("_reserved1", RevasumDataTypes.AxisInput_Filler72);
    //    }

    //}

    //public class AxisPLC2NC : AxisStructure
    //{

    //    public AxisPLC2NC(string name)
    //        : base(name)
    //    {
    //        AddItem("ControlDWord", RevasumDataTypes.AxisOutput_DWORD);
    //        AddItem("Override", RevasumDataTypes.AxisOutput_UDINT);                                     // Velocity Override
    //        AddItem("AxisMoveRequest", RevasumDataTypes.AxisOutput_UDINT);
    //        AddItem("AxisModeDWord", RevasumDataTypes.AxisOutput_UDINT);                                // Optional mode parameter
    //        AddItem("AxisModeLReal", RevasumDataTypes.AxisOutput_LREAL);                                // Optional mode parameter
    //        AddItem("PositionCorrection", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("ExtSetPos", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("ExtSetVelo", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("ExtSetAcc", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("ExtSetDirection", RevasumDataTypes.AxisOutput_UDINT);

    //        AddItem("_reserved1", RevasumDataTypes.AxisOutput_UDINT);

    //        AddItem("ExtControllerOutput", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("GearRatio1", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("GearRatio2", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("GearRatio3", RevasumDataTypes.AxisOutput_LREAL);
    //        AddItem("GearRatio4", RevasumDataTypes.AxisOutput_LREAL);

    //        AddItem("MapState", RevasumDataTypes.AxisOutput_BOOL);
    //        AddItem("PlcCycleControl", RevasumDataTypes.AxisOutput_BYTE);
    //        AddItem("PlcCycleCount", RevasumDataTypes.AxisOutput_BYTE);

    //        AddItem("_reserved2", RevasumDataTypes.AxisOutput_Filler21);

    //    }
    //}


    #endregion

}
