﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace RevasumHardwareSimulator.Global
{
    public class FileManager
    {
 
        static readonly string s_bitmapFolder;
        static readonly string s_configurationsFolder;
        static readonly string s_xmlConfigurationFolder;

        // --------------------------------------------------------------------------------------------------

        static FileManager()
        {
            DirectoryInfo di = Directory.GetParent(Directory.GetCurrentDirectory()).Parent;

            //s_bitmapFolder = sub.FullName + Path.DirectorySeparatorChar;
            
            // using this so the images are stored and loaded at the beginning and each XAML doesn't need to go out to the file system
            s_bitmapFolder = @"pack://application:,,,/RevasumHardwareSimulator;component/Bitmaps/";

            s_configurationsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Configurations");

            Directory.CreateDirectory(s_configurationsFolder);

            s_xmlConfigurationFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "XMLConfigurations");

            Directory.CreateDirectory(s_xmlConfigurationFolder);

        }

        // --------------------------------------------------------------------------------------------------

        public static string GetPathForBitmap(string aBitmap)
        {
            return s_bitmapFolder + aBitmap;
        }

        // --------------------------------------------------------------------------------------------------

        public static string GetPathForText(string aText)
        {
            return @"/not being used/" + aText;
        }

        // --------------------------------------------------------------------------------------------------

        public static string ConfigurationFilesFolderName
        {
            get { return s_configurationsFolder; }
        }

        // --------------------------------------------------------------------------------------------------

        public static string XMLConfigurationFilesFolderName
        {
            get { return s_xmlConfigurationFolder; }
        }

        // --------------------------------------------------------------------------------------------------


    }
}
