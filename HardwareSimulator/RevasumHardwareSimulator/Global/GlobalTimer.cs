﻿using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Threading;

namespace RevasumHardwareSimulator.Global
{

    public delegate void TimerTickDelegate(long tickNumber);

    public class GlobalTimer
    {
        private List<TimerTickDelegate> m_delegates = new List<TimerTickDelegate>();
        private long m_counter = 0;
        private DispatcherTimer m_periodicTimer;
        private DateTime m_startDateTime;
        private readonly int m_pollFrequency = 100;


        private readonly int m_pollsWithin1Sec;
        private int m_autoCorrectionCounter = 0;

        private static object _lock = new object();
        private static GlobalTimer _singleton;


        /// <summary>
        ///     Use this global instance
        /// </summary>
        public static GlobalTimer Instance
        {
            get
            {
                if (_singleton == null)
                {
                    _singleton = new GlobalTimer();
                }

                return _singleton;
            }
        }

        /// <summary>
        ///     I have no idea what this is used for
        /// </summary>
        public bool PerformAutoCorrection
        {
            set
            {
                m_autoCorrection = value; 
            }
        }
        private bool m_autoCorrection = true;

        /// <summary>
        ///     Read the current global timer polling frequency
        /// </summary>
        public double TimerPeriod
        {
            get
            {
                return m_pollFrequency * SpeedUpFactor;
            }
        }

        /// <summary>
        ///     Return the total number of behaviors
        /// </summary>
        public int NumberOfSubscribers
        {
            get 
            {
                int result = 0;

                lock (_lock)
                {
                    result = m_delegates.Count;
                }

                return result; 
            }
        }

        /// <summary>
        ///     Acceleration factor for simulation timer
        /// </summary>
        public double SpeedUpFactor
        {
            get
            { 
                return m_speedUpFactor; 
            }
            set
            {
                m_speedUpFactor = value;

                PerformAutoCorrection = m_speedUpFactor == 1;
            }
        }
        private double m_speedUpFactor = 1;

        /// <summary>
        ///     private ctor to protect against external instantiation, USE INSTANCE property instead
        /// </summary>
        private GlobalTimer()
        {
            m_pollsWithin1Sec = 1000 / m_pollFrequency;

            m_periodicTimer = new DispatcherTimer();
            m_periodicTimer.Tick += new EventHandler(PeriodicTImerTickWithAutoCorrection);
            m_periodicTimer.Interval = new TimeSpan(0, 0, 0, 0, m_pollFrequency);

            //m_periodicTimer.Start();
        }

        /// <summary>
        ///     Start the timer
        /// </summary>
        private void StartTimer()
        {
            //Logger.PurgeLogs();

            m_counter = 0;
            m_startDateTime = DateTime.Now;
            m_periodicTimer.Start();
        }

        /// <summary>
        ///     Stop the timer
        /// </summary>
        private void StopTimer()
        {
            m_periodicTimer.Stop();
            
            // deregister all delegates
            lock (_lock)
            {
                m_delegates.Clear();
            }
        }

        /// <summary>
        ///     The Dispatch TImer is not very accurate and this method will be auto-correcting every 1 sec. 
        ///     Care must be taken NOT to auto-correct when we are speeding up the simulation
        /// </summary>
        private void PeriodicTImerTickWithAutoCorrection(object sender, EventArgs e)
        {
            //Logger.LogMessage(nameof(GlobalTimer), nameof(PeriodicTImerTickWithAutoCorrection), $"Timer Fired");

            m_counter += (long)Math.Round(m_pollFrequency * m_speedUpFactor);
            m_autoCorrectionCounter++;

            // perform time correction every second, only autocorrect if we are trying to run at 1x
            if (m_autoCorrection && (m_autoCorrectionCounter == m_pollsWithin1Sec))  
            {
                long actualMSec = (long)(DateTime.Now - m_startDateTime).TotalMilliseconds;

                m_counter = actualMSec;
                m_autoCorrectionCounter = 0;
            }

            lock (_lock)
            {
                foreach (TimerTickDelegate aDelegate in m_delegates)
                {
                    //Type type = aDelegate.Target.GetType();

                    //if (aDelegate.Target is Behavior_Script scriptTarget) 
                    //{
                    //    Logger.LogMessage(nameof(GlobalTimer), nameof(PeriodicTImerTickWithAutoCorrection), $"BehaviorType: Script; Name: {scriptTarget.Name}");
                    //}
                    //else if (aDelegate.Target is Behavior_AND andTarget)
                    //{
                    //    Logger.LogMessage(nameof(GlobalTimer), nameof(PeriodicTImerTickWithAutoCorrection), $"BehaviorType: And; Name: {andTarget.Name}");
                    //}
                    //else if (aDelegate.Target is Behavior_Script)
                    //{
                    //    Logger.LogMessage(nameof(GlobalTimer), nameof(PeriodicTImerTickWithAutoCorrection), $"BehaviorType: Script; Name: ");
                    //}
                    //else 
                    //{
                    //    Logger.LogMessage(nameof(GlobalTimer), nameof(PeriodicTImerTickWithAutoCorrection), $"BehaviorType: Script; Name: ");
                    //}

                    aDelegate(m_counter);
                }
            }
        }




        /// <summary>
        ///     Register a new behavior delegate with the global timer
        /// </summary>
        /// <param name="aDelegate"></param>
        public void Register(TimerTickDelegate aDelegate)
        {
            lock (_lock)
            {
                m_delegates.Add(aDelegate);
            }
        }

        public void OnLine()
        {
            StartTimer();
        }

        public void OffLine()
        {
            StopTimer();
        }
    }
}

