﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;

namespace XMLSupport
{

    public class XMLNode
    {
        // ------------------------------------------------------------------
        #region construction
        // ------------------------------------------------------------------
        private readonly string m_tag;
        private readonly Dictionary<string, string> m_attributes = new Dictionary<string, string>();
        private readonly List<XMLNode> m_nodes = new List<XMLNode>();
        // ------------------------------------------------------------------
        private XMLNode(string aName, XmlTextReader reader)
        {
            m_tag = aName;
            CreateFromFile(reader);
        }
        // ------------------------------------------------------------------
        private XMLNode(string aName)
        {
            m_tag = aName;
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
        #region Creation
        // ------------------------------------------------------------------
        public static XMLNode FromFile(string pathName)
        {
            XmlTextReader reader = new XmlTextReader(pathName)
            {
                WhitespaceHandling = WhitespaceHandling.None
            };

            while (reader.NodeType != XmlNodeType.Element)
            {
                reader.Read();
            }

            XMLNode aNode = new XMLNode(reader.Name, reader);
            reader.Close();
            return aNode;
        }
        // ------------------------------------------------------------------
        public static XMLNode Empty(string tagName)
        {
            return new XMLNode(tagName);
        }
        // ------------------------------------------------------------------
        //public static XMLNode FromObject(string tagName, object anObject)
        //{
        //    XMLNode aNode = new XMLNode(tagName);
        //    //aNode.InitializeFromObject(anObject);
        //    return aNode;
        //}
        // ------------------------------------------------------------------
        public static XMLNode CopyFrom(XMLNode sysConfig)
        {
            XMLNode aNode = Empty(sysConfig.Tag);
            aNode.CopyFromSysConfig(sysConfig);
            return aNode;
        }
        // ------------------------------------------------------------------
        private XMLNode CopyFromSysConfig(XMLNode sysConfig)
        {
            foreach (string aKey in sysConfig.Attributes)
            {
                AddAttribute(aKey, sysConfig.AttributeAt(aKey));
            }

            //foreach (KeyValuePair<string, IXMLObject> kvp in sysConfig.m_objects)
            //    AddObject(kvp.Key, kvp.Value);

            foreach (XMLNode aNode in sysConfig.Nodes())
            {
                XMLNode newNode = Empty(aNode.Tag);
                newNode.CopyFromSysConfig(aNode);
                m_nodes.Add(newNode);
            }

            return this;
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
        #region printing
        // ------------------------------------------------------------------
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("[" + m_tag + "]");
            foreach (string aKey in m_attributes.Keys)
            {
                m_attributes.TryGetValue(aKey, out string val);
                sb.Append(" " + aKey + "=\"" + val + "\"");
            }
            return sb.ToString();
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
        #region Parsing
        // ------------------------------------------------------------------
        private void CreateFromFile(XmlTextReader reader)
        {
            bool empty = reader.IsEmptyElement;
            ReadAttributes(reader);
            if (reader.NodeType == XmlNodeType.Text)
            {
                reader.Read();
            }

            if (!empty)
            {
                ReadChildren(reader);
            }
        }
        // ------------------------------------------------------------------
        private void ReadAttributes(XmlTextReader reader)
        {
            if (reader.MoveToFirstAttribute())
            {
                m_attributes.Add(reader.Name, reader.Value);
                while (reader.MoveToNextAttribute())
                {
                    m_attributes.Add(reader.Name, reader.Value);
                }
            }
            reader.Read();
        }
        // ------------------------------------------------------------------
        private void ReadChildren(XmlTextReader reader)
        {
            XMLNode aNode;
            while (reader.NodeType != XmlNodeType.EndElement)
            {
                while (reader.NodeType != XmlNodeType.Element && reader.NodeType != XmlNodeType.EndElement)
                {
                    reader.Read();
                }
                if (reader.NodeType == XmlNodeType.Element)
                {
                    aNode = new XMLNode(reader.Name, reader);
                    m_nodes.Add(aNode);
                }
            }
            reader.Read();
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
        #region accessing
        // ------------------------------------------------------------------
        public string AttributeAt(string aKey)
        {
            if (!m_attributes.TryGetValue(aKey, out string aValue))
                return "";
            return aValue;
        }
        //// ------------------------------------------------------------------
        //public IXMLObject ObjectAt(string aKey)
        //{
        //    IXMLObject aValue;
        //    if (!m_objects.TryGetValue(aKey, out aValue))
        //        return null;
        //    return aValue;
        //}
        // ------------------------------------------------------------------
        public XMLNode NodeAt(string aKey)
        {
            foreach (XMLNode aNode in m_nodes)
                if (aNode.Tag == aKey)
                    return aNode;
            return null;
        }
        // ------------------------------------------------------------------
        public void ReplaceAttribute(string aKey, string aValue)
        {
            if (m_attributes.ContainsKey(aKey))
                m_attributes.Remove(aKey);
            m_attributes.Add(aKey, aValue);
        }
        // ------------------------------------------------------------------
        public void RemoveAttribute(string aKey)
        {
            if (m_attributes.ContainsKey(aKey))
                m_attributes.Remove(aKey);
        }
        // ------------------------------------------------------------------
        public void AddAttribute(string aKey, string aValue)
        {
            m_attributes.Add(aKey, aValue);
        }
        //// ------------------------------------------------------------------
        //public void AddObject(string aKey, IXMLObject anObject)
        //{
        //    m_objects.Add(aKey, anObject);
        //}
        //// ------------------------------------------------------------------
        //public void ReplaceObject(string aKey, IXMLObject anObject)
        //{
        //    if (m_objects.ContainsKey(aKey))
        //        m_objects.Remove(aKey);
        //    m_objects.Add(aKey, anObject);
        //}
        // ------------------------------------------------------------------
        public IEnumerable<XMLNode> Nodes()
        {
            foreach (XMLNode aNode in m_nodes)
                if (aNode.AttributeAt("Exists").ToUpper() != "FALSE")
                    yield return aNode;
        }
        //// ------------------------------------------------------------------
        //public void RemoveNode(Aix354XMLNode aNode)
        //{
        //    m_nodes.Remove(aNode);
        //}
        // ------------------------------------------------------------------
        public bool HasAttributes()
        {
            return m_attributes.Count > 0;
        }
        // ------------------------------------------------------------------
        public Dictionary<string, string>.KeyCollection Attributes
        {
            get { return m_attributes.Keys; }
        }
        // ------------------------------------------------------------------
        public bool HasNodes()
        {
            return m_nodes.Count > 0;
        }
        // ------------------------------------------------------------------
        public string Tag
        {
            get { return m_tag; }
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
        #region Writing
        // ------------------------------------------------------------------
        public void ToFile(string FilePath)
        {
            using (StreamWriter writer = new StreamWriter(FilePath))
            {
                this.WriteToFile(writer, 0);
            }
        }
        // ------------------------------------------------------------------
        private void WriteToFile(StreamWriter writer, int tab)
        {
            this.Tab(writer, tab);
            writer.Write("<" + Tag);
            foreach (string attrName in m_attributes.Keys)
            {
                writer.Write(" " + attrName + "=\"" + m_attributes[attrName] + "\"");
            }
            if (this.HasNodes())
            {
                writer.WriteLine(" >");
                foreach (XMLNode childNode in Nodes())
                    childNode.WriteToFile(writer, tab + 1);
                this.Tab(writer, tab);
                writer.WriteLine("</" + Tag + ">");
            }
            else
                writer.WriteLine(" />");
        }
        // ------------------------------------------------------------------
        private void Tab(StreamWriter writer, int tab)
        {
            // put two spaces for every tab
            for (int i = 0; i < tab; i++)
                writer.Write("  ");
        }
        // ------------------------------------------------------------------
        #endregion
        // ------------------------------------------------------------------
    }
}
