﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.Global
{

    // =======================================================================================================================

    #region Event Aggregator related interfaces

    // =======================================================================================================================

    public interface ISubscriber<T>
    {
        void Receive(T message);
    }

    // --------------------------------------------------------------------------------------------------

    public interface IEventAggregator
    {
        /// <summary>
        /// an object of interface ISubscriber T when Subscribe to type T will be notified when type T is published.
        /// </summary>
        void Subscribe<T>(ISubscriber<T> subcriber);

        /// <summary>
        /// Notify all subscribers of type T
        /// </summary>
        void Publish<T>(T message);

        /// <summary>
        /// Mark the permanent subscribers
        /// </summary>
        void Mark();

        /// <summary>
        /// Remove volatile subscribers
        /// </summary>
        void Restore();
    }

    // =======================================================================================================================

    #endregion

    // =======================================================================================================================

    #region local notification

    // =======================================================================================================================

    public interface ILocalNotifier
    {
        void LocalIOChanged();
    }

    // =======================================================================================================================

    #endregion

    // =======================================================================================================================

    #region I/O Point interfaces

    // =======================================================================================================================

    public interface IIOPoint
    {
        string ImageSource { get; }
        string Name { get; }
        string QualifiedName { get; }
    }

    // --------------------------------------------------------------------------------------------------

    public interface IDigitalPoint : IIOPoint
    {
        void Set();
        void Clear();
        void Toggle();
        bool IsSet();
        bool IsClear();
        bool BooleanValue { get; set; }
        string ValueAsSymbol { get; }

        string InitialValueSymbol { get; }
        void ToggleInitialValue();
    }

    // --------------------------------------------------------------------------------------------------

    public interface IAnalogPoint : IIOPoint
    {
        double AnalogValue { get; set; }
        double InitialValue { get; set; }

        //void SetValue(double aValue);
        //double GetValue();
    }

    // --------------------------------------------------------------------------------------------------

    public interface IAxisPoint : IIOPoint
    {
        double AxisPosition { get; set; }
        double Velocity { get; set; }
        double Acceleration { get; set; }
        int Direction { get; set; }
        int Command { get; set; }

        //void SetValue(double aValue);
        //double GetValue();
    }


    // =======================================================================================================================

    #endregion

    // =======================================================================================================================


}
