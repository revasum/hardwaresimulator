﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.Global
{

    public enum RevasumDataTypes
    {
        None,
        DigitalInput,
        DigitalOutput,
        AnalogInput,
        AnalogOutput,
        AxisPosition,
        DoubleAnalogInput,
        DoubleAnalogOutput,

        EncoderInput,
        MotorStructureFromMC,
        AxisInput_UDINT,                // 8 entries    * 4 bytes           = 32
        AxisInput_DINT,                 // 2 entries    * 4 bytes           = 8
        AxisInput_LREAL,                // 12 entries   * 8 bytes           = 96
        AxisInput_DWORD,                // 6 entries    * 4 bytes           = 24
        AxisInput_UINT,                 // 4 entries    * 2 bytes           = 8
        AxisInput_USINTARR8,            // 2 entries    * 8 bytes           = 16
        AxisInput_Filler72 ,            // 1 entry      * 72 bytes          = 72      filler array

        MotorStructureToMC,             //                                  128 bytes
        AxisOutput_UDINT,               // 5 entries    * 4 bytes           = 20
        AxisOutput_DWORD,               // 1 entry      * 4 bytes           = 4
        AxisOutput_LREAL,               // 10 entries   * 8 bytes           = 80
        AxisOutput_BYTE,                // 2 entries    * 1 byte            = 2
        AxisOutput_BOOL,                // 1 entry      * 1 byte            = 1
        AxisOutput_Filler21,             // 1 entry      * 21 bytes          = 21       filler array
    };



    public enum SortModes
    {
        None,
        ByModule,
        ByName,
        ByIO
    };

    public enum OnLineStatusTypes
    {
        None,
        OffLine,
        OnLine,
        PLCNotPresent,
        NotEqual,
        HandlesError,
        Error
    };

    public class Constants
    {
        static Constants()
        {
            BaseTimerTickFrequency = 10;
            FrequencyCorrection =  2.0 / (3.1 * BaseTimerTickFrequency);

        }
        public static int BaseTimerTickFrequency { get; set; }
        public static double FrequencyCorrection { get; set; }

        public static int ConvertTImeToTicks(int timeSpan)
        {
            return (int)Math.Round(timeSpan * FrequencyCorrection);
        }


    }
}
