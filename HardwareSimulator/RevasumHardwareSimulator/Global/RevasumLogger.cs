﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;


namespace RevasumHardwareSimulator.Global
{
    public class Logger
    {
        private static readonly String LogDirectory = @"C:\Temp\hwsim";
        private static readonly String TimestampFormat = "yyyy-MM-dd HH:mm:ss.fff";
        private static readonly String LogFilenameFormat = "yyyy-MM-dd";
        private static readonly String ExceptionFilenameFormat = "yyyy-MM-ddTHH-mm-ss-fff";



        public static void PurgeLogs()
        {
            String filename = Path.Combine(Logger.LogDirectory, DateTime.Now.ToString(Logger.LogFilenameFormat) + ".txt");

            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
        }


        /// <summary>
        ///     Log message string to a day-stamped file
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="message"></param>
        public static void LogMessage()
        {
            String filename = Path.Combine(Logger.LogDirectory, DateTime.Now.ToString(Logger.LogFilenameFormat) + ".txt");
            String timestamp = DateTime.Now.ToString(Logger.TimestampFormat);

            Directory.CreateDirectory(Logger.LogDirectory);

            File.AppendAllText(filename, $"{timestamp} \r\n");
        }

        /// <summary>
        ///     Log message string to a day-stamped file
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="message"></param>
        public static void LogMessage(String className, String methodName, String message)
        {
            String filename = Path.Combine(Logger.LogDirectory, DateTime.Now.ToString(Logger.LogFilenameFormat) + ".txt");
            String timestamp = DateTime.Now.ToString(Logger.TimestampFormat);

            Directory.CreateDirectory(Logger.LogDirectory);

            File.AppendAllText(filename, $"{timestamp} {className}.{methodName}: {message}\r\n");
        }

        /// <summary>
        ///     Log exception string to a time-stamped file
        /// </summary>
        /// <param name="methodName"></param>
        /// <param name="ex"></param>
        public static void LogException(String className, String methodName, Exception ex)
        {
            String filename = Path.Combine(Logger.LogDirectory, "Exception_" + DateTime.Now.ToString(Logger.ExceptionFilenameFormat) + ".txt");
            String timestamp = DateTime.Now.ToString(Logger.TimestampFormat);

            Directory.CreateDirectory(Logger.LogDirectory);

            File.AppendAllText(filename, $"{timestamp} {className}.{methodName}: {ex.Message}\r\n{ex}");
        }
    }
}
