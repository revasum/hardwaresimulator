﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;

namespace RevasumHardwareSimulator.Datalog
{

    public class LogEntry : INotifyPropertyChanged
    {

        public event PropertyChangedEventHandler PropertyChanged;
        
         protected virtual void OnPropertyChanged(string propertyName)
        {
            Application.Current.Dispatcher.BeginInvoke((Action)(() =>
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
            }));
        }
 
        public DateTime DateTime { get; set; }

        public string FormatTimeStamp
        {
            get { return DateTime.ToString("MM/dd-HH:mm:ss.fff"); }
        }

        public string FormatIndex
        {
            get { return Index.ToString("D5"); }
        }

        public int Index { get; set; }

        public string Message { get; set; }

        public string ObjectName { get; set; }

        public string QualifiedName { get; set; }

        public string ClassName { get; set; }

        public string MethodName { get; set; }

    }


}
