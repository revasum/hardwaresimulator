﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.Datalog
{
    /// <summary>
    /// Interaction logic for LogViewer.xaml
    /// </summary>
    public partial class LogViewer : UserControl
    {

        private int m_index = 0;
        private readonly int m_maxItems = 10000;  // circular buffer

        public ObservableCollection<LogEntry> LogEntries { get; set; }

        static LogViewer s_current;

        public LogViewer()
        {
            s_current = this;

            InitializeComponent();

            DataContext = LogEntries = new ObservableCollection<LogEntry>();
            ScrollEnabled = true;

            //PopulateWithPoeData();

        }

        // -------------------------------------------------------------

        #region Logging

        // -------------------------------------------------------------

        public static void Log(DateTime timestamp, string message, string className, string methodName, string senderName, string qualifiedName)
        {

            s_current._Log(timestamp, message, className, methodName, senderName, qualifiedName);
        }

        // -------------------------------------------------------------

        private void _Log(DateTime timestamp, string message, string className, string methodName, string senderName, string qualifiedName)
        {

            LogEntry entry = new LogEntry()
            {
                Index = m_index++,
                DateTime = timestamp,
                ClassName = className,
                MethodName = methodName,
                ObjectName = senderName,
                QualifiedName = qualifiedName,
                Message = message
            };
            Log(entry);
        }

        // -------------------------------------------------------------

        private void Log(LogEntry anEntry)
        {
            Dispatcher.BeginInvoke((Action)(() => LogOnGUIThread(anEntry)));
        }

        // -------------------------------------------------------------

        private void LogOnGUIThread(LogEntry anEntry)
        {
            LogEntries.Add(anEntry);
            if (ScrollEnabled && LogEntries.Count > m_maxItems)
                LogEntries.RemoveAt(0);
        }

        #endregion

        // -------------------------------------------------------------

        #region Edgar Allan Poe

        // -------------------------------------------------------------

        private int m_poeCount;
        private int m_poeMax;
        private List<string> PoeWords;

        // -------------------------------------------------------------
        
        private void PopulateWithPoeData()
        {

            string RavenWithBreaks;
            using (StreamReader streamReader = new StreamReader(FileManager.GetPathForText("Raven.txt"), Encoding.UTF8))
            {
                RavenWithBreaks = streamReader.ReadToEnd();
            }

            string Raven = String.Join("", RavenWithBreaks.Where(c => c != '\t' && c != '\n' && c != '\r'));   // remove control characters
            char weirdCharacter = Convert.ToChar(65533);
            Raven = Raven.Replace(weirdCharacter, ' ');
            Raven = string.Join(" ", Raven.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)); // remove double spaces

            PoeWords = Raven.Split(' ').ToList();
            m_poeMax = PoeWords.Count;

            m_poeCount = 0;
            while (m_poeCount < m_poeMax)
            {
                LogEntry le = CreatePoeEntry();
                LogEntries.Add(le);
            }
        }

        // -------------------------------------------------------------

        private LogEntry CreatePoeEntry()
        {
            int start = m_poeCount;
            int numOfElements = 7;
            if (m_poeCount + numOfElements > m_poeMax)
                numOfElements = m_poeMax - m_poeCount;
            m_poeCount += numOfElements;

            return new LogEntry()
            {
                Index = m_index++,
                DateTime = DateTime.Now,
                ObjectName = "Poe",
                QualifiedName = "Edgar Allan Poe",
                Message = string.Join(" ", Enumerable.Range(0, numOfElements)
                                                     .Select(x => PoeWords[x + start])),
            };
        }

        // -------------------------------------------------------------

        #endregion

        // -------------------------------------------------------------

        #region Scrolling

        // -------------------------------------------------------------
        
        private bool m_scrollEnabled = true;
        private bool ScrollEnabled
        {
            get { return m_scrollEnabled; }
            set { m_scrollEnabled = value; }
        }

        // -------------------------------------------------------------
        
        private void StopScrolling_Click(object sender, RoutedEventArgs e)
        {
            ScrollEnabled = false;
        }

        // -------------------------------------------------------------

        private void ResumeScrolling_Click(object sender, RoutedEventArgs e)
        {
            ScrollEnabled = true;
        }

        // -------------------------------------------------------------

        private void ScrollChanged_Event(object sender, ScrollChangedEventArgs e)
        {
            if (ScrollEnabled)
            {

                var scrollViewer = sender as ScrollViewer;
                if (e.ExtentHeightChange > 0)
                    scrollViewer.ScrollToEnd();
            }
        }

        // -------------------------------------------------------------

        #endregion

        // -------------------------------------------------------------

    }
}
