﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XMLSupport;

namespace RevasumSimulator.BaseClasses
{
    public class PropertyClass : BaseClass
    {
        string m_description;
        string m_value;

        public override string ClassName
        {
            get { return "Property"; }
        }

        public string Description
        {
            get { return m_description; }
        }

        public string TheValue
        {
            get { return m_value; }
        }


        protected override void GetNameFromSysConfig(XMLNode sysConfig)
        {
            if (sysConfig.AttributeAt("name") != "")
                Name = sysConfig.AttributeAt("name");
        }

        public override void Initialize(XMLNode sysConfig)
        {
            base.Initialize(sysConfig);
            m_description = sysConfig.AttributeAt("description");
            m_value = sysConfig.AttributeAt("value");
        }

        public override string ToString()
        {
            return "{Property} name=" + Name + ", value=" + m_value + ", description=" + m_description;
        }

        public override string[] AsStringArray()
        {
            string[] retValue = { Name, TheValue, Description };
            return retValue;
        }
    }
}
