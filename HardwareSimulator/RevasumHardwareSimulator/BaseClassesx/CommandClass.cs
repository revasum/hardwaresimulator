﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XMLSupport;

namespace RevasumSimulator.BaseClasses
{
    public class CommandClass : BaseClass
    {
        string m_type;
        string m_message;

        public override string ClassName
        {
            get { return "Command"; }
        }

        public string Message
        {
            get { return m_message; }
        }

        public string TheType
        {
            get { return m_type; }
        }


        protected override void GetNameFromSysConfig(XMLNode sysConfig)
        {
            Name = sysConfig.AttributeAt("message");
        }

        public override void Initialize(XMLNode sysConfig)
        {
            base.Initialize(sysConfig);
            m_message = Name;
            m_type = sysConfig.AttributeAt("type");
        }

        public override string ToString()
        {
            return "{Command} type=" + m_type + ", message=" + m_message;
        }

        public override string[] AsStringArray()
        {
            string[] retValue = { TheType, Message };
            return retValue;
        }

    }
}
