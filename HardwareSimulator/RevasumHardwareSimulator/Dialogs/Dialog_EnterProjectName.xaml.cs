﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_EnterProjectName.xaml
    /// </summary>
    public partial class Dialog_EnterProjectName : Window
    {
        public Dialog_EnterProjectName()
        {
            InitializeComponent();
        }

        public static string AskForProjectName()
        {

            Dialog_EnterProjectName inst = new Dialog_EnterProjectName();
            inst.ShowDialog();
            if (inst.DialogResult == true)
                return inst.m_projectName.Text;
            return "";
        }



        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

    }
}
