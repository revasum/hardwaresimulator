﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_YesNoSelection.xaml
    /// </summary>
    public partial class Dialog_YesNoSelection : Window
    {
        public Dialog_YesNoSelection(string title, string message, string yes, string no)
        {
            InitializeComponent();
            m_title.Text = title;
            m_message.Text = message;
            m_btOK.Content = yes;
            m_btn_Cancel.Content = no;
        }

        public static bool? AskForYesNoDecision(string title, string message, string yes, string no)
        {

            Dialog_YesNoSelection inst = new Dialog_YesNoSelection(title, message, yes, no);
            inst.ShowDialog();
            return inst.DialogResult;
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
            Close();
        }


    }
}
