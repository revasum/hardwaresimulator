﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_SourceToMerge.xaml
    /// </summary>
    public partial class Dialog_SourceToMerge : Window
    {
        //IOConfiguration m_configuration;

        public Dialog_SourceToMerge(IOConfiguration config)
        {
            InitializeComponent();
            //m_configuration = config;

            m_createdTime.Text = DateToString(config.CreationTIme);
            m_modifiedTime.Text = DateToString(config.ModifyTime);
            m_project.Text = config.ProjectName;
            m_ioPoints.Text = config.Points.Count.ToString();
            m_behaviors.Text = config.NumberOfBehaviors.ToString();
            m_defaults.Text = config.NumberOfDefaults.ToString();
            m_plcComments.Text = config.PLCComment;
        }

        // --------------------------------------------------------------------------------------

        string DateToString(DateTime timeStamp)
        {
            string answer = String.Format("on {0} at {1}", timeStamp.ToString("MM/dd/yy"), timeStamp.ToString("HH:mm:ss"));
            return answer;
        }

        // --------------------------------------------------------------------------------------

        private void Proceed_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        // --------------------------------------------------------------------------------------


    }
}
