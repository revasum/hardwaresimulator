﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_ErrorMessage.xaml
    /// </summary>
    public partial class Dialog_ErrorMessage : Window
    {
        public Dialog_ErrorMessage(string message)
        {
            InitializeComponent();
            Message.Text = message;
        }

        public static Dialog_ErrorMessage Show(string message)
        {
            Dialog_ErrorMessage dialog = new Dialog_ErrorMessage(message);
            dialog.ShowDialog();
            return dialog;
        }

        public event EventHandler WindowClosed;

        private void Window_Closed(object sender, EventArgs e)
        {
            WindowClosed?.Invoke(this, e);
        }
    }
}
