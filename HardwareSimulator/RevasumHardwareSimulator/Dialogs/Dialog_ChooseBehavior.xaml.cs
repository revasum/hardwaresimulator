﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_ChooseBehaviorForDI.xaml
    /// </summary>
    public partial class Dialog_ChooseBehavior : Window
    {

        public Dialog_ChooseBehavior(ObservableCollection<string> options, string IOName, string IOType)
        {
            InitializeComponent();
            //m_textBoxOptions = options;
            textBoxName.Text = IOName;
            textBoxIOType.Text = IOType;
            listView.ItemsSource = options;
        }

        private string SelectedItemName
        {
            get
            {
                if (listView.SelectedItem == null)
                    return "";
                return listView.SelectedItem.ToString();

            }
        }

        public static string AskForBehavior(ObservableCollection<string> options, string IOName, string IOType)
        {

            Dialog_ChooseBehavior inst = new Dialog_ChooseBehavior(options, IOName, IOType);
            inst.ShowDialog();
            if (inst.DialogResult == true)
                return inst.SelectedItemName;
            return "";
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
