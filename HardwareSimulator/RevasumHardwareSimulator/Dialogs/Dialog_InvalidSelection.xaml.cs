﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RevasumHardwareSimulator.Dialogs
{
    /// <summary>
    /// Interaction logic for Dialog_MustBeDigital.xaml
    /// </summary>
    public partial class Dialog_InvalidSelection : Window
    {
        public Dialog_InvalidSelection(string message)
        {
            InitializeComponent();
            Message.Text = message;
        }

        public static void Show(string message)
        {
            Dialog_InvalidSelection dialog = new Dialog_InvalidSelection(message);
            dialog.ShowDialog();
        }
    }
}
