﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RevasumHardwareSimulator.mvp_Presenters.ConfigurationManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.ConfigurationManagement
{
    /// <summary>
    /// Interaction logic for FileTab.xaml
    /// </summary>
    public partial class FileTab : UserControl
    {
        FileTabPresenter m_presenter;

        public FileTab()
        {
            m_presenter = new FileTabPresenter(this);
            InitializeComponent();
            DataContext = m_presenter;

        }

        // ================================================================================================

        #region Pushbuttons

        // ================================================================================================

        private void GetLatest_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.LoadlatestRequest();
        }

        // --------------------------------------------------------------------------------------

        private void GetFromPLC_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.LoadFromPLCRequest();
        }

        // --------------------------------------------------------------------------------------

        private void GetFromFile_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.LoadFromFileRequest();
        }

        // --------------------------------------------------------------------------------------

        private void MergeConfiguration_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.MergeConfigurationsRequest();
        }

        // --------------------------------------------------------------------------------------

        private void SaveConfigurationComments_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.SaveConfigurationComments();
        }

        // --------------------------------------------------------------------------------------

        private void BackupCurrent_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.BackupCurrentConfiguration();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Change Expanders Colors

        // ================================================================================================

        private void DigitalExpanded_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.DigitalExpanderColor = "CornflowerBlue";
        }

        // --------------------------------------------------------------------------------------

        private void DigitalCollapsed_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.DigitalExpanderColor = "Black";
            m_presenter.ExitingScreen();
        }

        // --------------------------------------------------------------------------------------

        private void AnalogExpanded_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.AnalogExpanderColor = "CornflowerBlue";
        }

        // --------------------------------------------------------------------------------------

        private void AnalogCollapsed_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.AnalogExpanderColor = "Black";
            m_presenter.ExitingScreen();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================



        private void XMLSave_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.SaveConfigurationAsXML();
        }

        // --------------------------------------------------------------------------------------

        private void XMLLoad_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.LoadConfigurationFromXML();
        }

        // --------------------------------------------------------------------------------------


    }
}
