﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Presenters.ConfigurationManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.ConfigurationManagement
{
    /// <summary>
    /// Interaction logic for InitialValues_Digital.xaml
    /// </summary>
    public partial class InitialValues_Digital : UserControl
    {
        public InitialValues_Digital()
        {
            InitializeComponent();
            DataContext = Presenter;
        }

        // --------------------------------------------------------------------------------------

        FileTabPresenter Presenter
        {
            get { return FileTabPresenter.Current; }
        }

        // --------------------------------------------------------------------------------------

        private void ToggleState_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            IDigitalPoint point = button.DataContext as IDigitalPoint;
            point.ToggleInitialValue();
            Presenter.DefaultsChanged();
        }

        // --------------------------------------------------------------------------------------

    }

}
