﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Global;

using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Presenters.ConfigurationManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.ConfigurationManagement
{
    /// <summary>
    /// Interaction logic for InitialValues_Analog.xaml
    /// </summary>
    public partial class InitialValues_Analog : UserControl
    {
        public InitialValues_Analog()
        {
            InitializeComponent();
            DataContext = FileTabPresenter.Current; 
        }

        // --------------------------------------------------------------------------------------

        FileTabPresenter Presenter
        {
            get { return FileTabPresenter.Current; }
        }

        // --------------------------------------------------------------------------------------

        private void KeyPressed_Event(object sender, KeyEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            IAnalogPoint point = textBox.DataContext as IAnalogPoint;
            double parsedText;
            if (Double.TryParse(textBox.Text, out parsedText))
                point.InitialValue = parsedText;
            Presenter.DefaultsChanged();
        }

        // --------------------------------------------------------------------------------------

    }
}
