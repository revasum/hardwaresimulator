﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_SingleDigital.xaml.  Digital Behavior with only ONE input. Currently I can think of only NOT operation that satisfies this
    /// </summary>
    public partial class BehaviorView_SingleDigital : UserControl
    {
        public BehaviorView_SingleDigital()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditNotBehaviorPresenter Presenter
        {
            get { return DataContext as EditNotBehaviorPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Add input
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            IOPoint selectedIOPoint = Presenter.SelectedAvailablePoint;

            if (selectedIOPoint != null)
                if (!selectedIOPoint.IsDigital)
                    Dialog_InvalidSelection.Show("Must be Digital point");
                else
                    Presenter.SelectedInput = selectedIOPoint as IDigitalPoint;
            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// remove input.  Since it is the sole input - delete the behavior
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(true);
        }

        // --------------------------------------------------------------------------------------

    }
}
