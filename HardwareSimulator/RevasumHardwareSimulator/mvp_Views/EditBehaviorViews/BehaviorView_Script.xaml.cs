﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_Script.xaml
    /// </summary>
    public partial class BehaviorView_Script : UserControl
    {
        public BehaviorView_Script()
        {
            InitializeComponent();
        }

        public EditScriptBehaviorPresenter Presenter
        {
            get { return DataContext as EditScriptBehaviorPresenter; }
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            RevasumSimulatedPoint selectedIOPoint = Presenter.SelectedAvailablePoint;

            IOPoint ioPoint = selectedIOPoint as IOPoint;
            if (ioPoint == null)
                Dialog_InvalidSelection.Show("Must be an IO point");
            else
                Presenter.AddIOPoint(ioPoint);

            Presenter.DeselectAvailablePoint();
        }

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            List<IOPoint> points = new List<IOPoint>();
            foreach (var item in listView.ItemsSource)
                points.Add(item as IOPoint);

            foreach (IOPoint item in points)
                if (item.IsSelected)
                {
                    Presenter.RemoveIOPoint(item);
                }

            if (listView.Items.Count == 0)
                Presenter.Delete(true);
        }

        private void TestScript_Click(object sender, RoutedEventArgs e)
        {
            Presenter.TestScriptRun();
        }
    }
}
