﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorViiew_Threshold.xaml - A digital behavior that goes on or off based on the value of analog input
    /// </summary>
    public partial class BehaviorViiew_Threshold : UserControl
    {
        public BehaviorViiew_Threshold()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditThresholdPresenter Presenter
        {
            get { return DataContext as EditThresholdPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Add an iput
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            IOPoint selectedIOPoint = Presenter.SelectedAvailablePoint;
            if (selectedIOPoint != null)
                if (!selectedIOPoint.IsAnalog)
                    Dialog_InvalidSelection.Show("Must be Analog point");
                else
                    Presenter.SelectedInput = selectedIOPoint as IAnalogPoint;
            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// remove the input and delete behavior
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(true);
        }

        // --------------------------------------------------------------------------------------

        private void CheckedChanged_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            Presenter.ReverseActing = (bool)box.IsChecked;
        }

        // --------------------------------------------------------------------------------------



    }
}
