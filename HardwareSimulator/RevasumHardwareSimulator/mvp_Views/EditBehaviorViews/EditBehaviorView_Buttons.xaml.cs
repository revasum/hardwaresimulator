﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Those three buttons are added to the bottom of each EditBehaviorView, and present a common interface for each
    /// </summary>
    public partial class EditBehaviorView_Buttons : UserControl
    {
        public EditBehaviorView_Buttons()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditBehaviorPresenter Presenter
        {
            get { return DataContext as EditBehaviorPresenter; }
        }

        // --------------------------------------------------------------------------------------

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Save();
        }

        // --------------------------------------------------------------------------------------

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(false);
        }

        // --------------------------------------------------------------------------------------

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Close();
        }

        // --------------------------------------------------------------------------------------

    }
}
