﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_Axis.xaml
    /// </summary>
    public partial class BehaviorView_Axis : UserControl
    {
        public BehaviorView_Axis()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditAxisPresenter Presenter
        {
            get { return DataContext as EditAxisPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Add an input.  THe input for this behavior can be only PLC -> NC command structure
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            IOPoint selectedIOPoint = Presenter.SelectedAvailablePoint;
            if (selectedIOPoint != null)
                if (!selectedIOPoint.IsAxisCommandStructure)
                    Dialog_InvalidSelection.Show("Must be Axis Command Structure");
                else
                    Presenter.SelectedInput = selectedIOPoint;
            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(true);
        }

        // --------------------------------------------------------------------------------------

    }
}
