﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_Linear.xaml
    /// </summary>
    public partial class BehaviorView_Linear : UserControl
    {
        public BehaviorView_Linear()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditLinearBehaviorPresenter Presenter
        {
            get { return DataContext as EditLinearBehaviorPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Add input that can start the behavior
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            IOPoint selectedIOPoint = Presenter.SelectedAvailablePoint;

            if (selectedIOPoint != null)
                if (!selectedIOPoint.IsDigital)
                    Dialog_InvalidSelection.Show("Must be Digital point");
                else
                    Presenter.SelectedInput = selectedIOPoint as IDigitalPoint;
            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// sisnce there is only one input possible, removing it equates to deleting the behavior
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(true);
        }

        // --------------------------------------------------------------------------------------

        private void CheckedChanged_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            Presenter.ReverseActing = (bool) box.IsChecked;
        }

        // --------------------------------------------------------------------------------------

        private void HoldValueChanged_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            Presenter.HoldLastValue = (bool)box.IsChecked;
        }

        // --------------------------------------------------------------------------------------
        

    }
}
