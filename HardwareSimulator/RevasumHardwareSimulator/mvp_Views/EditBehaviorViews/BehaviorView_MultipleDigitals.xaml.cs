﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;


namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_MultipleDigitals.xaml - All the digital inputs can be connected by either AND or OR operation to drive the target
    /// </summary>
    public partial class BehaviorView_MultipleDigitals : UserControl
    {

        public BehaviorView_MultipleDigitals()
        {
            InitializeComponent();
            //listView.SelectionMode = SelectionMode.Single;
        }

        // --------------------------------------------------------------------------------------

        public EditMultipleBehaviorPresenter Presenter
        {
            get { return DataContext as EditMultipleBehaviorPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// add another digital input
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            RevasumSimulatedPoint selectedIOPoint = Presenter.SelectedAvailablePoint;

            if (!(selectedIOPoint is IDigitalPoint digital))
            {
                Dialog_InvalidSelection.Show("Must be Digital point");
            }
            else
            {
                Presenter.NewIOPoint(digital);
            }

            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Remove one of the inputs. If it is the last input in the lest, delete the behavior
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            List<SelectedIDigitalPointTemplate> points = new List<SelectedIDigitalPointTemplate>();
            foreach (var item in listView.ItemsSource)
                points.Add(item as SelectedIDigitalPointTemplate);

            foreach (SelectedIDigitalPointTemplate item in points)
                if (item.IsSelected)
                {
                    Presenter.RemvoveIOPoint(item);
                }

            if (listView.Items.Count == 0)
                Presenter.Delete(true);


            //ItemCollection collection = listView.ItemsSource as ItemCollection;
            //if (listView.Items.Count > 1)
            //{
            //    IDigitalPoint listSelectedPoint = listView.SelectedItem as IDigitalPoint;
            //    if (listSelectedPoint == null) return;
            //    //listView.SelectedIndex = -1;
            //    //listView.SelectedItem = null;
            //    listView.Focus();
            //    Presenter.DeselectAvailablePoint();
            //    Presenter.RemoveIOPoint(listSelectedPoint);
            //}
            //else
            //    Presenter.Delete(true);
        }

        // --------------------------------------------------------------------------------------

        private void ListView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //System.Windows.Controls.ListView lv = sender as System.Windows.Controls.ListView;
        }

        private void CheckedChanged_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            SelectedIDigitalPointTemplate template = box.DataContext as SelectedIDigitalPointTemplate;
            template.IsReverseActing = (bool) box.IsChecked;
            Presenter.IsDirty = true;
       }

        // --------------------------------------------------------------------------------------

        private void SelectedChanged_Click(object sender, RoutedEventArgs e)
        {
            CheckBox box = sender as CheckBox;
            SelectedIDigitalPointTemplate template = box.DataContext as SelectedIDigitalPointTemplate;
            template.IsSelected = (bool)box.IsChecked;
        }

        // --------------------------------------------------------------------------------------


    }
}
