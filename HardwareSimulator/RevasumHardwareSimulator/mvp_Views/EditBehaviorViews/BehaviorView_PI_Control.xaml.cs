﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Views.EditBehaviorViews
{
    /// <summary>
    /// Interaction logic for BehaviorView_PI_Control.xaml
    /// </summary>
    public partial class BehaviorView_PI_Control : UserControl
    {
        public BehaviorView_PI_Control()
        {
            InitializeComponent();
        }

        // --------------------------------------------------------------------------------------

        public EditPIControlBehaviorPresenter Presenter
        {
            get { return DataContext as EditPIControlBehaviorPresenter; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Add input that can start the behavior
        /// </summary>
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            IOPoint selectedIOPoint = Presenter.SelectedAvailablePoint;

            if (selectedIOPoint != null)
                if (!selectedIOPoint.IsAnalog)
                    Dialog_InvalidSelection.Show("Must be Analog point");
                else
                    Presenter.SelectedInput = selectedIOPoint as IAnalogPoint;
            Presenter.DeselectAvailablePoint();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// sisnce there is only one input possible, removing it equates to deleting the behavior
        /// </summary>
        private void Remove_Click(object sender, RoutedEventArgs e)
        {
            Presenter.Delete(true);
        }
        

    }
}
