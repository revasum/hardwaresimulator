﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.BehaviorManagement
{
    /// <summary>
    /// Interaction logic for Behavior_Inputs.xaml
    /// </summary>
    public partial class Behavior_Inputs : UserControl
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        BehaviorInputsPresenter m_presenter;

        // --------------------------------------------------------------------------------------

        public Behavior_Inputs()
        {
            InitializeComponent();
            m_presenter = new BehaviorInputsPresenter(this);
            DataContext = m_presenter;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Select point (from left list)

        // ================================================================================================

        private void PointSelected_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            m_presenter.ButtonClicked(button.DataContext);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Search / Sort

        // ================================================================================================

        private void SearchText_Changed(object sender, TextChangedEventArgs e)
        {
            TextBox box = sender as TextBox;
            m_presenter.Search(box.Text);
        }

        // --------------------------------------------------------------------------------------

        private void SortMode_Changed(object sender, RoutedEventArgs e)
        {
            searchText.Text = "";
            RadioButton aButton = sender as RadioButton;
            string content = aButton.Content as string;
            m_presenter.Resort(content);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
