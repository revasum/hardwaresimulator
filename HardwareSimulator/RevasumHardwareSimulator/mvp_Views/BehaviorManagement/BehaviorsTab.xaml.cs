﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RevasumHardwareSimulator.mvp_Presenters;

using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.mvp_Views.BehaviorManagement
{
    /// <summary>
    /// Interaction logic for BehaviorsTab.xaml
    /// </summary>
    public partial class BehaviorsTab : UserControl
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        BehaviorTabPresenter m_presenter;

        // --------------------------------------------------------------------------------------

        public BehaviorsTab()
        {
            InitializeComponent();
            m_presenter = new BehaviorTabPresenter(this);
            DataContext = m_presenter;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Add / Remove Tab

        // ================================================================================================

        /// <summary>
        ///  Presenter calls this to prevent associating the same target with multiple behaviors
        /// </summary>
        /// <param name="aTargetPoint"></param>
        /// <returns></returns>
        public bool TargetAlreadyOpen(IOPoint aTargetPoint)
        {

            for (int i = 0; i < m_tabs.Items.Count; i++)
            {
                TabItem existingTab = (TabItem)m_tabs.Items[i];
                EditBehaviorPresenter existingPresenter = existingTab.DataContext as EditBehaviorPresenter;
                Behavior behavior = existingPresenter.Behavior;
                if (aTargetPoint == behavior.Target)
                {
                    existingTab.Focus();
                    return true;
                }
            }
            return false;
        }

        // --------------------------------------------------------------------------------------

        public void AddTab<T>(PresenterBase<T> presenter)
        {

            // to analyze this, first see BehaviorTabPresenter.InputSelected. 

            // Once BehaviorManager created  EditPresenter (based on use selection), the control is sent here (View.AddTab(editPresenter))
            //    (not also that the behavior manager also created appropriate model and view for the edit presenter)

            // if the tab already exists, we will remove it and reinstert it (changing its order)
            // otherwise we will create a new tab and set up binding between presenter and the view


            EditBehaviorPresenter edit = presenter as EditBehaviorPresenter;
            IOPoint newTarget = edit.Behavior.Target;
            TabItem newTab = null;

            // see if there is a behavior with this target already open.  if so, remove it and set the new tab to it.
            for (int i = 0; i < m_tabs.Items.Count; i++)
            {
                TabItem existingTab = (TabItem)m_tabs.Items[i];
                EditBehaviorPresenter existingPresenter = existingTab.DataContext as EditBehaviorPresenter;
                if (existingTab.DataContext.Equals(presenter))
                {
                    m_tabs.Items.Remove(existingTab);
                    newTab = existingTab;
                    break;
                }
            }

            // create new tab
            if (newTab == null)
            {
                newTab = new TabItem();
                Binding headerBinding = new Binding(newTarget.QualifiedName);
                BindingOperations.SetBinding(
                    newTab,
                    TabItem.HeaderProperty,
                    headerBinding);
                newTab.DataContext = presenter;
                newTab.Content = presenter.View;
            }

            // re-insert the existing tab, or newly created.
            newTab.Header = edit.Behavior.Name;
            m_tabs.Items.Insert(0, newTab);
            newTab.Focus();
        }

        // --------------------------------------------------------------------------------------

        public void RemoveTab<T>(PresenterBase<T> presenter)
        {
            for (int i = 0; i < m_tabs.Items.Count; i++)
            {
                TabItem item = (TabItem)m_tabs.Items[i];
                if (item.DataContext.Equals(presenter))
                {
                    m_tabs.Items.Remove(item);
                    break;
                }
            }
        }

        // --------------------------------------------------------------------------------------

        public void RemoveAllTabs()
        {
            while (m_tabs.Items.Count > 0)
            {
                TabItem item = (TabItem)m_tabs.Items[0];
                m_tabs.Items.Remove(item);
            }
        }

        // --------------------------------------------------------------------------------------

        private void ScreenLoaded_Click(object sender, RoutedEventArgs e)
        {
            using (new RevasumHardwareSimulator.Global.WaitCursor())
            {
                // what now?
            }

        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
