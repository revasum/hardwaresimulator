﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Presenters.OnLineManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.OnLineManagement
{
    /// <summary>
    /// Interaction logic for MonitoredPoints.xaml
    /// </summary>
    public partial class DigitalMonitoredPoints : UserControl
    {

        OnLineManagementPresenter m_presenter;

        // --------------------------------------------------------------------------------------

        #region Initialization

        // --------------------------------------------------------------------------------------

        public DigitalMonitoredPoints()
        {
            InitializeComponent();
            m_presenter = OnLineManagementPresenter.Current;
            DataContext = m_presenter;
        }

        // --------------------------------------------------------------------------------------

        OnLineManagementPresenter Presenter
        {
            get { return m_presenter; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Select behavior (the presenter will have to verify if the behavior exists)

        // ================================================================================================

        private void PointSelected_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            m_presenter.ButtonClicked(button.DataContext);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
