﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters.OnLineManagementPresenters;

namespace RevasumHardwareSimulator.mvp_Views.OnLineManagement
{
    /// <summary>
    /// Interaction logic for AxesMonitoredPoints.xaml
    /// </summary>
    public partial class AxesMonitoredPoints : UserControl
    {

        OnLineManagementPresenter m_presenter;
       
        public AxesMonitoredPoints()
        {
            InitializeComponent();
            m_presenter = OnLineManagementPresenter.Current;
            DataContext = m_presenter;
        }

        // ================================================================================================

        #region Select behavior (the presenter will have to verify if the behavior exists)

        // ================================================================================================

        private void PointSelected_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            m_presenter.ButtonClicked(button.DataContext);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
