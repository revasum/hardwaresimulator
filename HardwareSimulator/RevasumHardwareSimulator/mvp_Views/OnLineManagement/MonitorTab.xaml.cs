﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Presenters.OnLineManagementPresenters;
using RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters;

namespace RevasumHardwareSimulator.mvp_Views.OnLineManagement
{
    /// <summary>
    /// Interaction logic for MonitorTab.xaml
    /// </summary>
    public partial class MonitorTab : UserControl
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================
        

        static MonitorTab s_current;
        private readonly OnLineManagementPresenter m_presenter;


        public MonitorTab()
        {
            m_presenter = new OnLineManagementPresenter(this);
            InitializeComponent();
            DataContext = m_presenter;
            s_current = this;

        }

        // --------------------------------------------------------------------------------------------------

        public static MonitorTab Current
        {
            get { return s_current; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Simulation Start / Stop

        // ================================================================================================

        private void StartStopSimulation_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.StartStopSimulationAsync();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Diagnostics_Click

        // ================================================================================================

        private void ShowDiagnostics_Click(object sender, RoutedEventArgs e)
        {
            string numberOfReadTimeCounts =
                PlcAccessor.Instance().ReadDiagnostics.GetNumberOfTimeCounts() == PlcAccessor.Instance().ReadDiagnostics.MaxListSize ?
                $"Exceeds {PlcAccessor.Instance().ReadDiagnostics.MaxListSize}" : PlcAccessor.Instance().ReadDiagnostics.GetNumberOfTimeCounts().ToString();

            string numberOfWriteTimeCounts =
                PlcAccessor.Instance().WriteDiagnostics.GetNumberOfTimeCounts() == PlcAccessor.Instance().WriteDiagnostics.MaxListSize ?
                $"Exceeds {PlcAccessor.Instance().WriteDiagnostics.MaxListSize}" : PlcAccessor.Instance().WriteDiagnostics.GetNumberOfTimeCounts().ToString();

            MessageBox.Show($"UpdateDiagnostics\n\n" +
                            $"Error Count\t: {PlcAccessor.Instance().UpdateDiagnostics.ErrorCount}\n" +
                            $"Update Count\t: {PlcAccessor.Instance().UpdateDiagnostics.UpdateCount}\n\n" +
                            $"Read Diagnostics\n\n" +
                            $"Error Count\t: {PlcAccessor.Instance().ReadDiagnostics.ErrorCount}\n" +
                            $"Read Count\t: {numberOfReadTimeCounts}\n" +
                            $"Max Time\t: {PlcAccessor.Instance().ReadDiagnostics.MaxTime} ms\n" +
                            $"Average Time\t: {PlcAccessor.Instance().ReadDiagnostics.GetAverageInMilliseconds()} ms\n\n" +
                            $"Write Diagnostics\n\n" +
                            $"Error Count\t: {PlcAccessor.Instance().WriteDiagnostics.ErrorCount}\n" +
                            $"Retry Count\t: {PlcAccessor.Instance().WriteDiagnostics.RetryCount}\n" +
                            $"Write Count\t: {numberOfWriteTimeCounts}\n" +
                            $"Max Time\t: {PlcAccessor.Instance().WriteDiagnostics.MaxTime} ms\n" +
                            $"Average Time\t: {PlcAccessor.Instance().WriteDiagnostics.GetAverageInMilliseconds()} ms\n\n",
                            "ADS Client Diagnostics");
        }

        // ================================================================================================

        #endregion


        // ================================================================================================

        #region Add / Remove Tabs

        // ================================================================================================

        public void AddTab(OnLineBehaviorPresenter presenter)
        {
            OnLineBehaviorPresenter monitor = presenter as OnLineBehaviorPresenter;

            TabItem newTab = null;

            IOPoint target = monitor.Target;

            // see if there is a behavior with this target already open.  if so, remove it and set the new tab to it.
            for (int i = 0; i < m_tabs.Items.Count; i++)
            {
                TabItem existingTab = (TabItem)m_tabs.Items[i];
                OnLineBehaviorPresenter existingPresenter = existingTab.DataContext as OnLineBehaviorPresenter;
                if (existingPresenter.Target == target)
                {
                    m_tabs.Items.Remove(existingTab);
                    newTab = existingTab;
                    break;
                }
            }

            //// create new tab
            if (newTab == null)
            {
                newTab = new TabItem();
                Binding headerBinding = new Binding(target.QualifiedName);
                BindingOperations.SetBinding(
                    newTab,
                    TabItem.HeaderProperty,
                    headerBinding);
                newTab.DataContext = presenter;
                newTab.Content = presenter.View;
            }

            //// re-insert the existing tab, or newly created.
            newTab.Header = target.QualifiedName;
            m_tabs.Items.Insert(0, newTab);
            newTab.Focus();

        }

        // --------------------------------------------------------------------------------------

        public void RemoveTab(OnLineBehaviorPresenter presenter)
        {
            for (int i = 0; i < m_tabs.Items.Count; i++)
            {
                TabItem item = (TabItem)m_tabs.Items[i];
                if (item.DataContext.Equals(presenter))
                {
                    m_tabs.Items.Remove(item);
                    break;
                }
            }
        }

        // --------------------------------------------------------------------------------------

        public void RemoveAllTabs()
        {
            while (m_tabs.Items.Count > 0)
                m_tabs.Items.RemoveAt(0);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Slider

        // ================================================================================================

        private void SliderValueChanged_Click(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            // present the slider value as a double number between 0.25 and 10

            double exponent = Math.Pow(2, m_speedSlider.Value);
            double normalized = exponent / 4.0;

            m_presenter.NewSliderValue(normalized);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


        private void Break_Click(object sender, RoutedEventArgs e)
        {
            m_presenter.Break();
        }

        // --------------------------------------------------------------------------------------------------

    }

}
