﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RevasumHardwareSimulator.mvp_Views.OnLineBehaviorViews
{
    /// <summary>
    /// Interaction logic for OnLineView_LINEAR.xaml
    /// </summary>
    public partial class OnLineView_LINEAR : UserControl
    {
        public OnLineView_LINEAR()
        {
            InitializeComponent();
        }
    }
}
