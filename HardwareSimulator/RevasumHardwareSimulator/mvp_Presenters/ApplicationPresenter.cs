﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Notifiers;

namespace RevasumHardwareSimulator.mvp_Presenters
{

    /// <summary>
    /// a presenter for the Main Window.  Supports the main icon bitmap, and closing application notification.
    /// It also controls the heading color based on On-Line status
    /// </summary>
    public class ApplicationPresenter : PresenterBase<MainWindow>
    {

        GlobalTimer m_globalTimer;  // just to start it and have someone holding a reference to it

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        private static ApplicationPresenter s_current = null;
        public static ApplicationPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public ApplicationPresenter(
            MainWindow view)
            : base(view)
        {
            // just a test
            using (new RevasumHardwareSimulator.Global.WaitCursor())
            {
                s_current = this;
            }
            m_globalTimer = GlobalTimer.Instance;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Main Window Interface

        // ================================================================================================

        public string ApplicationIconPath
        {
            get { return FileManager.GetPathForBitmap("AppIcon.ico"); }
        }

        // --------------------------------------------------------------------------------------

        public string RevasumLogo
        {
            get { return FileManager.GetPathForBitmap("R.png"); }
        }

        // --------------------------------------------------------------------------------------

        public void ClosingApplication()
        {
            Publish(new ClosingApplication());
        }
        // ================================================================================================

        #endregion

        // ================================================================================================

        #region border color and header control

        // ================================================================================================

        string m_foregroundColor = "DarkSlateGray";
        public string ForegroundColor
        {
            get { return m_foregroundColor; }
            set
            {
                m_foregroundColor = value;
                OnPropertyChanged("ForegroundColor");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_BorderColor = "CadetBlue";
        public string BorderColor
        {
            get { return m_BorderColor; }
            set
            {
                m_BorderColor = value; 
                OnPropertyChanged("BorderColor");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_applicationHeaderText = "Revasum Hardware Simulator";
        public string ApplicationHeaderText
        {
            get { return m_applicationHeaderText; }
            set
            {
                m_applicationHeaderText = value;
                OnPropertyChanged("ApplicationHeaderText");
            }
        }

        // --------------------------------------------------------------------------------------

        bool m_TabChangeEnabled = true;
        public bool TabChangeEnabled
        {
            get { return m_TabChangeEnabled; }
            set
            {
                m_TabChangeEnabled = value;
                OnPropertyChanged("");
            }
        }

        // --------------------------------------------------------------------------------------

        public void ChangeOnLineStatus(OnLineStatusTypes newStatus)
        {
            switch (newStatus)
            {
                case OnLineStatusTypes.OffLine:
                    ApplicationHeaderText = "Revasum Hardware Simulator OFFLINE";
                    ForegroundColor = "DarkSlateGray";
                    BorderColor = "CadetBlue";
                    TabChangeEnabled = true;
                    break;
                case OnLineStatusTypes.OnLine:
                    ApplicationHeaderText = "Revasum Hardware Simulator ONLINE";
                    ForegroundColor = "Firebrick";
                     BorderColor = "DarkGoldenrod";
                    TabChangeEnabled = false;
                   break;
                case OnLineStatusTypes.NotEqual:
                case OnLineStatusTypes.Error:
                    ApplicationHeaderText = "Revasum Hardware Simulator ONLINE (Configurations not equal)";
                    ForegroundColor = "Red";
                     BorderColor = "Red";
                    TabChangeEnabled = false;
                   break;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
