﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{

    /// <summary>
    /// this is a superclass to all behavior presenters. It manages color selection for modified behavior. It saves, deletes, and closes behavior. It also controls enable and disable
    /// of Save and Delete buttons
    /// </summary>
    public abstract class EditBehaviorPresenter : PresenterBase<UserControl>
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public EditBehaviorPresenter()
        {
            IsDeleteButtonEnabled = HasAtLeastOneInput;
        }

        // --------------------------------------------------------------------------------------

        BehaviorTabPresenter TabPresenter
        {
            get { return BehaviorTabPresenter.Current; }
        }

        // --------------------------------------------------------------------------------------

        protected Behavior m_behavior;

        public virtual Behavior Behavior
        {
            get { return m_behavior; }
            set { m_behavior = value; }
        }

        // --------------------------------------------------------------------------------------

        public string BehaviorDescription
        {
            get { return Behavior.Description; }
            set
            {
            }
        }

        // --------------------------------------------------------------------------------------

        public string BehaviorName
        {
            get { return Behavior.QualifiedName; }
            set
            {
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region dirty color, save/delete buttons enable

        // ================================================================================================

        protected bool m_isDirty = false;
        public bool IsDirty
        {
            get { return m_isDirty; }
            set
            {
                m_isDirty = value;
                OnPropertyChanged("DirtyColor");
                OnPropertyChanged("IsDirty");
                IsDeleteButtonEnabled = HasAtLeastOneInput;
            }
        }

        // --------------------------------------------------------------------------------------

        public string DirtyColor
        {
            get { return IsDirty ? "Red" : "Blue"; }
        }

        // --------------------------------------------------------------------------------------

        bool m_isDeleteButtonEnabled;
        public bool IsDeleteButtonEnabled
        {
            get { return m_isDeleteButtonEnabled; }
            set
            {
                m_isDeleteButtonEnabled = value;
                OnPropertyChanged("IsDeleteButtonEnabled");
            }
        }

        // --------------------------------------------------------------------------------------

        public void Activated()
        {
            IsDeleteButtonEnabled = true;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region save, delete, close

        // ================================================================================================

        public void Save()
        {
            IOPoint aPoint = (IOPoint)Behavior.Target;
            aPoint.Behavior = Behavior;
            this.Publish(new BehaviorCreated(Behavior));
            this.Publish(new ConfigurationModified());
            IsDirty = false;
        }

        // --------------------------------------------------------------------------------------

        public void Delete(bool removingInput)
        {
            IOPoint aPoint = (IOPoint)Behavior.Target;
            aPoint.Behavior = null;
            aPoint.BehaviorAdded = false;
            RemoveAllInputs();
            IsDirty = false;
            IsDeleteButtonEnabled = HasAtLeastOneInput;
            this.Publish(new BehaviorDeleted(Behavior));
            this.Publish(new ConfigurationModified());
            Close(true);
        }

        // --------------------------------------------------------------------------------------

        public void Close(bool unconditional = false)
        {
            if (!unconditional)
            {
                if (IsDirty)   // we care about it only when inputs have been already assigned
                {
                    if (Dialog_YesNoSelection.AskForYesNoDecision(
                        "Behavior is Not Saved",
                        "if you close without saving, the changes will be discarded",
                        "Save",
                        "Discard"
                        ) == true)
                    {
                        Save();
                    }
                }
            }

            TabPresenter.CloseTab(this);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region choose a point as an input for the behavior

        // ================================================================================================

        /// <summary>
        /// intermediary between all Edit Behavior Views and BehaviorAllPointsPresenter - used to choose selected point as an input for a behavior
        /// </summary>
        public IOPoint SelectedAvailablePoint
        {
            get { return TabPresenter.SelectedAvailablePoint; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// intermediary between all Edit Behavior Views and BehaviorAllPointsPresenter - used to deselect current point once it has been successfully added to the behavior
        /// </summary>
        public void DeselectAvailablePoint()
        {
            TabPresenter.DeselectAvailablePoint();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract behavior inputs removal

        // ================================================================================================

        /// <summary>
        /// Make sure that all  attached inputs are disconnected from the Bahavior when the behavior is being deleted
        /// </summary>
        protected abstract void RemoveAllInputs();

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// We save and deleted a behavior ONLY when at least one input has been assigned to it. Otherwise we do not consider that behavior applicable
        /// </summary>
        protected abstract bool HasAtLeastOneInput { get; }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
