﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditPIControlBehaviorPresenter : EditBehaviorPresenter
    {
        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInput = ((Behavior_PI_Control)Behavior).Input;
                OnPropertyChanged("SelectedInput");
            }
        }

        // --------------------------------------------------------------------------------------

        IAnalogPoint m_selectedPoint;
        public IAnalogPoint SelectedInput
        {
            get { return m_selectedPoint; }
            set
            {
                if (Behavior.Target == value)
                {
                    Dialog_InvalidSelection.Show("Input for this Behavior cannot be the same as the Target");
                    return;
                }
                Behavior_PI_Control single = (Behavior_PI_Control)Behavior;
                if (single.Input != value)
                    IsDirty = true;
                single.Input = value;
                m_selectedPoint = value;
                OnPropertyChanged("SelectedInput");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region subclass specific

        // ================================================================================================
        public double Ki
        {
            get { return ((Behavior_PI_Control)Behavior).Ki; }
            set
            {
                ((Behavior_PI_Control)Behavior).Ki = value;
                IsDirty = true;
                OnPropertyChanged("Ki");
            }
        }

        // --------------------------------------------------------------------------------------

        public double Kp
        {
            get { return ((Behavior_PI_Control)Behavior).Kp; }
            set
            {
                ((Behavior_PI_Control)Behavior).Kp = value;
                IsDirty = true;
                OnPropertyChanged("Kp");
            }
        }

        // --------------------------------------------------------------------------------------

        public double OnOffThreshold
        {
            get { return ((Behavior_PI_Control)Behavior).OnOffThreshold; }
            set
            {
                ((Behavior_PI_Control)Behavior).OnOffThreshold = value;
                IsDirty = true;
                OnPropertyChanged("OnOffThreshold");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================


        protected override void RemoveAllInputs()
        {
            SelectedInput = null;
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInput != null; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}

