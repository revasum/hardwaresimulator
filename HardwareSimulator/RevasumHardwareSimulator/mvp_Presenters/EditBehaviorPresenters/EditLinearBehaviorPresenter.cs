﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditLinearBehaviorPresenter : EditBehaviorPresenter
    {

        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInput = ((Behavior_LINEAR)Behavior).Input;
                OnPropertyChanged("SelectedInput");
            }
        }

        // --------------------------------------------------------------------------------------

        IDigitalPoint m_selectedPoint;
        public IDigitalPoint SelectedInput
        {
            get { return m_selectedPoint; }
            set
            {

                if (Behavior.Target == value)
                {
                    Dialog_InvalidSelection.Show("Input for this Behavior cannot be the same as the Target");
                    return;
                }
                Behavior_LINEAR single = (Behavior_LINEAR)Behavior;
                if (single.Input != value)
                    IsDirty = true;
                single.Input = value;
                m_selectedPoint = value;
                OnPropertyChanged("SelectedInput");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region subclass specific

        // ================================================================================================

        public double Slope
        {
            get { return ((Behavior_LINEAR)Behavior).Slope; }
            set
            {
                ((Behavior_LINEAR)Behavior).Slope = value;
                IsDirty = true;
                OnPropertyChanged("Slope");
            }
        }

        // --------------------------------------------------------------------------------------

        public double Maximum
        {
            get { return ((Behavior_LINEAR)Behavior).Maximum; }
            set
            {
                ((Behavior_LINEAR)Behavior).Maximum = value;
                IsDirty = true;
                OnPropertyChanged("Maximum");
            }
        }

        // --------------------------------------------------------------------------------------

        public double Minimum
        {
            get { return ((Behavior_LINEAR)Behavior).Minimum; }
            set
            {
                ((Behavior_LINEAR)Behavior).Minimum = value;
                IsDirty = true;
                OnPropertyChanged("Minimum");
            }
        }

        // --------------------------------------------------------------------------------------

        public bool HoldLastValue
        {
            get { return ((Behavior_LINEAR)Behavior).HoldLastValue; }
            set
            {
                Behavior_LINEAR behavior = Behavior as Behavior_LINEAR;
                if (behavior.HoldLastValue != value)
                    IsDirty = true;
                behavior.HoldLastValue = value;
                OnPropertyChanged("HoldLastValue");
            }
        }

        // --------------------------------------------------------------------------------------

        public bool ReverseActing
        {
            get { return ((Behavior_LINEAR)Behavior).ReverseActing; }
            set
            {
                Behavior_LINEAR behavior = Behavior as Behavior_LINEAR;
                if (behavior.ReverseActing != value)
                    IsDirty = true;
                behavior.ReverseActing = value;
                OnPropertyChanged("ReverseActing");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================


        protected override void RemoveAllInputs()
        {
            SelectedInput = null;
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInput != null; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
