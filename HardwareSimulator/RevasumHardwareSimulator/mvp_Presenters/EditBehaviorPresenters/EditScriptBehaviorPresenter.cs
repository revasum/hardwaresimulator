﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditScriptBehaviorPresenter : EditBehaviorPresenter
    {
        ObservableCollection<IOPoint> m_points;
        public ObservableCollection<IOPoint> SelectedInputs
        {
            get { return m_points; }
            set
            {
                m_points = value;
                OnPropertyChanged("SelectedInputs");
            }
        }

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInputs = new ObservableCollection<IOPoint>(((Behavior_Script)Behavior).IOPoints);
                m_castBehavior = (Behavior_Script)value;
                m_castBehavior.ErrorMessageEvent += behavior_ErrorMessageEvent;
            }
        }

        private Behavior_Script m_castBehavior;

        public void AddIOPoint(IOPoint aPoint)
        {
            if(Behavior.Target == aPoint)
            {
                Dialog_InvalidSelection.Show("Input for this Behavior cannot be the same as the Target");
                return;
            }

            foreach (var existingPoint in m_castBehavior.IOPoints)
            {
                if (existingPoint == aPoint)
                {
                    Dialog_InvalidSelection.Show("This point was already selected");
                    return;
                }
            }

            SelectedInputs.Add(aPoint);
            m_castBehavior.AddPoint(aPoint);

            IsDirty = true; 
        }

        public void RemoveIOPoint(IOPoint aPoint)
        {
            SelectedInputs.Remove(aPoint);
            m_castBehavior.RemovePoint(aPoint);
            IsDirty = true;
            OnPropertyChanged("SelectedInputs");
        }
        
        protected override void RemoveAllInputs()
        {
            SelectedInputs = new ObservableCollection<IOPoint>();
            IsDirty = true;
            OnPropertyChanged("SelectedInputs");
        }

        public string Script
        {
            get { return m_castBehavior.Script;  }
            set
            {
                m_castBehavior.Script = value;
                IsDirty = true; 
                OnPropertyChanged("Script");
            }
        }

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInputs != null && SelectedInputs.Count > 0; }
        }

        public void TestScriptRun()
        {
            m_castBehavior.TestScriptRun();
        }

        private bool errorWindowAlreadyOpen = false;
        private Dialog_ErrorMessage errorDialog;
        private void behavior_ErrorMessageEvent(object sender, string e)
        {

            if (errorWindowAlreadyOpen)
                return;
            errorDialog = Dialog_ErrorMessage.Show(e);
            errorDialog.WindowClosed += ErrorDialog_WindowClosed;
        }

        private void ErrorDialog_WindowClosed(object sender, EventArgs e)
        {
            errorDialog.WindowClosed -= ErrorDialog_WindowClosed;
            errorWindowAlreadyOpen = false;
            errorDialog = null;
        }
    }
}
