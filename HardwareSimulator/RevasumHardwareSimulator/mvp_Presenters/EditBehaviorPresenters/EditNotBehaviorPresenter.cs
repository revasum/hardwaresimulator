﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditNotBehaviorPresenter : EditBehaviorPresenter
    {

        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInput = ((Behavior_NOT)Behavior).Input;
                OnPropertyChanged("SelectedInput");
            }
        }

        // --------------------------------------------------------------------------------------

        IDigitalPoint m_selectedPoint;
        public IDigitalPoint SelectedInput
        {
            get { return m_selectedPoint; }
            set
            {

                if (Behavior.Target == value)
                {
                    Dialog_InvalidSelection.Show("Input for this Behavior cannot be the same as the Target");
                    return;
                }
                Behavior_NOT single = (Behavior_NOT)Behavior;
                if (single.Input != value)
                    IsDirty = true;
                single.Input = value;
                m_selectedPoint = value;
                OnPropertyChanged("SelectedInput");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================

        protected override void RemoveAllInputs()
        {
            SelectedInput = null;
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInput != null; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region delays

        // ================================================================================================

        public int OnDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OnDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OnDelayTime = value;
                OnPropertyChanged("OnDelayTime");
                IsDirty = true;
            }
        }

        // --------------------------------------------------------------------------------------

        public int OffDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OffDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OffDelayTime = value;
                OnPropertyChanged("OffDelayTime");
                IsDirty = true;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
