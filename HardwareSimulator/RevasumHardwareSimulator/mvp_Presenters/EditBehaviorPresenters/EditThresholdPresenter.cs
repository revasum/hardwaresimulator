﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditThresholdPresenter : EditBehaviorPresenter
    {

        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInput = ((Behavior_THRESHOLD)Behavior).Input;
                OnPropertyChanged("SelectedInput");
            }
        }

        // --------------------------------------------------------------------------------------

        IAnalogPoint m_selectedPoint;
        public IAnalogPoint SelectedInput
        {
            get { return m_selectedPoint; }
            set
            {
                Behavior_THRESHOLD single = (Behavior_THRESHOLD)Behavior;
                if (single.Input != value)
                    IsDirty = true;
                single.Input = value;
                m_selectedPoint = value;
                OnPropertyChanged("SelectedInput");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region subclass specific

        // ================================================================================================

        public bool ReverseActing
        {
            get { return ((Behavior_THRESHOLD)Behavior).ReverseActing; }
            set
            {
                Behavior_THRESHOLD behavior = Behavior as Behavior_THRESHOLD;
                if (behavior.ReverseActing != value)
                    IsDirty = true;
                behavior.ReverseActing = value;
                OnPropertyChanged("ReverseActing");
            }
        }

        // --------------------------------------------------------------------------------------

        public double ThresholdValue
        {
            get { return ((Behavior_THRESHOLD)Behavior).ThresholdValue; }
            set
            {
                IsDirty = true;
                ((Behavior_THRESHOLD)Behavior).ThresholdValue = value;
                OnPropertyChanged("ThresholdValue");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================

        protected override void RemoveAllInputs()
        {
            SelectedInput = null;
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInput != null; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region delays

        // ================================================================================================

        public int OnDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OnDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OnDelayTime = value;
                OnPropertyChanged("OnDelayTime");
                IsDirty = true;
            }
        }

        // --------------------------------------------------------------------------------------

        public int OffDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OffDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OffDelayTime = value;
                OnPropertyChanged("OffDelayTime");
                IsDirty = true;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
