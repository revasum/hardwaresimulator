﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{

    /// <summary>
    /// used for behavior that have one or more digital inputs connected but a bacis logical operation (AND, OR)
    /// </summary>
    public class EditMultipleBehaviorPresenter : EditBehaviorPresenter
    {

        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInputs = new ObservableCollection<SelectedIDigitalPointTemplate>(((Behavior_ListInputs)Behavior).Inputs);
            }
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<SelectedIDigitalPointTemplate> m_points;
        public ObservableCollection<SelectedIDigitalPointTemplate> SelectedInputs
        {
            get { return m_points; }
            set
            {
                m_points = value;
                OnPropertyChanged("SelectedInputs");
            }
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

        #region add/remove a point - specific to multi-point presenter

        // ================================================================================================

        public void NewIOPoint(IDigitalPoint aPoint)
        {
            if (Behavior.Target == aPoint)
            {
                Dialog_InvalidSelection.Show("Input for this Behavior cannot be the same as the Target");
                return;
            }

            foreach (SelectedIDigitalPointTemplate existingPoint in ((Behavior_ListInputs)Behavior).Inputs)
            {
                if (existingPoint.Input == aPoint)
                {
                    Dialog_InvalidSelection.Show("This point was already selected");
                    return;
                }
            }

            SelectedIDigitalPointTemplate template = new SelectedIDigitalPointTemplate() { Input = aPoint, IsReverseActing = false, IsSelected = false };
            SelectedInputs.Add(template);
            ((Behavior_ListInputs)Behavior).AddInput(template);
            IsDirty = true;
        }

        // --------------------------------------------------------------------------------------

        //public void RemoveIOPoint(IDigitalPoint aPoint)
        //{
        //    SelectedIDigitalPointTemplate template = null;
        //    foreach (SelectedIDigitalPointTemplate point in SelectedInputs)
        //        if (template.Input == aPoint)
        //        {
        //            template = point;
        //            break;
        //        }

        //    if (template != null)
        //    {
        //        SelectedInputs.Remove(template);
        //        ((Behavior_ListInputs)Behavior).RemoveInput(aPoint);
        //        IsDirty = true;
        //        OnPropertyChanged("SelectedInputs");
        //    }
        //}


        // --------------------------------------------------------------------------------------

        public void RemvoveIOPoint(SelectedIDigitalPointTemplate aPoint)
        {
            SelectedInputs.Remove(aPoint);
            ((Behavior_ListInputs)Behavior).RemoveInput(aPoint);
            IsDirty = true;
            OnPropertyChanged("SelectedInputs");
        }
        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================

        protected override void RemoveAllInputs()
        {
            SelectedInputs = new ObservableCollection<SelectedIDigitalPointTemplate>();
            OnPropertyChanged("SelectedInputs");
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInputs != null && SelectedInputs.Count > 0; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region delays

        // ================================================================================================

        public int OnDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OnDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OnDelayTime = value;
                OnPropertyChanged("OnDelayTime");
                IsDirty = true;
            }
        }

        // --------------------------------------------------------------------------------------

        public int OffDelayTime
        {
            get { return ((Behavior_Digital)Behavior).OffDelayTime; }
            set
            {
                ((Behavior_Digital)Behavior).OffDelayTime = value;
                OnPropertyChanged("OffDelayTime");
                IsDirty = true;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
