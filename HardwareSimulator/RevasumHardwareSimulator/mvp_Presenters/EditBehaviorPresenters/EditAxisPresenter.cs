﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using System.Windows.Controls;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters
{
    public class EditAxisPresenter : EditBehaviorPresenter
    {

        // ================================================================================================

        #region common properties - Behavior and SelectedInput

        // ================================================================================================

        public override Behavior Behavior
        {
            get { return base.Behavior; }
            set
            {
                base.Behavior = value;
                SelectedInput = ((Behavior_AXIS)Behavior).Input;
                OnPropertyChanged("SelectedInput");
            }
        }

        // --------------------------------------------------------------------------------------

        IOPoint m_selectedPoint;
        public IOPoint SelectedInput
        {
            get { return m_selectedPoint; }
            set
            {
                Behavior_AXIS single = (Behavior_AXIS)Behavior;
                if (single.Input != value)
                    IsDirty = true;
                single.Input = value;
                m_selectedPoint = value;
                OnPropertyChanged("SelectedInput");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region abstract overrides

        // ================================================================================================

        protected override void RemoveAllInputs()
        {
            SelectedInput = null;
        }

        // --------------------------------------------------------------------------------------

        protected override bool HasAtLeastOneInput
        {
            get { return SelectedInput != null; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}

