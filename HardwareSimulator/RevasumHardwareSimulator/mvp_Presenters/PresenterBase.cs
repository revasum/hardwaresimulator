﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.mvp_Presenters
{

    public class PresenterBase<T> : RevasumBaseObject
    {
        private T m_view;

        public PresenterBase()
        {
        }

        public PresenterBase(T view)
        {
            m_view = view;
        }

        public T View
        {
            get { return m_view; }
            set { m_view = value; }
        }

    }
}
