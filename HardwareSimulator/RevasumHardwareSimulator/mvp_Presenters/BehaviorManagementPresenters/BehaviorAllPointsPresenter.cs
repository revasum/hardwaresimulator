﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Views.BehaviorManagement;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Notifiers;

namespace RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters
{
    /// <summary>
    /// This presenter services the right list of Behavior Tab.  Buy clicking on any of available points a user can add an input to current behavior (if applicable). The superclass 
    /// (BehaviorSortedPointsPresenterclass) implements sorting and searching functions.
    /// </summary>
    class BehaviorAllPointsPresenter : BehaviorSortedPointsPresenterclass<Behavior_AllPoints>, ISubscriber<ConfigurationReloaded>
    {

        // =======================================================================================================================

        #region Initialization

        // =======================================================================================================================

        private static BehaviorAllPointsPresenter s_current;
        public static BehaviorAllPointsPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public BehaviorAllPointsPresenter(Behavior_AllPoints view)
            : base(view)
        {
            s_current = this;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Inherited

        // =======================================================================================================================

        /// <summary>
        /// supply this virtual collection to superclass for searching and sorting
        /// </summary>
        protected override List<IOPoint> PointsSource 
        {
            get { return m_configurationManager.CurrentConfiguration.Points; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region ISubscriber

        // =======================================================================================================================

        public override void Receive(ConfigurationReloaded message)
        {
            base.Receive(message);
            m_selectedPoint = null;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Select Point

        // =======================================================================================================================

        IOPoint m_selectedPoint;
        public IOPoint SelectedPoint
        {
            get { return m_selectedPoint; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// select or unselect current point
        /// </summary>
        public void ButtonClicked(object buttonOwner)
        {
            IOPoint aPoint = buttonOwner as IOPoint;

            if (m_selectedPoint != null)
                m_selectedPoint.IsSelected = false; 
            if (m_selectedPoint == aPoint)
                m_selectedPoint = null;
            else
            {
                m_selectedPoint = aPoint;
                m_selectedPoint.IsSelected = true;
            }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// programatically unselect when the selection was chosen for a behavior
        /// </summary>
        public void Deselect()
        {
            if (m_selectedPoint != null)
            m_selectedPoint.IsSelected = false;
            m_selectedPoint = null;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================


    }
}
