﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RevasumHardwareSimulator.mvp_Views.BehaviorManagement;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using System.Collections.ObjectModel;

using RevasumHardwareSimulator.mvp_Presenters;

namespace RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters
{
    /// <summary>
    /// This presenter services the left list of Behavior Tab.  Buy clicking on any of available points a user can create a behavior. The superclass 
    /// (BehaviorSortedPointsPresenterclass) implements sorting and searching functions.

    /// </summary>
    class BehaviorInputsPresenter : BehaviorSortedPointsPresenterclass<Behavior_Inputs> 
    {

        // --------------------------------------------------------------------------------------

        private static BehaviorInputsPresenter s_current;
        public static BehaviorInputsPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public BehaviorInputsPresenter(Behavior_Inputs view)
            : base(view)
        {
            s_current = this;
        }

        // --------------------------------------------------------------------------------------

        protected override List<IOPoint> PointsSource
        {
            get { return m_configurationManager.CurrentConfiguration.Inputs; }
        }

        // --------------------------------------------------------------------------------------

        BehaviorTabPresenter TabPresenter
        {
            get { return BehaviorTabPresenter.Current; }
        }

        // --------------------------------------------------------------------------------------

        public void ButtonClicked(object buttonOwner)
        {
            IOPoint aPoint = buttonOwner as IOPoint;
            TabPresenter.InputSelected(aPoint);
        }

        // --------------------------------------------------------------------------------------

    }
}
