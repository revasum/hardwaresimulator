﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RevasumHardwareSimulator.mvp_Views.BehaviorManagement;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using System.Collections.ObjectModel;

using RevasumHardwareSimulator.mvp_Presenters;

namespace RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters
{
    /// <summary>
    /// this is a common superclass to BehaviorInputsPresenter and BehaviorAllPointsPresenter. It implements searching and sorting of an ovveriden list "PointsSource"
    /// </summary>
    abstract class BehaviorSortedPointsPresenterclass<T> : PresenterBase<T>, ISubscriber<ConfigurationReloaded>
    {

        // =======================================================================================================================

        #region Initialization

        // =======================================================================================================================

        protected ConfigurationManager m_configurationManager;

        // --------------------------------------------------------------------------------------

        public BehaviorSortedPointsPresenterclass(T view)
            : base(view)
        {
            m_configurationManager = ConfigurationManager.Current;
            Register<ConfigurationReloaded>(this);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region GUI Update

        // =======================================================================================================================

        protected abstract List<IOPoint> PointsSource { get; }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IOPoint> m_ObservablePoints;
        public ObservableCollection<IOPoint> ObservablePoints
        {
            get { return m_ObservablePoints; }
            set
            {
                m_ObservablePoints = value;
                OnPropertyChanged("ObservablePoints");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region ISubscriber

        // =======================================================================================================================

        public virtual void Receive(ConfigurationReloaded message)
        {
            ObservablePoints = new ObservableCollection<IOPoint>(PointsSource);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Sorting

        // =======================================================================================================================

        // cache sorted lists, and re-sort only if anything in the list changed
        List<IOPoint> m_sortedByName;
        List<IOPoint> m_sortedByModule;
        List<IOPoint> m_sortedByIO;

        // --------------------------------------------------------------------------------------

        void ResetSortResults()
        {
            m_sortedByName = null;
            m_sortedByModule = null;
            m_sortedByIO = null;
        }

        // --------------------------------------------------------------------------------------

        List<IOPoint> Sort(string sortBy)
        {
            List<IOPoint> sourcePoints = PointsSource;
            List<IOPoint> sortedPoints;

            switch (sortBy)
            {
                case "Name":
                    if (m_sortedByName == null)
                        m_sortedByName = sourcePoints.OrderBy(o => o.Name).ToList();
                    sortedPoints = m_sortedByName;
                    break;
                case "I/O Type":
                    if (m_sortedByIO == null)
                        m_sortedByIO = sourcePoints.OrderBy(o => o.QualifiedNameByIOType).ToList();
                    sortedPoints = m_sortedByIO;
                    break;
                default:
                    if (m_sortedByModule == null)
                        m_sortedByModule = sourcePoints.OrderBy(o => o.QualifiedNameByModule).ToList();
                    sortedPoints = m_sortedByModule;
                    break;
            }

            return sortedPoints;
        }

        // --------------------------------------------------------------------------------------

        public void Resort(string what)
        {
            List<IOPoint> sortedPoints = Sort(what);
            ObservablePoints = new ObservableCollection<IOPoint>(sortedPoints);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Searching

        // =======================================================================================================================

        List<IOPoint> FindByLookup(string searchString)
        {
            List<IOPoint> sourcePoints = PointsSource;
            IEnumerable<IOPoint> found =
                        from io in sourcePoints
                        where io.QualifiedNameByModule.IndexOf(searchString, StringComparison.OrdinalIgnoreCase) >= 0
                        select io;
            return found.ToList();
        }

        // --------------------------------------------------------------------------------------

        public void Search(string searchString)
        {
            List<IOPoint> complyingPoints = FindByLookup(searchString);
            ObservablePoints = new ObservableCollection<IOPoint>(complyingPoints);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
