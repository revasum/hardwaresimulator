﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

using RevasumHardwareSimulator.Dialogs;
using RevasumHardwareSimulator.mvp_Views.BehaviorManagement;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.BehaviorManagementPresenters
{

    /// <summary>
    /// This presenter manages the center part of the creation Behaviors section. Once a behavior was chosen it will be displayed in the central section
    /// </summary>
    class BehaviorTabPresenter : PresenterBase<BehaviorsTab>, ISubscriber<ConfigurationReloaded>
    {

        // =======================================================================================================================

        #region Initialization

        // =======================================================================================================================

        protected ConfigurationManager m_configurationManager;

        // --------------------------------------------------------------------------------------

        static BehaviorTabPresenter s_current;
        public static BehaviorTabPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public BehaviorTabPresenter(BehaviorsTab view)
            : base(view)
        {
            m_configurationManager = ConfigurationManager.Current;
            s_current = this;
            Register<ConfigurationReloaded>(this);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region ISubscriber

        // =======================================================================================================================


        public void Receive(ConfigurationReloaded message)
        {
            View.RemoveAllTabs();
        }

      // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Open / Close behavior

        // =======================================================================================================================

        public void InputSelected(IOPoint anInput)
        {

            // the control is sent here when an input (of the list on left) was chosen.
            // 1. present to user a list of available behaviors for this input, and have him choose one
            // 2. ask the BehaviorManager to create proper classes (model, view,presenter) based on the selected string (all done using reflections)
            // if selected input already has a behavior associated with it, open it

            if (View.TargetAlreadyOpen(anInput))
                return;

            EditBehaviorPresenter editPresenter = null;
            if (anInput.Behavior == null)
            {
                ObservableCollection<string> options = new ObservableCollection<string>(EditBehaviorManager.AllBehaviorNamesSupportingTarget(anInput));
                string result = Dialog_ChooseBehavior.AskForBehavior(options, anInput.Name, anInput.RevasumDataType.ToString());

                if (!String.IsNullOrEmpty(result))
                    editPresenter = EditBehaviorManager.GetPresenter(result, anInput);
                else
                    return;
            }
            else 
            {
                editPresenter = EditBehaviorManager.Activatepresenter(anInput);
                editPresenter.Activated();
            }

            View.AddTab(editPresenter);
        }

        // --------------------------------------------------------------------------------------

        public void CloseTab<T>(PresenterBase<T> presenter)
        {
            View.RemoveTab(presenter);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region access to selected points

        // =======================================================================================================================

        public IOPoint SelectedAvailablePoint
        {
            get { return BehaviorAllPointsPresenter.Current.SelectedPoint; }
        }

        // --------------------------------------------------------------------------------------

        public void DeselectAvailablePoint()
        {
            BehaviorAllPointsPresenter.Current.Deselect();
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
