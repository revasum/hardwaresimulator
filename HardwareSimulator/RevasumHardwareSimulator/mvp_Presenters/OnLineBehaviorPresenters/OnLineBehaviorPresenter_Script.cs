﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters
{
    public class OnLineBehaviorPresenter_Script : OnLineBehaviorPresenter
    {

        public OnLineBehaviorPresenter_Script()
        {

        }
        
        //update local GUI storage stucture when inputs change
        private void behavior_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            try
            {
                var behavior = (Behavior_Script)m_target.Behavior;
                for (int i = 0; i < behavior.IOPoints.Count; i++)
                {
                    InputValues[i].InputValue = behavior.IOPoints[i].ValueAsString;
                }
            }
            catch{}
        }

        public override IOPoint Target
        {
            get { return m_target; }
            set
            {
                m_target = value;
                TargetModified();
            }
        }

        private ObservableCollection<ScripInputPreseter> m_InputValues;
        public ObservableCollection<ScripInputPreseter> InputValues
        {
            get { return m_InputValues; }
            set
            {
                if (m_InputValues == value) return;
                m_InputValues = value;
                OnPropertyChanged("InputValues");
            }
        }
        
        protected override void TargetModified()
        {
            var behavior = (Behavior_Script)m_target.Behavior;
            
            if (Behavior == null)
                Close();

            if (InputValues == null || InputValues.Count != behavior.IOPoints.Count)
            {
                InputValues = new ObservableCollection<ScripInputPreseter>();
                foreach (var input in behavior.IOPoints)
                {
                    InputValues.Add(new ScripInputPreseter()
                    {
                        InputName = input.QualifiedName,
                        InputValue = input.ValueAsString
                    });
                }
                behavior.PropertyChanged += behavior_PropertyChanged;
            }
            else
            {
                for(int i = 0; i < behavior.IOPoints.Count; i++)
                {
                    InputValues[i].InputValue = behavior.IOPoints[i].ValueAsString;
                }
            }
        }
    }

    public class ScripInputPreseter : INotifyPropertyChanged
    {
        private string inputName;
        public string InputName
        {
            get { return inputName; }
            set
            {
                if (inputName == value) return;
                inputName = value;
                OnPropertyChanged("InputName");
            }
        }

        private string inputValue;
        public string InputValue
        {
            get { return inputValue; }
            set
            {
                if (inputValue == value) return;
                inputValue = value;
                OnPropertyChanged("InputValue");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
