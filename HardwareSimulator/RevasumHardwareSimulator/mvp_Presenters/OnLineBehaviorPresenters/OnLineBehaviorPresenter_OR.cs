﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Collections.ObjectModel;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.Global;


namespace RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters
{
    public class OnLineBehaviorPresenter_OR : OnLineBehaviorPresenter
    {


        // --------------------------------------------------------------------------------------

        public override IOPoint Target
        {
            get { return m_target; }
            set { 
                m_target = value;
                TargetModified();
            }
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<SelectedIDigitalPointTemplate> m_inputs;
        public ObservableCollection<SelectedIDigitalPointTemplate> Inputs
        {
            get { return m_inputs; }
            set
            {
                m_inputs = value;
                OnPropertyChanged("Inputs");
            }
        }

        // --------------------------------------------------------------------------------------

        protected override void TargetModified()
        {
            Behavior_ListInputs behavior = m_target.Behavior as Behavior_ListInputs;
            Behavior = behavior;
            if (behavior == null)
            {
                Inputs = new ObservableCollection<SelectedIDigitalPointTemplate>();
                Close();
            }
            else
                Inputs = new ObservableCollection<SelectedIDigitalPointTemplate>(behavior.Inputs);
        }

        // --------------------------------------------------------------------------------------

    }
}

