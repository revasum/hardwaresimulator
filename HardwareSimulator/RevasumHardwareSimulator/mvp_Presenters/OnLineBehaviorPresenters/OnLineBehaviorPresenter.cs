﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using RevasumHardwareSimulator.mvp_Presenters.OnLineManagementPresenters;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters
{
    public class OnLineBehaviorPresenter : PresenterBase<UserControl>, ISubscriber<ConfigurationModified>
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public OnLineBehaviorPresenter()
        {
            Register<ConfigurationModified>(this);
        }

        // --------------------------------------------------------------------------------------

        OnLineManagementPresenter TabPresenter
        {
            get { return OnLineManagementPresenter.Current; }
        }

        // --------------------------------------------------------------------------------------

        public void SetView(UserControl aView)
        {
            base.View = aView;
            aView.DataContext = this;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Models

        // ================================================================================================

        Behavior m_behavior;
        public virtual Behavior Behavior
        {
            get { return m_target.Behavior; }
            set
            {
                m_behavior = value;
                //OnPropertyChanged("Behavior");
                //m_behavior.CurrentOperationResult = "blah";
            }
        }

        // --------------------------------------------------------------------------------------

        public string BehaviorDescription
        {
            get { return m_target.Behavior.Description; }
            set
            {
            }
        }

        // --------------------------------------------------------------------------------------

        protected IOPoint m_target;
        public virtual IOPoint Target
        {
            get { return m_target; }
            set { m_target = value; }
        }

        // --------------------------------------------------------------------------------------

        public string TargetName
        {
            get { return m_target.QualifiedName; }
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

        #region ISubscriber

        // =======================================================================================================================

        public void Receive(ConfigurationModified message)
        {
            TargetModified();
        }

        // --------------------------------------------------------------------------------------

        protected virtual void TargetModified()
        {
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================


        public void Close()
        {
            TabPresenter.Close(this);
        }

        // =======================================================================================================================


        //string m_behaviorState = "no idea";
        //public string BehaviorState
        //{
        //    get { return m_behaviorState; }
        //    set
        //    {
        //        m_behaviorState = value;
        //        OnPropertyChanged("BehaviorState");
        //    }
        //}

    }
}
