﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Views.OnLineManagement;
using RevasumHardwareSimulator.mvp_Models;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters;
using System.Diagnostics;
using System.Threading.Tasks;

namespace RevasumHardwareSimulator.mvp_Presenters.OnLineManagementPresenters
{
    public class OnLineManagementPresenter : PresenterBase<MonitorTab>, ISubscriber<ConfigurationReloaded>, ISubscriber<ConfigurationModified>
    {

        bool m_isSimulationRunning = false;
        List<IOPoint> m_currentConfiguredPoints;
        readonly string COLOR_OK = "DarkGoldenrod";
        readonly string COLOR_NG = "Red";
        readonly string COLOR_STOP = "White";

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        private static OnLineManagementPresenter s_current;
        public static OnLineManagementPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public OnLineManagementPresenter(MonitorTab view)
            : base(view)
        {
            s_current = this;
            ShowStartButton();
            Register<ConfigurationReloaded>(this);
            Register<ConfigurationModified>(this);
        }

        // ================================================================================================

        #endregion

        // =======================================================================================================================

        #region ISubscriber

        // =======================================================================================================================

        public void Receive(ConfigurationReloaded message)
        {
            View.RemoveAllTabs();
            m_currentConfiguredPoints = ConfigurationManager.Current.CurrentConfigurationPoints;
            if (m_currentConfiguredPoints.Count > 0)
            {
                SelectDigitalPoints(m_currentConfiguredPoints);
                SelectAnalogPoints(m_currentConfiguredPoints);
                SelectAxisStructurePoints(m_currentConfiguredPoints);
            }
        }

        // --------------------------------------------------------------------------------------

        public void Receive(ConfigurationModified message)
        {
            View.RemoveAllTabs();
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region GUI Interfaces

        // ================================================================================================

        private Stopwatch _stopwatch = new Stopwatch();

        public String ElapsedTime
        {
            get
            {
                TimeSpan ts = _stopwatch.Elapsed;

                return string.Format("{0:D2}:{1:D3}", ts.Seconds, ts.Milliseconds);
            }
        }

        string m_startStopIcon;
        public string StartStopIcon
        {
            get { return m_startStopIcon; }
            set
            {
                m_startStopIcon = value;
                OnPropertyChanged("StartStopIcon");
            }
        }

        // --------------------------------------------------------------------------------------

        bool m_IsMonitorCheckboxEnabled = true;
        public bool IsMonitorCheckboxEnabled
        {
            get { return m_IsMonitorCheckboxEnabled; }
            set
            {
                m_IsMonitorCheckboxEnabled = value;
                OnPropertyChanged("IsMonitorCheckboxEnabled");
            }
        }

        // --------------------------------------------------------------------------------------

        bool m_IsStartButtonEnabled = true;
        public bool IsStartButtonEnabled
        {
            get { return m_IsStartButtonEnabled; }
            set
            {
                m_IsStartButtonEnabled = value;
                OnPropertyChanged("IsStartButtonEnabled");
            }
        }

        // --------------------------------------------------------------------------------------

        bool m_IsStopButtonEnabled = true;
        public bool IsStopButtonEnabled
        {
            get { return m_IsStopButtonEnabled; }
            set
            {
                m_IsStopButtonEnabled = value;
                OnPropertyChanged("IsStopButtonEnabled");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_borderColor;
        public string BorderColor
        {
            get { return m_borderColor; }
            set
            {
                m_borderColor = value;
                OnPropertyChanged("BorderColor");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Start / Stop Simulation


        /// <summary>
        ///     Start Button starts here
        /// </summary>
        public async void StartStopSimulationAsync()
        {
            _stopwatch.Restart();

            try
            {

                // NOTE: start/stop request is a toggle
                if (m_isSimulationRunning)
                {
                    StopSimulation();

                    _stopwatch.Stop();
                }
                else
                {
                    if (m_currentConfiguredPoints == null || m_currentConfiguredPoints.Count == 0)
                    {
                        ApplicationPresenter.Current.ChangeOnLineStatus(OnLineStatusTypes.OffLine);

                        return;
                    }

                    OnLineStatusTypes onLineStatus = OnLineStatusTypes.None;

                    // 500 ms
                    await Task.Run(() =>
                    {
                        PlcAccessor.Instance().LoadPLC();
                    });

                    OnPropertyChanged("ElapsedTime");

                    IOPointList plcPoints = PlcAccessor.Instance().PLCPoints;

                    if (plcPoints.Count < 2)
                    {
                        ApplicationPresenter.Current.ChangeOnLineStatus(OnLineStatusTypes.OffLine);

                        return;
                    }

                    ShowStopButton();

                    IOConfiguration configuration = ConfigurationManager.Current.CurrentConfiguration;

                    List<IOPoint> originalPoints = configuration.OriginalPointOnly;

                    // 2,139 ms
                    bool handlesOK = false;

                    await Task.Run(() =>
                    {
                        handlesOK = PlcAccessor.Instance().ConnectToConfiguration(originalPoints);
                    });

                    OnPropertyChanged("ElapsedTime");

                    if ((plcPoints.Count == originalPoints.Count) && (plcPoints.PLCComment == configuration.PLCComment))
                    {
                        if (handlesOK)
                        {
                            onLineStatus = OnLineStatusTypes.OnLine;
                        }
                        else
                        {
                            onLineStatus = OnLineStatusTypes.HandlesError;
                        }
                    }
                    else
                    {
                        onLineStatus = OnLineStatusTypes.NotEqual;
                    }

                    // 400 ms
                    await Task.Run(() =>
                    {
                        StartSimulation(onLineStatus);
                    });

                    OnPropertyChanged("ElapsedTime");
                }

                IsMonitorCheckboxEnabled = m_isSimulationRunning;
                m_isSimulationRunning = !m_isSimulationRunning;
            }
            finally
            {
                _stopwatch.Stop();

                OnPropertyChanged("ElapsedTime");
            }
        }

        // --------------------------------------------------------------------------------------

        void ShowStartButton()
        {
            StartStopIcon = FileManager.GetPathForBitmap("StartButton2.png");
        }

        // --------------------------------------------------------------------------------------

        void ShowStopButton()
        {
            StartStopIcon = FileManager.GetPathForBitmap("StopButton2.png");
        }

        // --------------------------------------------------------------------------------------

        void StartSimulation(OnLineStatusTypes status)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            Console.WriteLine($"Started: {stopwatch.Elapsed}");

            string borderColor = status == OnLineStatusTypes.OnLine ? COLOR_OK : COLOR_NG;
            ApplicationPresenter.Current.ChangeOnLineStatus(status);
            BorderColor = borderColor;



            Console.WriteLine($"point.SetInitialValue(): {stopwatch.Elapsed}");

            //foreach (IOPoint point in m_currentConfiguredPoints)
            System.Threading.Tasks.Parallel.ForEach(m_currentConfiguredPoints, point =>
            {
                point.SetInitialValue();
            });

            Console.WriteLine($"point.OnLine(): {stopwatch.Elapsed}");

            //foreach (IOPoint point in m_currentConfiguredPoints)
            System.Threading.Tasks.Parallel.ForEach(m_currentConfiguredPoints, point =>
            {
                point.OnLine();
            });

            Console.WriteLine($"GlobalTimer.Instance.OnLine(): {stopwatch.Elapsed}");

            GlobalTimer.Instance.OnLine();

            Console.WriteLine($"Completed: {stopwatch.Elapsed}");
        }

        // --------------------------------------------------------------------------------------

        void StopSimulation()
        {
            GlobalTimer.Instance.OffLine();

            ShowStartButton();

            ApplicationPresenter.Current.ChangeOnLineStatus(OnLineStatusTypes.OffLine);
            BorderColor = COLOR_STOP;

            System.Threading.Tasks.Parallel.ForEach(m_currentConfiguredPoints, point =>
            {
               point.OffLine();
            });
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Point Collections

        // ================================================================================================

        ObservableCollection<IDigitalPoint> m_digitalPoints;
        public ObservableCollection<IDigitalPoint> DigitalPoints
        {
            get { return m_digitalPoints; }
            set
            {
                m_digitalPoints = value;
                OnPropertyChanged("DigitalPoints");
            }
        }

        // --------------------------------------------------------------------------------------

        void SelectDigitalPoints(List<IOPoint> aList)
        {
            ObservableCollection<IDigitalPoint> selected = new ObservableCollection<IDigitalPoint>();
            foreach (IOPoint point in aList)
            {
                if (point.IsDigital)
                {
                    IDigitalPoint digital = point as IDigitalPoint;
                    selected.Add(digital);
                }
            }
            DigitalPoints = selected;
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IAnalogPoint> m_analogPoints;
        public ObservableCollection<IAnalogPoint> AnalogPoints
        {
            get { return m_analogPoints; }
            set
            {
                m_analogPoints = value;
                OnPropertyChanged("AnalogPoints");
            }
        }

        // --------------------------------------------------------------------------------------

        void SelectAnalogPoints(List<IOPoint> aList)
        {
            ObservableCollection<IAnalogPoint> selected = new ObservableCollection<IAnalogPoint>();
            foreach (IOPoint point in aList)
            {
                if (point.IsAnalog)
                {
                    IAnalogPoint analog = point as IAnalogPoint;
                    selected.Add(analog);
                }
            }
            AnalogPoints = selected;
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IAxisPoint> m_axisStructurePoints;
        public ObservableCollection<IAxisPoint> AxisStructurePoints
        {
            get { return m_axisStructurePoints; }
            set
            {
                m_axisStructurePoints = value;
                OnPropertyChanged("AxisStructurePoints");
            }
        }

        // --------------------------------------------------------------------------------------

        void SelectAxisStructurePoints(List<IOPoint> aList)
        {
            ObservableCollection<IAxisPoint> selected = new ObservableCollection<IAxisPoint>();
            foreach (IOPoint point in aList)
            {
                if (point.IsAxisCommandStructure)
                {
                    IAxisPoint axis = point as IAxisPoint;
                    selected.Add(axis);
                }
            }
            AxisStructurePoints = selected;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Show Selected Point

        // ================================================================================================

        public void ButtonClicked(object buttonOwner)
        {
            IOPoint aPoint = buttonOwner as IOPoint;
            if (aPoint.BehaviorAdded)
            {
                OnLineBehaviorPresenter onlinePresenter = OnLineBehaviorManager.GetPresenter(aPoint);
                View.AddTab(onlinePresenter);
            }
        }

        // --------------------------------------------------------------------------------------

        public void Close(OnLineBehaviorPresenter aPresenter)
        {
            View.RemoveTab(aPresenter);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Speed Slider Interface

        // ================================================================================================

        public void NewSliderValue(double aValue)
        {
            SliderOperation = aValue > 0.8 ? "*" : "/";

            // it may be hard to hit perfectly 1 - we will round values in the range 0.8 to 1.2 
            if (aValue < 0.8)
                SliderConversion = 1 / aValue;
            else if (aValue < 1.2)
                SliderConversion = 1;
            else
                SliderConversion = aValue;

            GlobalTimer.Instance.SpeedUpFactor = aValue;
        }

        // --------------------------------------------------------------------------------------

        double m_sliderConversion = 1;
        public double SliderConversion
        {
            get { return m_sliderConversion; }
            set
            {
                m_sliderConversion = value;
                OnPropertyChanged("SliderConversion");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_SliderOperation = "*";
        public string SliderOperation
        {
            get { return m_SliderOperation; }
            set
            {
                m_SliderOperation = value;
                OnPropertyChanged("SliderOperation");
            }
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

        public void Break()
        {
        }

        // --------------------------------------------------------------------------------------


    }
}
