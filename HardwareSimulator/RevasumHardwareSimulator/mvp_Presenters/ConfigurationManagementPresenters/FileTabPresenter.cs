﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.IO;

using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Views.ConfigurationManagement;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.Dialogs;
using XMLSupport;
using Microsoft.Win32;

namespace RevasumHardwareSimulator.mvp_Presenters.ConfigurationManagementPresenters
{
    /// <summary>
    /// restore most recent configuration OR choose a configuration from file OR create a new configuration from PLC
    /// </summary>
    public class FileTabPresenter : PresenterBase<FileTab>,
        ISubscriber<ConfigurationModified>,
        ISubscriber<BehaviorCreated>,
        ISubscriber<BehaviorDeleted>,
        ISubscriber<ClosingApplication>
    {

        private readonly ConfigurationManager m_configurationManager;

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        private static FileTabPresenter s_current;
        public static FileTabPresenter Current
        {
            get { return s_current; }
        }

        // --------------------------------------------------------------------------------------

        public FileTabPresenter(FileTab view)
            : base(view)
        {
            s_current = this;
            m_configurationManager = ConfigurationManager.Current;
            Register<ConfigurationModified>(this);
            Register<BehaviorCreated>(this);
            Register<BehaviorDeleted>(this);
            Register<ClosingApplication>(this);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region supported ObservableCollections

        // ================================================================================================

        private void SetSupportedCollections()
        {
            CurrentConfigurationPoints = m_configurationManager.CurrentConfiguration.AsObservableCollection;
            AllDigitalPoints = m_configurationManager.CurrentConfiguration.DigitalPointsAsObservableCollection;
            AllAnalogPoints = m_configurationManager.CurrentConfiguration.AnalogPointsAsObservableCollection;
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IOPoint> m_currentConfigurationPoints = new ObservableCollection<IOPoint>();
        public ObservableCollection<IOPoint> CurrentConfigurationPoints
        {
            get { return m_currentConfigurationPoints; }
            set
            {
                m_currentConfigurationPoints = value;
                OnPropertyChanged("CurrentConfigurationPoints");
            }
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IDigitalPoint> m_AllDigitalPoints = new ObservableCollection<IDigitalPoint>();
        public ObservableCollection<IDigitalPoint> AllDigitalPoints
        {
            get { return m_AllDigitalPoints; }
            set
            {
                m_AllDigitalPoints = value;
                OnPropertyChanged("AllDigitalPoints");
            }
        }

        // --------------------------------------------------------------------------------------

        ObservableCollection<IAnalogPoint> m_AllAnalogPoints = new ObservableCollection<IAnalogPoint>();
        public ObservableCollection<IAnalogPoint> AllAnalogPoints
        {
            get { return m_AllAnalogPoints; }
            set
            {
                m_AllAnalogPoints = value;
                OnPropertyChanged("AllAnalogPoints");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Pushbuttons

        // ================================================================================================

        public void LoadlatestRequest()
        {
            if (m_configurationManager.Restore())
                NewConfigurationLoaded();
        }

        // --------------------------------------------------------------------------------------

        public void LoadFromPLCRequest()
        {
            if (PlcAccessor.Instance().LoadPLC())
            {
                OpenFileDialog dlg = new OpenFileDialog()
                {
                    Filter = "XML Files (*.xml)|*.xml",
                    InitialDirectory = Properties.Settings.Default.previousLoadFromPLCRequestFilename,
                    Title = "Select configuration file to merge with..."
                };

                if ((bool)dlg.ShowDialog())
                {
                    XMLNode node = XMLNode.FromFile(dlg.FileName);
                    IOConfiguration config = new IOConfiguration(node);
                    config.ProjectName = dlg.FileName;

                    // save the path for the next time we need it
                    Properties.Settings.Default.previousLoadFromPLCRequestFilename = dlg.FileName;
                    Properties.Settings.Default.Save();


                    IOPointList plcPoints = PlcAccessor.Instance().PLCPoints;


                    IOPointList pointsToAdd = new IOPointList();
                    IOPointList pointsToRemove = new IOPointList();

                    foreach (IOPoint plcPoint1 in plcPoints.AllPoints)
                    {
                        IOPoint xmlPoint1 = config.Points.Find(x => x.QualifiedName == plcPoint1.QualifiedName);
                        if (xmlPoint1 == null)
                        {
                            pointsToAdd.Add(plcPoint1);
                        }
                    }

                    foreach (IOPoint xmlPoint2 in config.Points)
                    {
                        IOPoint plcPoint2 = plcPoints.AllPoints.Find(x => x.QualifiedName == xmlPoint2.QualifiedName);
                        if (plcPoint2 == null)
                        {
                            //IOPoint ogPoint = config.OriginalPointOnly.Find(x => x.QualifiedName == xmlPoint2.QualifiedName);
                            //if (ogPoint != null)
                            //{
                                pointsToRemove.Add(xmlPoint2);
                            //}
                        }
                    }

                    foreach (IOPoint pointToRemove in pointsToRemove.AllPoints)
                        config.Points.Remove(pointToRemove);

                    foreach (IOPoint pointToAdd in pointsToAdd.AllPoints)
                        config.Points.Add(pointToAdd);

                    config.Points.Sort((x, y) => x.QualifiedNameByModule.CompareTo(y.QualifiedNameByModule));
                    ConfigurationManager.Current.CurrentConfiguration = config;
                }
                else
                {
                    string projectName = Dialog_EnterProjectName.AskForProjectName();
                    if (!String.IsNullOrEmpty(projectName))
                    {
                        m_configurationManager.CreateNewConfiguration(PlcAccessor.Instance().PLCPoints, projectName);
                        NewConfigurationLoaded();
                    }
                }



            }
            else
                ConfigurationLoadedVisibility = "Hidden";
        }

        // --------------------------------------------------------------------------------------

        public void LoadFromFileRequest()
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog
            {
                Filter = "XML Files (*.xml)|*.xml",
                InitialDirectory = Properties.Settings.Default.previousLoadFromFileRequestFilename,
                Title = "Select a configuration file to load..."
            };

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                IOConfiguration config = m_configurationManager.Restore(dlg.FileName);
                m_configurationManager.SetCurrentFile(config);
                NewConfigurationLoaded();

                // save the path for the next time we need it
                Properties.Settings.Default.previousLoadFromFileRequestFilename = dlg.FileName;
                Properties.Settings.Default.Save();
            }
        }

        // --------------------------------------------------------------------------------------

        public void MergeConfigurationsRequest()
        {
            if (m_configurationManager.ConfigurationExists)
            {
                var dlg = new Microsoft.Win32.OpenFileDialog
                {
                    Filter = "XML Files (*.xml)|*.xml",
                    InitialDirectory = Properties.Settings.Default.previousMergeConfigurationsRequestFilename,
                    Title = "Select a configuration file to merge with..."
                };

                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    //IOConfiguration source = m_configurationManager.Restore(dlg.FileName);
                    XMLNode node = XMLNode.FromFile(dlg.FileName);
                    IOConfiguration source = new IOConfiguration(node);
                    //IOConfiguration tmp = new IOConfiguration();
                    source.ProjectName = dlg.FileName;
                    Dialog_SourceToMerge dialog = new Dialog_SourceToMerge(source);
                    IsAboutExpanded = true;
                    if (dialog.ShowDialog() == true)
                    {
                        //MergeConfigurations(source);

                        MergeConfigurationsNew(source);
                    }

                    // save the path for the next time we need it
                    Properties.Settings.Default.previousMergeConfigurationsRequestFilename = dlg.FileName;
                    Properties.Settings.Default.Save();
                }
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region About Box

        // ================================================================================================

        void SetAboutBoxData()
        {
            NumberOfPoints = m_configurationManager.CurrentConfigurationPoints.Count;
            ConfigurationComment = m_configurationManager.CurrentConfiguration.ConfigurationComment;
            PLCComment = m_configurationManager.CurrentConfiguration.PLCComment;
            ProjectName = m_configurationManager.CurrentConfiguration.ProjectName;
            ConfigurationCreated = DateToString(m_configurationManager.CurrentConfigurationCreationTime);
            ConfigurationModified = DateToString(m_configurationManager.CurrentConfigurationModificationTime);
            NumberOfBehaviors = m_configurationManager.NumberOfBehaviors;
            NumberOfDefaults = m_configurationManager.NumberOfDefaults;
        }

        // --------------------------------------------------------------------------------------

        //string DateToString(DateTime timeStamp)
        //{
        //    string answer = String.Format("on {0} at {1}", timeStamp.ToString("MM/dd/yy"), timeStamp.ToString("HH:mm:ss"));
        //    return answer;
        //}

        string DateToString(DateTime timeStamp)
        {
            return ConfigurationManager.DateToString(timeStamp);
        }

        // --------------------------------------------------------------------------------------

        string m_projectName;
        public string ProjectName
        {
            get { return m_projectName; }
            set
            {
                m_projectName = value;
                OnPropertyChanged("ProjectName");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_ConfigurationCreated;
        public string ConfigurationCreated
        {
            get { return m_ConfigurationCreated; }
            set
            {
                m_ConfigurationCreated = value;
                OnPropertyChanged("ConfigurationCreated");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_ConfigurationModified;
        public string ConfigurationModified
        {
            get { return m_ConfigurationModified; }
            set
            {
                m_ConfigurationModified = value;
                OnPropertyChanged("ConfigurationModified");
            }
        }

        // --------------------------------------------------------------------------------------

        int m_NumberOfPoints;
        public int NumberOfPoints
        {
            get { return m_NumberOfPoints; }
            set
            {
                m_NumberOfPoints = value;
                OnPropertyChanged("NumberOfPoints");
            }
        }

        // --------------------------------------------------------------------------------------

        int m_NumberOfBehaviors;
        public int NumberOfBehaviors
        {
            get { return m_NumberOfBehaviors; }
            set
            {
                m_NumberOfBehaviors = value;
                OnPropertyChanged("NumberOfBehaviors");
            }
        }

        // --------------------------------------------------------------------------------------

        int m_NumberOfDefaults;
        public int NumberOfDefaults
        {
            get { return m_NumberOfDefaults; }
            set
            {
                m_NumberOfDefaults = value;
                OnPropertyChanged("NumberOfDefaults");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_ConfigurationComment;
        public string ConfigurationComment
        {
            get { return m_ConfigurationComment; }
            set
            {
                m_ConfigurationComment = value;
                OnPropertyChanged("ConfigurationComment");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_PLCComment;
        public string PLCComment
        {
            get { return m_PLCComment; }
            set
            {
                m_PLCComment = value;
                OnPropertyChanged("PLCComment");
            }
        }

        // --------------------------------------------------------------------------------------

        bool m_isAboutExpanded = false;
        public bool IsAboutExpanded
        {
            get
            { return m_isAboutExpanded; }

            set
            {
                m_isAboutExpanded = value;
                OnPropertyChanged("IsAboutExpanded");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region ISubscriber

        // ================================================================================================

        public void Receive(ConfigurationModified message)
        {
            ConfigurationModified = DateToString(m_configurationManager.CurrentConfigurationModificationTime);
        }

        // --------------------------------------------------------------------------------------

        public void Receive(BehaviorCreated message)
        {
            NumberOfBehaviors = m_configurationManager.NumberOfBehaviors;
        }

        // --------------------------------------------------------------------------------------

        public void Receive(BehaviorDeleted message)
        {
            NumberOfBehaviors = m_configurationManager.NumberOfBehaviors;
        }

        // --------------------------------------------------------------------------------------

        public void Receive(ClosingApplication message)
        {
            ExitingScreen();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Change Expanders Colors

        // ================================================================================================

        string m_digitalExpanderColor = "Black";
        public string DigitalExpanderColor
        {
            get { return m_digitalExpanderColor; }
            set
            {
                m_digitalExpanderColor = value;
                OnPropertyChanged("DigitalExpanderColor");
            }
        }

        // --------------------------------------------------------------------------------------

        string m_analogExpanderColor = "Black";
        public string AnalogExpanderColor
        {
            get { return m_analogExpanderColor; }
            set
            {
                m_analogExpanderColor = value;
                OnPropertyChanged("AnalogExpanderColor");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region vaarious methods dealing with the preservation of modifications

        // ================================================================================================

        /// <summary>
        /// Reset all and update lists and about box based on recently re-loaded configuration
        /// </summary>
        public void NewConfigurationLoaded()
        {
            using (new RevasumHardwareSimulator.Global.WaitCursor())
            {
                ConfigurationLoadedVisibility = "Visible";
                Publish(new ConfigurationReloaded());
                SetSupportedCollections();
                SetAboutBoxData();
            }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// mark the data as "dirty".  SAving will be required upon exit
        /// </summary>
        public void DefaultsChanged()
        {
            m_defaultsChanged = true;
        }

        bool m_defaultsChanged = false;

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// make the central part of the screen invisible until a configuration is loaded.  (Clear it also when an unsuccessful attempt to load from PLC was made)
        /// </summary>
        string m_ConfigurationLoadedVisibility = "Hidden";
        public string ConfigurationLoadedVisibility
        {
            get { return m_ConfigurationLoadedVisibility; }
            set
            {
                m_ConfigurationLoadedVisibility = value;
                OnPropertyChanged("ConfigurationLoadedVisibility");
            }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// save configuration and update collection when changes were made to initial values.  
        /// NOTE: Rather then keep saving every time user changes a value, we do it when he is Done with all the changes, i.e. closes digital or analog Initial Data Expander.
        /// NOTE: In the case user closed the application after making changes - we also capture this condition through Receive(ClosingApplication).
        /// </summary>
        public void ExitingScreen()
        {
            if (m_defaultsChanged)
            {
                m_configurationManager.Save();
                NewConfigurationLoaded();
            }
            m_defaultsChanged = false;
        }

        // --------------------------------------------------------------------------------------

        public void SaveConfigurationComments()
        {
            m_configurationManager.CurrentConfiguration.ConfigurationComment = ConfigurationComment;
            Publish(new ConfigurationModified());
        }

        // --------------------------------------------------------------------------------------

        public void BackupCurrentConfiguration()
        {
            if (m_configurationManager.ConfigurationExists)
                m_configurationManager.Backup();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Merge Configurations

        // ================================================================================================

        void MergeConfigurations(IOConfiguration source)
        {
            int newBehaviors = 0;
            int newDefaults = 0;
            string folder = FileManager.ConfigurationFilesFolderName;
            string path = folder + "\\MergeResult.txt";
            ProjectName = source.ProjectName;
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(String.Format("Merging source file created on {0}", source.CreationTIme));
                writer.WriteLine(String.Format("  into current configuration (last modified on {0})", m_configurationManager.CurrentConfigurationModificationTime));
                writer.WriteLine();
                writer.WriteLine("-------------------------------------------------------------");
                writer.WriteLine();

                foreach (IOPoint sourcePoint in source.Points)
                {
                    IOPoint targetPoint = FindPointNamed(sourcePoint.QualifiedName);
                    if (targetPoint == null)
                        writer.WriteLine("*** missing point: " + sourcePoint.QualifiedName);
                    else
                    {
                        if (sourcePoint.BehaviorAdded)
                            if (MergeBehavior(sourcePoint, targetPoint, writer)) newBehaviors++;
                        if (sourcePoint.HasDefault)
                            if (MergeDefault(sourcePoint, targetPoint, writer)) newDefaults++;
                    }
                }

                writer.Close();
            }
            if (newBehaviors > 0 || newDefaults > 0)
            {
                ModifyConfigurationComment(source, newBehaviors, newDefaults);
                Publish(new ConfigurationModified());
                SetAboutBoxData();
            }
        }

        void MergeConfigurationsNew(IOConfiguration source)
        {
            int newBehaviors = 0;
            int newDefaults = 0;
            string folder = FileManager.ConfigurationFilesFolderName;
            string path = folder + "\\MergeResult.txt";
            ProjectName = source.ProjectName;
            using (var writer = new StreamWriter(path))
            {
                writer.WriteLine(String.Format("Merging source file created on {0}", source.CreationTIme));
                writer.WriteLine(String.Format("  into current configuration (last modified on {0})", m_configurationManager.CurrentConfigurationModificationTime));
                writer.WriteLine();
                writer.WriteLine("-------------------------------------------------------------");
                writer.WriteLine();

                foreach (IOPoint sourcePoint in source.Points)
                {
                    IOPoint targetPoint = FindPointNamed(sourcePoint.QualifiedName);
                    if (targetPoint == null)
                        writer.WriteLine("*** missing point: " + sourcePoint.QualifiedName);
                    else
                    {
                        if (sourcePoint.BehaviorAdded)
                        { 
                            targetPoint = sourcePoint;
                            newBehaviors++;
                        }
                        else if (sourcePoint.HasDefault)
                        {
                            targetPoint = sourcePoint;
                            newDefaults++;
                        }
                    }
                }

                writer.Close();
            }
            if (newBehaviors > 0 || newDefaults > 0)
            {
                ModifyConfigurationComment(source, newBehaviors, newDefaults);
                Publish(new ConfigurationModified());
                SetAboutBoxData();
            }
        }

        // --------------------------------------------------------------------------------------

        IOPoint FindPointNamed(string qualifiedName)
        {
            foreach (IOPoint point in m_configurationManager.CurrentConfigurationPoints)
                if (point.QualifiedName == qualifiedName)
                    return point;
            return null;
        }

        // --------------------------------------------------------------------------------------

        bool MergeBehavior(IOPoint sourcePoint, IOPoint targetPoint, StreamWriter writter)
        {
            if (targetPoint.BehaviorAdded)
            {
                writter.WriteLine("--- skipped (behavior already exisits): " + targetPoint.QualifiedName);
                return false;
            }

            writter.WriteLine("+++ added behavior: " + targetPoint.QualifiedName);
            targetPoint.Behavior = sourcePoint.Behavior;
            sourcePoint.Behavior.Target = targetPoint;
            Publish(new BehaviorCreated(targetPoint.Behavior));
            return true;

        }

        // --------------------------------------------------------------------------------------

        bool MergeDefault(IOPoint sourcePoint, IOPoint targetPoint, StreamWriter writter)
        {
            if (targetPoint.HasDefault)
            {
                writter.WriteLine("--- skipped (default value already exisits): " + targetPoint.QualifiedName);
                return false;
            }
            writter.WriteLine("+++ added default value: " + targetPoint.QualifiedName);
            if (targetPoint.IsDigital)
                ((IOPoint_Digital)targetPoint).InitialValue = ((IOPoint_Digital)sourcePoint).InitialValue;
            else if (targetPoint.IsAnalog)
                ((IOPoint_Analog)targetPoint).InitialValue = ((IOPoint_Analog)sourcePoint).InitialValue;
            return true;
        }

        // --------------------------------------------------------------------------------------

        void ModifyConfigurationComment(IOConfiguration source, int behaviors, int defaults)
        {
            string newConfiguration =
                m_configurationManager.CurrentConfiguration.ConfigurationComment +
                "\nMerged from file created " + DateToString(source.CreationTIme) +
                "\n\tAdded " + behaviors.ToString() + " behaviors" +
                "\n\t and " + defaults.ToString() + " defaults";
            m_configurationManager.CurrentConfiguration.ConfigurationComment = newConfiguration;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region XML

        // ================================================================================================

        public void SaveConfigurationAsXML()
        {
            XMLConfiguration.Save(m_configurationManager.CurrentConfiguration);
        }

        // --------------------------------------------------------------------------------------

        public void LoadConfigurationFromXML()
        {
            XMLConfiguration.Load();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
