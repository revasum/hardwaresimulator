﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.Notifiers
{
    public class BehaviorCreated
    {
        Behavior m_behavior;

        public BehaviorCreated(Behavior aBehavior)
        {
            m_behavior = aBehavior;
        }

        public Behavior Behavior
        {
            get { return m_behavior; }
        }
    }
}
