﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.Notifiers
{
    public class Notifier
    {
        DateTime m_timestamp;
        protected string m_message;

        public Notifier(string aMessage)
        {
            m_timestamp = DateTime.Now;
            m_message = aMessage;
        }

        public override string ToString()
        {
            return m_timestamp.ToString("MMdd_HHmmfff \t") + m_message;
        }

        RevasumBaseObject m_sender;
        public RevasumBaseObject Sender
        {
            get { return m_sender; }
            set { m_sender = value; }
        }

    }
}
