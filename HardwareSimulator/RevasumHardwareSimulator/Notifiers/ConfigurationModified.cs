﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.Notifiers
{
    public class ConfigurationModified
    {
        IOConfiguration m_configuration = new IOConfiguration();

        public ConfigurationModified()
        {
            //m_configuration = config;
        }

        public IOConfiguration Configuration
        {
            get { return m_configuration; }
        }

    }
}
