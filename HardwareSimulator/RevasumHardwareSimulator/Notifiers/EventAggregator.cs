﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models;

namespace RevasumHardwareSimulator.Notifiers
{
    public class EventAggregator : IEventAggregator
    {
        private Dictionary<Type, IList> m_subscribers = new Dictionary<Type, IList>();
        private Dictionary<Type, IList> m_marked = new Dictionary<Type, IList>();

        // -------------------------------------------------------------

        static EventAggregator s_current;

        // -------------------------------------------------------------

        public static EventAggregator Current
        {
            get
            {
                if (s_current == null)
                    s_current = new EventAggregator();
                return s_current;
            }
        }

        // -------------------------------------------------------------

        private EventAggregator()
        {

        }

        // -------------------------------------------------------------

        public void Subscribe<T>(ISubscriber<T> subscriber)
        {
            IList typeSubscribers;
            if (!this.m_subscribers.TryGetValue(typeof(T), out typeSubscribers))
            {
                typeSubscribers = new List<ISubscriber<T>>();
                this.m_subscribers[typeof(T)] = typeSubscribers;
            }
            typeSubscribers.Add(subscriber);
        }

        // -------------------------------------------------------------

        public void Publish<T>(T message)
        {
            bool subscribersExist = false;
            IList typeSubscribers;
            if (this.m_subscribers.TryGetValue(typeof(T), out typeSubscribers))
            {
                IList<ISubscriber<T>> typedList = typeSubscribers as IList<ISubscriber<T>>;
                foreach (ISubscriber<T> sub in typedList)
                {
                    sub.Receive(message);
                    subscribersExist = true;
                }
            }
            if (message.GetType() != typeof(AllSubscribersNotified) && subscribersExist)
                this.Publish(new AllSubscribersNotified(message));
        }

        // -------------------------------------------------------------

        public void Mark()
        {
            m_marked = m_subscribers;
        }

        // -------------------------------------------------------------

        public void Restore()
        {
            m_subscribers = m_marked;
        }

        // -------------------------------------------------------------

    }
}
