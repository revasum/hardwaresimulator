﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.Notifiers
{
    public class TestNotifier : Notifier
    {
        private int m_destination;
        public TestNotifier(string message, int test)
            : base(message)
        {
            m_destination = test;
        }

        public int Destination
        {
            get { return m_destination; }
        }
    }
}
