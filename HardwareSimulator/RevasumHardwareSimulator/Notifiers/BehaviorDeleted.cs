﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.Behaviors;

namespace RevasumHardwareSimulator.Notifiers
{
    public class BehaviorDeleted
    {
         Behavior m_behavior;

         public BehaviorDeleted(Behavior aBehavior)
        {
            m_behavior = aBehavior;
        }

        public Behavior Behavior
        {
            get { return m_behavior; }
        }
   }
}
