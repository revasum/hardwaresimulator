﻿using RevasumHardwareSimulator.Global;
using System;
using TwinCAT.Ads;
using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_Digital : IOPoint, IDigitalPoint
    {

        // ================================================================================================

        #region Construction

        // ================================================================================================

        public IOPoint_Digital(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_Digital(string name, string module)
        {
            Name = name;
            ADSModuleName = module;
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_Digital(XMLNode node)
            : base(node)
        {
            BeckhoffSize = 1;
            BeckhoffDataType = "ADST_UINT8";
            ADSType = "BYTE";
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Overrides

        // ================================================================================================

        public override bool IsDigital
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            if (Name.StartsWith("di") || Name.StartsWith("sdi"))
            {
                RevasumDataType = RevasumDataTypes.DigitalInput;
            }
            else if (Name.StartsWith("do") || Name.StartsWith("sdo"))
            {
                RevasumDataType = RevasumDataTypes.DigitalOutput;
            }
            else
            {
                throw new Exception("Invalid Digital Point");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get
            {
                switch (RevasumDataType)
                {
                    case RevasumDataTypes.DigitalInput: 
                        return FileManager.GetPathForBitmap("Digital In 2.png");

                    case 
                        RevasumDataTypes.DigitalOutput: return FileManager.GetPathForBitmap("Digital Out 0.png");
                }

                return FileManager.GetPathForBitmap("butters.png");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region IDigitalPoint compliance - value

        // ================================================================================================

        public void Set()
        {
            BooleanValue = true;
        }

        // --------------------------------------------------------------------------------------------------

        public void Clear()
        {
            BooleanValue = false;
        }

        // --------------------------------------------------------------------------------------------------

        public void Toggle()
        {
            BooleanValue = !BooleanValue;
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsSet()
        {
            return BooleanValue;
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsClear()
        {
            return !BooleanValue;
        }

        // --------------------------------------------------------------------------------------------------

        public string ValueAsSymbol
        {
            get
            {
                if (BooleanValue)  // change it to m_booleanValue
                {
                    return FileManager.GetPathForBitmap("PointOn.jpg");
                }
                else
                {
                    return FileManager.GetPathForBitmap("PointOff.png");
                }
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region IDigitalPoint compliance - Default Values

        // ================================================================================================

        bool m_initialValue = false;
        public bool InitialValue
        {
            get { return m_initialValue; }
            set
            {
                m_initialValue = value;
                OnPropertyChanged("InitialValueSymbol");
                OnPropertyChanged("InitialValueAsString");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public string InitialValueSymbol
        {
            get
            {
                if (m_initialValue)
                {
                    return FileManager.GetPathForBitmap("PointOn.jpg");
                }
                else
                {
                    return FileManager.GetPathForBitmap("PointOff.png");
                }
            }
        }

        // -------------------------------------------------------------------

        public void ToggleInitialValue()
        {
            m_initialValue = !m_initialValue;
            OnPropertyChanged("InitialValueSymbol");
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region I/O Point Overrides

        // ================================================================================================

        public override string InitialValueAsString
        {
            get
            {
                return m_initialValue ? "ON" : "";
            }
            set 
            { 
                if (value.ToUpper() == "ON")
                {
                    m_initialValue = true;
                    InitialValue = true;
                }
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool HasDefault
        {
            get
            {
                return IsInput ? m_initialValue : false;
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ValueAsString
        {
            get { return BooleanValue.ToString(); }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region BooleanValue

        // ================================================================================================

        [NonSerialized]
        protected bool m_booleanValue = false;
        public bool BooleanValue
        {
            get { return m_booleanValue; }
            set
            {
                if (m_booleanValue != value || !m_valueInSync)
                {
                    m_booleanValue = value;
                    m_valueInSync = true;
                    OnPropertyChanged("ValueAsSymbol");
                    Changed();

                    if (IsInput)
                    {
                        WriteToPLC();
                    }
                }
            }
        }

        // -------------------------------------------------------------------

        public override void SetInitialValue()
        {
            m_valueInSync = false;

            if (IsInput)
            {
                BooleanValue = m_initialValue;
            }
            else
            {
                ReadFromPLC();
            }

        }

        // -------------------------------------------------------------------

        public override void ResetValue()
        {
            BooleanValue = false;
        }

        // -------------------------------------------------------------------

        public override void ResetDependents()
        {
            base.ResetDependents();
            BooleanValue = false;
        }

        // -------------------------------------------------------------------

        //public override void ReadFromPLC()
        //{
        //    // we do it only when first come onLine. Later notification takes care of obtaining the values

        //    AdsStream dataStream = new AdsStream(1);
        //    AdsBinaryReader binReader = new AdsBinaryReader(dataStream);
        //    ADSClient.Read(m_adsHandle, dataStream);
        //    int iValue = binReader.ReadByte();
        //    PLCValueChanged(iValue);
        //}

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region PLC Interface

        // ================================================================================================

        public override void PLCValueChanged(object newValue)
        {
            byte byteValue = byte.Parse(newValue.ToString());
            BooleanValue = byteValue > 0;
        }

        // --------------------------------------------------------------------------------------------------

        protected override void WriteToPLC()
        {
            //if (PLCWriteEnable)
            try
            {
                if (IsInput && ADSHandle != 0)
                {
                    byte iValue = BooleanValue ? (byte)1 : (byte)0;
                    PlcAccessor.Instance().Write(ADSHandle, iValue);
                }
            }

            catch
            {
                Console.WriteLine("Error writing to PLC (DigitalPoint)");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================



    }
}
