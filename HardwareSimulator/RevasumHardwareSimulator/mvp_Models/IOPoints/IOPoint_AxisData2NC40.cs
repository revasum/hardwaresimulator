﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;
using System.Runtime.InteropServices;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_AxisData2NC40 : IOPoint, IAxisPoint
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        MemoryLayoutPLC2NC40 last_memoryValue = new MemoryLayoutPLC2NC40();

        // --------------------------------------------------------------------------------------

        public IOPoint_AxisData2NC40(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
            string[] args = symbolInfo.Name.Split('.');
            Name = args[2];
        }

        // --------------------------------------------------------------------------------------

        public IOPoint_AxisData2NC40(XMLNode node)
            : base(node)
        {
            BeckhoffSize = 40;
            BeckhoffDataType = "ADST_BIGTYPE";
            ADSType = "ST_AxisParametersForSimu";
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Overrides

        // ================================================================================================

        public override bool IsOutputMotorControlStructure
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------

        public override bool IsOutput
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            RevasumDataType = RevasumDataTypes.MotorStructureToMC;
        }

        // --------------------------------------------------------------------------------------

        public override string InitialValueAsString
        {
            get { return ""; }
            set { }
        }

        // --------------------------------------------------------------------------------------

        public override bool HasDefault
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get { return FileManager.GetPathForBitmap("MC Out.png"); }
        }

        // --------------------------------------------------------------------------------------

        public override bool IsAxisCommandStructure
        {
            get { return true; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region PLC Access

        // ================================================================================================

        public override void PLCValueChanged(object theValue)
        {
            if (theValue is MemoryLayoutPLC2NC40 memory && !MemoryTheSame(last_memoryValue, memory))
            {
                Command = memory.ModuleCommand;
                Direction = memory.Direction;
                AxisPosition = memory.PositionSetPointInput;
                Velocity = memory.VelocitySetPointInput;
                Acceleration = memory.AccelerationSetPointInput;

                // 0 means "use default Acceleration", which typically is 200 mm/(sec * sec)
                if (Acceleration == 0)
                {
                    Acceleration = 200;
                }

                last_memoryValue = memory;

                Changed();

            }
        }

        // --------------------------------------------------------------------------------------------------

        bool MemoryTheSame(MemoryLayoutPLC2NC40 thisMemory, MemoryLayoutPLC2NC40 thatMemory)
        {
            if (last_memoryValue == null || thatMemory == null)
                return false;
            return (thisMemory.ModuleCommand == thatMemory.ModuleCommand &&
                thisMemory.Direction == thatMemory.Direction &&
                thisMemory.PositionSetPointInput == thatMemory.PositionSetPointInput &&
                thisMemory.VelocitySetPointInput == thatMemory.VelocitySetPointInput &&
                thisMemory.AccelerationSetPointInput == thatMemory.AccelerationSetPointInput &&
                thisMemory.CommandCount == thatMemory.CommandCount);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Properties

        // ================================================================================================


        public double Velocity { get; set; }

        public double Acceleration { get; set; }

        // NOTE: I/O point already contains "maintenance" variable "Position", which holds the position of the point in ADS data stream
        // - had to use a different name
        [NonSerialized]
        double m_axisPosition;
        public double AxisPosition
        {
            get { return m_axisPosition; }
            set
            {
                m_axisPosition = value;
                OnPropertyChanged("AxisPosition");
            }
        }

        public int Direction { get; set; }

        public int Command { get; set; }

        // ================================================================================================

        #endregion

        // ================================================================================================

        public override void ReadFromPLC()
        {

            if (ADSHandle != 0)
            {
                AdsStream dataStream = new AdsStream(40);
                PlcAccessor.Instance().Read(ADSHandle, dataStream);
            }

        }

    }
}
