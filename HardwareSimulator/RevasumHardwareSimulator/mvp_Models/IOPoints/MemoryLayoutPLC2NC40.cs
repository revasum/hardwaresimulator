﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class MemoryLayoutPLC2NC40
    {

        [MarshalAs(UnmanagedType.I2)]
        public short ModuleCommand;

        [MarshalAs(UnmanagedType.R8)]
        public Double PositionSetPointInput;

        [MarshalAs(UnmanagedType.R8)]
        public Double VelocitySetPointInput;

        [MarshalAs(UnmanagedType.R8)]
        public Double AccelerationSetPointInput;

        [MarshalAs(UnmanagedType.I2)]
        public short Direction;

        [MarshalAs(UnmanagedType.I2)]
        public short CommandCount;

    }
}
