﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 0)]
    public class MemoryLayoutNC2PLC
    {

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 StatusDWord;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 ErrorCode;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 AxisState;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 AxisModeConfirmation;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 HomingState;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 CoupleState;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 SvbEntries;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 SafENtries;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 AxisId;

        [MarshalAs(UnmanagedType.U4)]
        public UInt32 OpModeDWord;

        [MarshalAs(UnmanagedType.R8)]
        public Double ActPos;

        [MarshalAs(UnmanagedType.R8)]
        public Double ModuloActPos;

        [MarshalAs(UnmanagedType.U2)]
        public ushort ActiveControlLoopIndex;

        [MarshalAs(UnmanagedType.U2)]
        public ushort ControlLoopIndex;

        [MarshalAs(UnmanagedType.I4)]
        public Int32 ModuloActTurns;

        [MarshalAs(UnmanagedType.R8)]
        public Double ActVelo;

        [MarshalAs(UnmanagedType.R8)]
        public Double PosDiff;

        [MarshalAs(UnmanagedType.R8)]
        public Double SetPos;

        [MarshalAs(UnmanagedType.R8)]
        public Double SetVelo;

        [MarshalAs(UnmanagedType.R8)]
        public Double SetAcc;

        [MarshalAs(UnmanagedType.R8)]
        public Double TargetPos;

        [MarshalAs(UnmanagedType.R8)]
        public Double ModuloSetPos;

        [MarshalAs(UnmanagedType.I4)]
        public Int32 ModuloSetTurns;

        [MarshalAs(UnmanagedType.U2)]
        public ushort CmdNo;

        [MarshalAs(UnmanagedType.U2)]
        public ushort CmdState;

        [MarshalAs(UnmanagedType.R8)]
        public Double SetJerk;

        [MarshalAs(UnmanagedType.R8)]
        public Double SetTorque;

        [MarshalAs(UnmanagedType.R8)]
        public Double ActTorque;

        //[MarshalAs(UnmanagedType.U4)]
        //public UInt32 StateDWord2;

        //[MarshalAs(UnmanagedType.U4)]
        //public UInt32 StateDWord3;

        //[MarshalAs(UnmanagedType.U4)]
        //public UInt32 TouchProbeState;

        //[MarshalAs(UnmanagedType.U4)]
        //public UInt32 TouchProbeCounter;

        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8, ArraySubType = UnmanagedType.U1)]
        //public byte[] CamCouplingState;

        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8, ArraySubType = UnmanagedType.U1)]
        //public byte[] CamCouplingTableID;

        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 72, ArraySubType = UnmanagedType.U1)]
        //public byte[] _reserved1;


    }
}
