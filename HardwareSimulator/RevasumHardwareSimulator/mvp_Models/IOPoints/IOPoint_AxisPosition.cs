﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//using TwinCAT.Ads;
using RevasumHardwareSimulator.Global;
//using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_AxisPosition : IOPoint_Analog
    {

        // ================================================================================================

        #region Construction

        // ================================================================================================

        public IOPoint_AxisPosition(string name, string module) : base(name, module)
        {
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_AxisPosition(XMLNode node)
            : base(node)
        {
            RevasumDataType = RevasumDataTypes.None;
            BeckhoffSize = -1;
            BeckhoffDataType = null;
            ADSType = null;
            ADSComment = null;
            ADSNameDescription = null;
            ADSFullName = null;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Overrides

        // ================================================================================================

        public override bool IsInput
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsOriginalPoint
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get { return FileManager.GetPathForBitmap("movement.png"); }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
