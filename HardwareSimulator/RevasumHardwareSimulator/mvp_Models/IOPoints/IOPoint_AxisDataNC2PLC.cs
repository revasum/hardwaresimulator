﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;
using System.Runtime.InteropServices;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{

    // --------------------------------------------------------------------------------------------------

    [Serializable]
    public class IOPoint_AxisDataNC2PLC : IOPoint, IAxisPoint
    {

        // =======================================================================================================================

        #region Initialize

        // =======================================================================================================================

        [NonSerialized]
        //private readonly bool USES_BECKHOFF_SIMULATED_AXIS = true; There does not seem to be any reason for this flag, anything with this datatype appears to be using Beckhoff simulated axis. -KM

        private readonly MemoryLayoutNC2PLC m_memory;
        private readonly IOPoint_AxisPosition m_positionTracker;

        // --------------------------------------------------------------------------------------------------

        public IOPoint_AxisDataNC2PLC(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
            string trackerName = Name + "Position";
            Name += "ToPLC";
            m_memory = new MemoryLayoutNC2PLC();
            _ = System.Runtime.InteropServices.Marshal.SizeOf(m_memory);
            m_positionTracker = new IOPoint_AxisPosition(trackerName, ADSModuleName);
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_AxisDataNC2PLC(XMLNode node)
            : base(node)
        {
            string trackerName = Name + "Position";
            //Name += "ToPLC"; 
            m_memory = new MemoryLayoutNC2PLC();
            _ = System.Runtime.InteropServices.Marshal.SizeOf(m_memory);
            m_positionTracker = new IOPoint_AxisPosition(trackerName, ADSModuleName);

            BeckhoffSize = 256;
            BeckhoffDataType = "ADST_BIGTYPE";
            ADSType = "MC.NCTOPLC_AXIS_REF";
        }

        // --------------------------------------------------------------------------------------------------

        public override void CompleteInitialization(IOPointList allPoints)
        {
            allPoints.Add(m_positionTracker);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Overriden properties

        // =======================================================================================================================

        public override bool IsInput
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsInputMotorControlStructure
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            RevasumDataType = RevasumDataTypes.MotorStructureFromMC;
        }

        // --------------------------------------------------------------------------------------------------

        public override string InitialValueAsString
        {
            get { return ""; }
            set { }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ValueAsString
        {
            get { return AxisPosition.ToString(); }
        }

        // --------------------------------------------------------------------------------------------------
        public override bool HasDefault
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get { return FileManager.GetPathForBitmap("MC In.png"); }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsAxisCommandStructure
        {
            get { return true; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Specific properties

        // =======================================================================================================================

        [NonSerialized]
        double m_axisPosition;
        public double AxisPosition
        {
            get { return m_axisPosition; }
            set
            {
                Changed();
                m_axisPosition = value;
                OnPropertyChanged("AxisPosition");

                //if (!USES_BECKHOFF_SIMULATED_AXIS)
                //{
                //    m_memory.ActPos = m_axisPosition;
                //    WriteToPLC();
                //}
            }
        }

        // --------------------------------------------------------------------------------------------------

        public double Velocity { get; set; }

        public double Acceleration { get; set; }

        public int Direction { get; set; }

        public int Command { get; set; }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region PLC access

        // =======================================================================================================================

        public override void PLCValueChanged(object newValue)
        {
            if (newValue is MemoryLayoutNC2PLC memory)
            {
                m_positionTracker.AnalogValue = memory.ActPos;


               // if (USES_BECKHOFF_SIMULATED_AXIS)
                    AxisPosition = memory.ActPos;
            }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void WriteToPLC()
        {
            if (ADSHandle != 0)
            {
                PlcAccessor.Instance().Write(ADSHandle, m_memory);
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void ReadFromPLC()
        {
            if (ADSHandle != 0)
            {
                AdsStream dataStream = new AdsStream(256);
                PlcAccessor.Instance().Read(ADSHandle, dataStream);
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================





    }
}
