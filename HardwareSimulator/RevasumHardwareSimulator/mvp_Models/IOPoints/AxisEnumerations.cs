﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{

    // =======================================================================================================================

    #region Axis Directions

    // =======================================================================================================================

    public enum AxisStatesEnum
    {
        Idle,
        AtSpeed = 3,
        Acceleration,
        Deceleration
    }

    // =======================================================================================================================

    #endregion

    // =======================================================================================================================

    #region Axis Directions

    // =======================================================================================================================

    public enum AxisDirectionsEnum
    {
        Positive_Direction = 1,
        Shortest_Way,
        Negative_Direction,
        Current_Direction,
        Undefined_Direction = 128,
    }

    // =======================================================================================================================

    #endregion

    // =======================================================================================================================

    #region Axis Commands

    // =======================================================================================================================
    
    public enum AxisCommandsEnum
    {
        eCommand_Nothing,

        //High priority, preemptive group motion commands
		eCommand_Abort_All_Motion,
		eCommand_Stop_All_Motion,
		eCommand_Halt_All_Motion,

	    // Group Motion Settings
		eCommand_Enable_All_Axis_Settings = 20,
		eCommand_Disable_All_Axis_Settings,
		eCommand_Enable_All_Axis,
		eCommand_Disable_All_Axis,
		eCommand_Enable_Forward_Direction_All,
		eCommand_Disable_Forward_Direction_All,
		eCommand_Enable_Backward_Direction_All,
		eCommand_Disable_Backward_Direction_All,

	    // Group Motion Commands
		eCommand_Home_All_Motion = 40,
		eCommand_Reset_All_Motion,

		eCommand_Prep_Motion_Table = 50,
		eCommand_Execute_Motion_Table,
		eCommand_Execute_Motion_Index,
		eCommand_Advance_Motion_Index,
		eCommand_Pause_Motion_Table,
		
	    // Probe Zeroing
		eCommand_Surface_Probe_Zero = 80,
		eCommand_HeadAngle_Probe_Zero,
		eCommand_ForceSensor_Zero,
		eCommand_StoreWaferHeight,
		eCommand_Surface_Probe_Zero_To_Tamar,
		eCommand_Surface_Probe_Average,	//85
		eCommand_Chuck_Probe_Sample,
		
	    // eValve Commands
		eCommand_On = 100,
		eCommand_Off,
		eCommand_Up,
		eCommand_Down,
		eCommand_Left,
		eCommand_Right,  //105
		eCommand_Extend,
		eCommand_Retract,
		eCommand_Open,
		eCommand_Close,
		eCommand_ChuckCleanerArm_Park,			// for Work Chuck Cleaner* -  110 
		eCommand_ChuckCleanerArm_OverPark,
		eCommand_ChuckCleanerArm_OverChuck,
		eCommand_ChuckCleanerArm_ChuckClean,
		eCommand_Lock,
		eCommand_Unlock, // 115
		eCommand_Inflate,
		eCommand_Deflate,
		eCommand_FlashOn,
		eCommand_FlashOff,
		eCommand_Apply,	// 120 
		eCommand_Release,
		eCommand_SrdBrushToPark,
		eCommand_SrdBrushToSweep,
		eCommand_SrdBrushToAbovePark,
		eCommand_SrdBrushToAboveWaferCenter, // 125
		eCommand_SrdBrushToAboveWaferEdge,
		eCommand_SrdBrushSweep,
		eCommand_IncrementWaferCounter,
		eCommand_ClearWaferCounter,
		eCommand_FlashRapid,				// 130 
		eCommand_Toggle,
		eCommand_SrdSetMarkerPosition,
		eCommand_SrdToSafePosition,
		eCommand_FlashPattern,
		eCommand_RecoverAfterAbort,
		
		// Scope commands
		eCommand_Start = 140,
		eCommand_Stop,
		eCommand_Save,

		// Initialize
		eSpinRinseDry_Command_Initialize = 200,
		eSpinRinseDry_Command_ModuleEnable ,
		eSpinRinseDry_Command_ModuleDisable,
		
		// Axis Pid Commands
		eCommand_PID_Start = 250,
		eCommand_PID_Stop,
		eCommand_PID_Reset,
		
		eCommand_AutoAlign_Start = 270,
		eCommand_AutoAlign_Stop,
		eCommand_AutoAlign_Reset,

		// Tamar Commands
		eCommand_Tamar_NoCommand = 300,
		eCommand_Tamar_Version,
		eCommand_Tamar_Initialize,
		eCommand_Tamar_SetBufferSize,
		eCommand_Tamar_SetNumberOfPeaks,
		eCommand_Tamar_Close,
		eCommand_Tamar_Connect,
		eCommand_Tamar_Disconnect,
		eCommand_Tamar_ClearQueue,
	
		eCommand_Tamar_SetGain = 310,
		eCommand_Tamar_SetSpectraToAverage,
		eCommand_Tamar_SetIndexOfRefraction1,
		eCommand_Tamar_SetIndexOfRefraction2 ,
		eCommand_Tamar_SetMinThickness1,
		eCommand_Tamar_SetMaxThickness1,
		eCommand_Tamar_SetMinThickness2 ,
		eCommand_Tamar_SetMaxThickness2 ,
	
		eCommand_Tamar_GetGain = 330,
		eCommand_Tamar_GetIndexOfRefraction1 ,
		eCommand_Tamar_GetIndexOfRefraction2 ,
		eCommand_Tamar_GetSpectraToAverage ,
		eCommand_Tamar_GetMinThickness1,
		eCommand_Tamar_GetMaxThickness1,
		eCommand_Tamar_GetMinThickness2 ,
		eCommand_Tamar_GetMaxThickness2 ,
	
		eCommand_Tamar_GetThickness1 = 350,
		eCommand_Tamar_GetThickness2,
		eCommand_Tamar_GetThickness1Continuous,
		eCommand_Tamar_GetThickness2Continuous,
		eCommand_Tamar_StopGetThickness1Continuous,
		eCommand_Tamar_StopGetThickness2Continuous,
		
		
		eCommand_Tamar_SetTimeToAverage = 370,
		eCommand_Tamar_ResetAverage,
		eCommand_Tamar_SetAverageOffset,
		
	    // robot commands
		eCommand_Select_Partner_Module = 400,
		eCommand_Select_Slot_Number,
		eCommand_Configure_Partner_Variable,
		eCommand_Partner_Vacuum_ON,	// 403 
		eCommand_Partner_Vacuum_OFF,	// 404 
		eCommand_Partner_Blowoff_ON,	// 405 
		eCommand_Partner_Blowoff_OFF,	// 406 
		eCommand_Partner_Slide_Open,	// 407 
		eCommand_Partner_Slide_Close,	// 408 
		
	    // Sequencer Commands
		eCommand_Sequencer_Execute_Table = 900,	    
		eCommand_Sequencer_Execute_All_Tables,		
		eCommand_Sequencer_Execute_Current_Record,	
		eCommand_Sequencer_Single_Step_Record,		
		eCommand_Sequencer_Pause_Execution,			
		eCommand_Sequencer_Resume_Execution,		
		eCommand_Sequencer_Abort_Execution,			
		eCommand_Sequencer_Stop_Execution,			
		eCommand_Sequencer_Clear_Sequence_Tables,	
		eCommand_Sequencer_Reset,					
		eCommand_Sequencer_NextStep,        // 910: Next Step Sequencer *)
		
		eCommand_Sequence_Marker_Set = 950,		// (* 950: Store a number (marker) to be recorded in the sequencer log *)
		eCommand_On_EndStep_Goto_Step = 960,	//	(* 960: If any endpoint/endstep triggers, jump to the specified sequence table step number. *)
		eCommand_On_EndStep_Fault,				//	(* 961: If any endpoint/endstep triggers, fault out the sequencer and save the specified fault ID. *)
		eCommand_Always_Goto_Step,				//	(* 962: At the end of this step, unconditionally jump to the specified sequence table step number. *)
		eCommand_Always_Fault,					//	(* 963: At the end of this step, unconditionally fault out the sequencer and save the specified fault ID. *)
		eCommand_Allow_Backward_Goto,			//	(* 964: Sets a flag to allow any GOTO to jump backward (to a step# smaller than the current step#). *)
		eCommand_On_EndForce_Goto_Step,			//	(* 965: If EndForce triggers, jump to the specified sequence table step. *)

	// Individual Axis Commands *)

	// Preemptive Commands *)
		eCommand_Stop_Axis = 1002,
		eCommand_Halt_Axis,

	// Non-Preemptive Commands *)
		eCommand_Enable_Axis_All_Settings = 1020,
		eCommand_Disable_Axis_All_Settings,
		eCommand_Enable_Axis,
		eCommand_Disable_Axis,
		eCommand_Enable_Forward_Direction_Axis,
		eCommand_Disable_Forward_Direction_Axis,
		eCommand_Enable_Backward_Direction_Axis,
		eCommand_Disable_Backward_Direction_Axis,

	// Motion Commands *)
		eCommand_Home_Axis = 1040,
		eCommand_Reset_Axis,
		eCommand_SOE_Reset_Axis,
		eCommand_MoveAbsolute_Axis = 1050,
		eCommand_MoveRelative_Axis,
		eCommand_MoveModulo_Axis,
		eCommand_MoveVelocity_Axis,

		eCommand_FastForwardJog_Axis = 1070,
		eCommand_ForwardJog_Axis,
		eCommand_BackwardJog_Axis,
		eCommand_FastBackwardJog_Axis,
		eCommand_ContinuousForwardJog_Axis,
		eCommand_ContinuousBackwardJog_Axis,
		eCommand_DisableJog_Axis,
		eCommand_Paramterize_Limits_Axis = 1090,
		eCommand_Read_Bias_Offset,
		eCommand_Write_Bias_Offset,
		eCommand_SetAxisPosition,

		eCommand_AtHomeSignalSet = 23000  // MC_Home, Simulation Test 

    }

    // =======================================================================================================================

    #endregion

    // =======================================================================================================================

}
