﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_EncoderInput : IOPoint
    {

        public IOPoint_EncoderInput(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
        }

        public IOPoint_EncoderInput(string name, string module)
        {
            Name = name;
            ADSModuleName = module;
        }

        public IOPoint_EncoderInput(XMLNode node): base(node)
        {
            BeckhoffSize = 4;
            BeckhoffDataType = "ADST_INT32";
            ADSType = "DINT";
        }


        public override string InitialValueAsString
        {
            get { return ""; }
            set { }

        }

        public override bool HasDefault
        {
            get { return false; }
        }

        public override bool IsDoubleInteger
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsEncoderInput
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsInput
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            RevasumDataType = RevasumDataTypes.EncoderInput;
        }

        // --------------------------------------------------------------------------------------------------

        //public override bool IsInput
        //{
        //    get { return true; }
        //}

        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get
            {
                return FileManager.GetPathForBitmap("Encoder.png");
            }
        }

        // --------------------------------------------------------------------------------------------------


        // --------------------------------------------------------------------------------------------------


        public override void ReadFromPLC() { }

    }
}
