﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;
using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_DoubleInteger : IOPoint
    {

        public IOPoint_DoubleInteger(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
        }

        public IOPoint_DoubleInteger(XMLNode node) : base(node)
        {
            BeckhoffSize = 4;
            BeckhoffDataType = node.AttributeAt("BeckhoffDataType");
            ADSType = node.AttributeAt("AdsType");
        }

        public IOPoint_DoubleInteger(string name, string module)
        {
            Name = name;
            ADSModuleName = module;
        }

        public override bool IsDoubleInteger
        {
            get { return true; }
        }

        public override string InitialValueAsString
        {
            get { return ""; }
            set { }

        }


        public override bool HasDefault
        {
            get { return false; }
        }


        // --------------------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            RevasumDataType = RevasumDataTypes.DoubleAnalogOutput;
        }

        // --------------------------------------------------------------------------------------------------

        //public override bool IsInput
        //{
        //    get
        //    {
        //        return false;
        //    }
        //}

        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get
            {
                return FileManager.GetPathForBitmap("Parrot.png");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void ReadFromPLC() { }

    }
}
