﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TwinCAT.Ads;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public abstract class IOPoint : RevasumSimulatedPoint, IIOPoint
    {
        // =======================================================================================================================

        #region To Do

        // --------------------------------------------------------------------------------------------------

        static IOPoint()
        {
            ToDo("1. Research the possibility of basing GetHashCode on GUID", "IOPoint");
            ToDo("2. anIOPoint should not know about SelectedBackground", "IOPoint");
            ToDo("3. Split Image Source implemented in subclass", "IOPoint");
        }

        // --------------------------------------------------------------------------------------------------

        #endregion

        // =======================================================================================================================

        #region Construction

        // =======================================================================================================================

        public IOPoint()
        {
            IsSelected = false;
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint(string aName)
        {
            m_name = aName;
            IsSelected = false;
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint(XMLNode node)
        {
            string qualified = node.AttributeAt("QualifiedName");
            string[] parts = qualified.Split('.');
            ADSModuleName = parts[0];
            Name = parts[1];
            ADSFullName = node.AttributeAt("ADSFullName");
            ADSNameDescription = node.AttributeAt("ADSNameDescription");
            ADSComment = node.AttributeAt("ADSComment");
            Position = IntValueAtAttribute(node, "Position", 0);
            BehaviorAdded = BoolValueAtAttribute(node, "HasBehavior");
            RevasumDataType = (RevasumDataTypes)Enum.Parse(typeof(RevasumDataTypes), node.AttributeAt("RevasumDataType"));
            InitialValueAsString = node.AttributeAt("InitialValue");
            SetInitialValue();
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint(TcAdsSymbolInfo symbolInfo)
        {
            EncryptName(symbolInfo);
            EstablishDataType();
            EstablishMaintenanceData(symbolInfo);
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Create a point based on supplied ADSSymbol
        /// </summary>
        public static IOPoint FromAdsSymbolInfo(TcAdsSymbolInfo symbolInfo)
        {
            if (symbolInfo.Size == 1)
                return new IOPoints.IOPoint_Digital(symbolInfo);
            else if (symbolInfo.Size == 2)
            {
                // purge some unused crap
                if (symbolInfo.Name.Contains("GwVfdIO"))
                    return null;
                return new IOPoints.IOPoint_Analog(symbolInfo);
            }
            else if (symbolInfo.Size == 256)
                return new IOPoints.IOPoint_AxisDataNC2PLC(symbolInfo);
            else if (symbolInfo.Size == 128)
                // this is the old structure that Beckhoff created but is not using.  We replaced it with the 40-byte structure below
                return null;
            else if (symbolInfo.Size == 40)
                // new "NC command" structure that Chris created to replace the one above
                return new IOPoint_AxisData2NC40(symbolInfo);
            else if (symbolInfo.Size == 4)
                if (symbolInfo.Name.Contains(".enci"))
                    return new IOPoints.IOPoint_EncoderInput(symbolInfo);
                else
                    return new IOPoints.IOPoint_DoubleInteger(symbolInfo);

            throw new Exception("Unknown type encountered in the ADS Table. Don't know how to encode it    (IOPoint.FromAdsSymbolInfo)");

        }

        // --------------------------------------------------------------------------------------------------

        public void Restore(List<IOPoint> allPoints)
        {
            if (BehaviorAdded)
                Behavior.Restore(allPoints, this);
        }

        // --------------------------------------------------------------------------------------------------

        public virtual void CompleteInitialization(IOPointList allPoints) { }
        

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region ADS Name Encoding

        // =======================================================================================================================

        private void EncryptName(TcAdsSymbolInfo symbolInfo)
        {
            // the name of ADS item, as received from PLC is composed of several period-separated fields
            //  field 0 = "MAIN"
            //  field 1 = name of the module the I/O point belongs to
            //  field 2 = "stModuleIO"  - this string indicates the point is I/O. We disregard all the others
            //  field 3 = name of the point
            //  field 4 = optional. May include "to PLC" or "from PLC" indication

            ADSFullName = symbolInfo.Name;
            string[] names = ADSFullName.Split('.');

            if (names.Length == 2)
            {
                // this is a Global Variable
                Name = names[1];
                ADSModuleName = names[0];
                ADSNameDescription = ADSFullName;
                ADSComment = symbolInfo.Comment;
            }
            else if (names.Length == 4)
            {
                Name = names[3];
                ADSModuleName = names[1].Substring("fbModule".Length, names[1].Length - "fbModule".Length);
                ADSNameDescription = (names.Length > 4) ? names[4] : String.Empty;
                ADSComment = symbolInfo.Comment;
            }
            else 
            {
                // what do we do with unexpected formats?
                //throw new NotImplementedException($"Unexpected symbol format: {ADSFullName}");
                Name = names[3];
                ADSModuleName = names[1].Substring("fbModule".Length, names[1].Length - "fbModule".Length);
                ADSNameDescription = (names.Length > 4) ? names[4] : String.Empty;
                ADSComment = symbolInfo.Comment;
            }

        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Period-separated name as read from ADS. Used to establish ADS Notification
        /// </summary>
        public string ADSFullName { get; set; }

        /// <summary>
        /// Used to generate fully qualified name
        /// </summary>
        public string ADSModuleName { get; set; }

        /// <summary>
        /// part of ADSFullName. It is optional and when used it usually indicates the direction. We use it to identify inputs and outputs
        /// </summary>
        public string ADSNameDescription { get; set; }

        /// <summary>
        /// the function of some I/O cannot be determined solely based on ADS types. For those, Dan added Input/Output descriptions in the comment section
        /// </summary>
        public string ADSComment { get; set; }


        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region QualifiedName

        // =======================================================================================================================

        public override string QualifiedName
        {
            get { return QualifiedNameByModule; }
            set { }
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// combined Module Name and Name of an I/O point. Used for sorting
        /// </summary>
        public string QualifiedNameByModule
        {
            get { return ADSModuleName + "." + Name; }
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// combined Type, Module Name and Name of an I/O point. Used for sorting
        /// </summary>
        public string QualifiedNameByIOType
        {
            get { return RevasumDataType.ToString() + "." + ADSModuleName + "." + Name; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Data Type and other maintenance properties

        // =======================================================================================================================

        protected abstract void EstablishDataType();

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Enumeration defining the point (Digital/Analog Inputs, Digital/Analog Outputs, Encoder Inputs, Axis Data In/Out)
        /// </summary>
        public RevasumDataTypes RevasumDataType { get; set; }


        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Maintenance property - indicates position of this data point in the list of ADS points read from PLC
        /// </summary>
        public int Position { get; set; }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// the values retrieved below are not used by the application.  We retrieve them here only for maintenance purposes.
        /// </summary>
        private void EstablishMaintenanceData(TcAdsSymbolInfo symbolInfo)
        {
            ITcAdsSymbol5 newSymbol = symbolInfo as ITcAdsSymbol5;


            m_size = newSymbol.Size;
            m_dataType = newSymbol.DataTypeId.ToString();     // example:    "ADS_UINT8", "ADS_INT16"
            m_adsType = newSymbol.TypeName.ToString();        // example:    "BYTE", "INT", etc
        }

        // --------------------------------------------------------------------------------------------------

        int m_size = -1;
        public int BeckhoffSize
        {
            get { return m_size; }
            set { m_size = value; }
        }

        // --------------------------------------------------------------------------------------------------

        string m_dataType;
        public string BeckhoffDataType
        {
            get { return m_dataType; }
            set { m_dataType = value; }
        }

        // --------------------------------------------------------------------------------------------------

        string m_adsType;
        public string ADSType
        {
            get { return m_adsType; }
            set { m_adsType = value; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Function query

        // =======================================================================================================================

        public virtual bool IsInput
        {
            get
            {
                if (RevasumDataType == RevasumDataTypes.DigitalInput ||
                    RevasumDataType == RevasumDataTypes.AnalogInput)
                    return true;

                return false;
            }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsOutput
        {
            get
            {
                if (RevasumDataType == RevasumDataTypes.DigitalOutput ||
                   RevasumDataType == RevasumDataTypes.AnalogOutput ||
                   RevasumDataType == RevasumDataTypes.MotorStructureToMC)
                    return true;

                return false;
            }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsAxisCommandStructure
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsInputMotorControlStructure
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsOutputMotorControlStructure
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsEncoderInput
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual bool IsDoubleInteger
        {
            get { return false; }
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Original Point is one that is read from PLC, as opposed to ones artificially created (extracted from Axis Structures)
        /// </summary>
        public virtual bool IsOriginalPoint
        {
            get { return true; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Attached Behavior

        // =======================================================================================================================

        private Behavior m_behavior;
        public Behavior Behavior
        {
            get { return m_behavior; }
            set
            {
                m_behavior = value;
                BehaviorAdded = (m_behavior != null);
                BehaviorTypeName = (m_behavior == null) ? "" : m_behavior.Function;
                OnPropertyChanged("Behavior");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private bool m_behaviorAdded = false;
        public bool BehaviorAdded
        {
            get { return m_behaviorAdded; }
            set
            {

                m_behaviorAdded = value;
                OnPropertyChanged("BehaviorImageSource");
            }
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// This is the image of behavior that a point owns. 
        /// </summary>
        public string BehaviorImageSource
        {
            get
            {
                if (BehaviorAdded)
                    return Behavior.ImageSource;
                else
                    if (IsInput)
                        return FileManager.GetPathForBitmap("empty-box.png");
                    else
                        return FileManager.GetPathForBitmap("dash_1.png");
                       //return FileManager.GetPathForBitmap("NA.jpg");
            }
        }
        
        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Used by FileTab
        /// </summary>
        public string BehaviorTypeName
        {
            get { return m_behaviorTypeName; }
            set
            {
                m_behaviorTypeName = value;
                OnPropertyChanged("BehaviorTypeName");
            }
        }        private string m_behaviorTypeName;


        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Value

        // =======================================================================================================================

        public virtual string ValueAsString { get; set; }

        // --------------------------------------------------------------------------------------------------

        public abstract string InitialValueAsString { get; set; }

        // --------------------------------------------------------------------------------------------------

        public abstract bool HasDefault { get; }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Odd GUI Interfaces

        // =======================================================================================================================

        [NonSerialized]
        private bool m_isSelected = false;
        public bool IsSelected
        {
            get { return m_isSelected; }
            set
            {
                m_isSelected = value;
                OnPropertyChanged("SelectedBackground");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public abstract string ImageSource { get; }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// Once a point is selected it needs to modify its background color. Weird that I am doing it here, but each point needs to keep track of its own background
        /// </summary>
        public string SelectedBackground
        {
            get
            {
                if (IsSelected)
                    return "Cyan";
                return "AliceBlue";
            }
        }

        // =======================================================================================================================

        #endregion


        // =======================================================================================================================

        #region Online

        // =======================================================================================================================

        [NonSerialized]
        protected bool m_valueInSync = false;

        public override void OnLine()
        {
            m_valueInSync = false;
            if (BehaviorAdded)
               Behavior.OnLine();
        }

        // -------------------------------------------------------------------

        public override void OffLine()
        {
            ResetDependents();

            if (BehaviorAdded)
                Behavior.OffLine();
            m_valueInSync = false;
            ResetValue();
        }

        // -------------------------------------------------------------------

        public virtual void SetInitialValue()
        {
        }

        // -------------------------------------------------------------------

        public virtual void ResetValue()
        {
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Printing

        // =======================================================================================================================

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("({0}) {1} | {2}", ADSModuleName, Name, RevasumDataType.ToString()));
            return sb.ToString();
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region PLC Interface

        // =======================================================================================================================

        public int ADSHandle { get; set; }

        // ------------------------------------------------------------------- 

        protected virtual void WriteToPLC()
        {
        }

        // --------------------------------------------------------------------------------------------------

        public virtual void ReadFromPLC()
        {
            // we do it only when first come onLine. Later notification takes care of obtaining the values

            if (ADSHandle != 0)
            {
                AdsStream dataStream = new AdsStream(4);
                AdsBinaryReader binReader = new AdsBinaryReader(dataStream);
                PlcAccessor.Instance().Read(ADSHandle, dataStream);
                int iValue = binReader.ReadInt32();
                PLCValueChanged(iValue);
            }
        }

        // --------------------------------------------------------------------------------------------------

        public virtual void PLCValueChanged(object theValue)
        {
            Console.WriteLine("Invalid type - Should be overridden and never come here. (Received {0} from {1})", ValueAsString, QualifiedName);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public static IOPoint FromXML(XMLNode node)
        {
            //  NOTES:
            // 1. I didn't take care of translating the initial values yet .... can wait till the next step
            // 2. I know, I know ... we could use reflections and recreate the type in a fancy way, instead of using "Case statement" :-P

            IOPoint aPoint = null;
            string dataType = node.AttributeAt("RevasumDataType");
            switch (dataType)
            {
                case "DigitalInput": 
                case "DigitalOutput": aPoint = new IOPoint_Digital(node);
                    break;
                case "AnalogInput": 
                case "AnalogOutput": aPoint = new IOPoint_Analog(node);
                    break;
                case "MotorStructureToMC": aPoint = new IOPoint_AxisData2NC40(node);
                    break;
                case "MotorStructureFromMC": aPoint = new IOPoint_AxisDataNC2PLC(node);
                    break;
                case "AxisPosition": aPoint = new IOPoint_AxisPosition(node);
                    break;


            }

            return aPoint;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================


    }
}
