﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{
    [Serializable]
    public class IOPoint_DoubleAnalog : IOPoint, IAnalogPoint
    {

        // ================================================================================================

        #region Construction

        // ================================================================================================

        public IOPoint_DoubleAnalog(TcAdsSymbolInfo symbolInfo)
            : base(symbolInfo)
        {
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_DoubleAnalog(string name, string module)
        {
            Name = name;
            ADSModuleName = module;
        }

        // --------------------------------------------------------------------------------------------------

        public IOPoint_DoubleAnalog(XMLNode node) : base(node)
        {
            BeckhoffSize = 2;
            BeckhoffDataType = node.AttributeAt("BechoffDataType");
            ADSType = node.AttributeAt("AdsType");
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Overrides

        // ================================================================================================

        public override bool IsAnalog
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void EstablishDataType()
        {
            if (Name.StartsWith("ai") || Name.StartsWith("nRaw")) RevasumDataType = RevasumDataTypes.AnalogInput;
            else if (Name.StartsWith("ao")) RevasumDataType = RevasumDataTypes.AnalogOutput;
            else
                throw new Exception("Invalid Analog Point");
        }
        // --------------------------------------------------------------------------------------------------

        public override string ImageSource
        {
            get
            {
                switch (RevasumDataType)
                {
                    case RevasumDataTypes.AnalogInput: return FileManager.GetPathForBitmap("Analog In.png");
                    case RevasumDataTypes.AnalogOutput: return FileManager.GetPathForBitmap("Analog Out.png");
                }
                return FileManager.GetPathForBitmap("goose.png");
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Default Values

        // ================================================================================================

        double m_initialValue;

        public double InitialValue
        {
            get { return m_initialValue; }
            set
            {
                m_initialValue = value;
                OnPropertyChanged("InitialValueAsString");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool HasDefault
        {
            get { return m_initialValue != 0; }
        }

        // --------------------------------------------------------------------------------------------------

        public override string InitialValueAsString
        {
            get
            {
                if (m_initialValue != 0)
                    return m_initialValue.ToString();
                return "";
            }
            set { }

        }

        // -------------------------------------------------------------------

        public override void SetInitialValue()
        {
            m_valueInSync = false;
            if (IsInput)
                AnalogValue = m_initialValue;
            else
                ReadFromPLC();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region On Line

        // ================================================================================================

        public override void PLCValueChanged(object anObject)
        {
            Changed();
            int newValue = ushort.Parse(anObject.ToString());
            if (AnalogValue != newValue)
            {
                AnalogValue = newValue;
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ValueAsString
        {
            get { return AnalogValue.ToString(); }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void WriteToPLC()
        {
            //if (PLCWriteEnable)
            if (IsInput && ADSHandle != 0)
            {
                short iValue = (short)AnalogValue;
                PlcAccessor.Instance().WriteAny(ADSHandle, iValue);
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region IAnalogPoint interface compliance

        // ================================================================================================

        [NonSerialized]
        double m_analogValue;

        // --------------------------------------------------------------------------------------------------

        public double AnalogValue
        {
            get { return m_analogValue; }
            set
            {
                m_analogValue = value;
                OnPropertyChanged("AnalogValue");
                Changed();
                if (IsInput)
                    WriteToPLC();
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
