﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{

    [Serializable]
    public class IOPoint_NC2PLC<T> : IOPoint_AxisPointBase
    {

        [NonSerialized]
        T m_value;
        [NonSerialized]
        WriteValue<T> m_writeDelegate;
        [NonSerialized]
        CaptureValue<T> m_captureDataDelegate;


        public IOPoint_NC2PLC(string qualifiedName, string name, RevasumDataTypes dataType)
        {

            Name = name;
            ADSModuleName = qualifiedName;
            RevasumDataType = dataType;
        }



        protected override void EstablishDataType()
        {
            // already set in constrructor
        }


        public void CreateDelegate(WriteValue<T> aDelegate)
        {
            m_writeDelegate = aDelegate;
        }

        public void CreateCaptureDelegate(CaptureValue<T> aDelegate)
        {
            m_captureDataDelegate = aDelegate;
        }

        public void Restore(WriteValue<T> aDelegate)
        {
            m_writeDelegate = aDelegate;

        }


        public T Value
        {
            get { return m_value; }
            set {
                if (!m_value.Equals(value))
                {
                    m_value = value;
                    //Log(m_value.ToString());
                }
                
 }
        }

        [NonSerialized]
        int m_analogValue;

        public int AnalogValue
        {
            get { return Convert.ToInt32(m_value); }
            set
            {
                int baseValue = Convert.ToInt32(value);
                m_analogValue = baseValue;
            }
        }

        //public void ReadMemory<T2>(MemoryLayoutPLC2NC memory)
        //{
        //    Value = m_readDelegate(memory);
        //}


        public override void ReadFromPLC(object anObject)
        {
            MemoryLayoutNC2PLC memory = anObject as MemoryLayoutNC2PLC;
            //ReadMemory<T>(memory);
            Value = m_captureDataDelegate(memory);
        }

        
        public void WriteValue(T aValue)
        {
            m_value = aValue;
            m_writeDelegate(m_value);
        }


        //public override bool IsInput
        //{
        //    get { return true; }
        //}


        public override string ImageSource
        {
            get
            {
                switch (RevasumDataType)
                {
                    case RevasumDataTypes.AxisInput_DINT: return FileManager.GetPathForBitmap("MC In - dint.jpg");
                    case RevasumDataTypes.AxisInput_LREAL: return FileManager.GetPathForBitmap("MC In - lreal.jpg");
                    case RevasumDataTypes.AxisInput_UDINT: return FileManager.GetPathForBitmap("MC In - udint.jpg");
                    case RevasumDataTypes.AxisInput_UINT: return FileManager.GetPathForBitmap("MC In - uint.jpg");
                    case RevasumDataTypes.AxisInput_DWORD: return FileManager.GetPathForBitmap("MC In - dword.jpg");
                }
                return FileManager.GetPathForBitmap("goose.png");
            }
        }

        //public override bool IsAxisItem
        //{
        //    get { return true; }
        //}

        //public override bool IsAxisItemToPLC
        //{
        //    get { return true; }
        //}



    }
}
