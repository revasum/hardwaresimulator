﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using TwinCAT.Ads;

namespace RevasumHardwareSimulator.mvp_Models.IOPoints
{

    //[Serializable]
    //public abstract class IOPoint_PLC2NCBase : IOPoint
    //{

    //    public abstract void ReadFromPLC(MemoryLayoutPLC2NC memory);
    //}



    [Serializable]
    public class IOPoint_PLC2NC<T> : IOPoint_AxisPointBase
    {

        [NonSerialized]
        T m_value;
        [NonSerialized]
        ReadValue<T> m_readDelegate;

        public IOPoint_PLC2NC(string qualifiedName, string name, RevasumDataTypes dataType)
        {

            Name = name;
            ADSModuleName = qualifiedName;
            RevasumDataType = dataType;


        }



        protected override void EstablishDataType() 
        {
            // already set in constrructor
        }


        public void CreateDelegate(ReadValue<T> aDelegate)
        {
            m_readDelegate = aDelegate;
        }

        public void ReadMemory<T2>(MemoryLayoutPLC2NC memory)
        {
            Value = m_readDelegate(memory);
        }

        //public override void Restore(List<IOPoint> points)
        //{
        //    //Restore();

        //}

        public void Restore(ReadValue<T> aDelegate)
        {
            m_readDelegate = aDelegate;

        }

        public T Value
        {
            get { return m_value; }
            set
            {
                if (!m_value.Equals(value))
                {
                    m_value = value;
                    //Log(m_value.ToString());
                }
            }
        }

        //public override int AnalogValue
        //{
        //    get { return Convert.ToInt32(Value); }
        //    set
        //    {

        //         switch (RevasumDataType)
        //        {
        //            case Global.RevasumDataTypes.AxisOutput_DWORD:
        //            case Global.RevasumDataTypes.AxisOutput_UDINT:
        //                 (uint))Value = Convert.ToUInt32(value);

        //            case Global.RevasumDataTypes.AxisOutput_BYTE:
        //            case Global.RevasumDataTypes.AxisOutput_BOOL:
        //            case Global.RevasumDataTypes.AxisOutput_LREAL:
        //                break;
               
        //        Value 


        //        int baseValue = Convert.ToInt32(value);
        //        base.AnalogValue = baseValue;
        //    }
        //}


        public override void ReadFromPLC(object anObject)
        {
            MemoryLayoutPLC2NC memory = anObject as MemoryLayoutPLC2NC;
            Value = m_readDelegate(memory);
        }



        public override string ImageSource
        {
            get
            {
                switch (RevasumDataType)
                {
                    case RevasumDataTypes.AxisOutput_BOOL: return FileManager.GetPathForBitmap("MC Out - bool.jpg");
                    case RevasumDataTypes.AxisOutput_BYTE: return FileManager.GetPathForBitmap("MC Out -byte.jpg");
                    case RevasumDataTypes.AxisOutput_DWORD: return FileManager.GetPathForBitmap("MC Out - dword.jpg");
                    case RevasumDataTypes.AxisOutput_LREAL: return FileManager.GetPathForBitmap("MC Out -lreal.jpg");
                    case RevasumDataTypes.AxisOutput_UDINT: return FileManager.GetPathForBitmap("MC Out -udint.jpg");
                }
                return FileManager.GetPathForBitmap("goose.png");
            }
        }

        //public override bool IsAxisItem
        //{
        //    get { return true; }
        //}
        //public override bool IsAxisItemFromPLC
        //{
        //    get { return true; }
        //}


    }
}
