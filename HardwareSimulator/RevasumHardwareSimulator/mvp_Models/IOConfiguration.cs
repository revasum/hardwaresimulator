﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

using XMLSupport;
using RevasumHardwareSimulator.mvp_Models.Behaviors;

namespace RevasumHardwareSimulator.mvp_Models
{

    [Serializable]
    public class IOConfiguration
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        IOPointList m_IOPoints = new IOPointList();
        public static IOPointList m_IOPoints2 = new IOPointList();
        DateTime m_creationTime;
        DateTime m_modifyTime;
        

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// this constructor runs only when creating new configuration based on PLC data. All other times the configuration will be deserialized
        /// </summary>
        public IOConfiguration(IOPointList plcPoints, string projectName)
        {
            m_creationTime = DateTime.Now;
            m_modifyTime = m_creationTime;
            ProjectName = projectName;

            m_IOPoints = plcPoints;
            CountBehaviorsAndDefaults();
            CompleteInitialization();
        }

        // --------------------------------------------------------------------------------------

        public IOConfiguration()
        {
            m_creationTime = DateTime.Now;
            m_modifyTime = m_creationTime;
            m_IOPoints = new IOPointList();
        }


        // --------------------------------------------------------------------------------------

        public IOConfiguration(XMLNode rootNode)
        {
            LoadAttributes(rootNode);
            LoadIOPoints(rootNode.NodeAt("IOPoints"));
            m_IOPoints2 = m_IOPoints;
            List<Behavior> behaviors = new List<Behavior>();
            LoadBehaviors(rootNode.NodeAt("Behaviors"), behaviors);
            LinkBehaviorsToTargets(behaviors);
            CountBehaviorsAndDefaults();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Properties

        // ================================================================================================

        public List<IOPoint> Points
        {
            get { return m_IOPoints.AllPoints; }
        }

        // --------------------------------------------------------------------------------------

        public DateTime CreationTIme
        {
            get { return m_creationTime; }
            set { m_creationTime = value; }
        }

        // --------------------------------------------------------------------------------------

        public DateTime ModifyTime
        {
            get { return m_modifyTime; }
            set { m_modifyTime = value; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Comment entered on the "About" screen
        /// </summary>
        public string ConfigurationComment { get; set; }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Name chosen from a list of existing projects. 
        /// </summary>
        public string ProjectName { get; set; }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Properties derived from IOPointList

        // ================================================================================================

        public List<IOPoint> Inputs
        {
            get
            {
                List<IOPoint> inputList = new List<IOPoint>();
                foreach (IOPoint point in Points)
                    if (point.IsInput)
                        inputList.Add(point);
                return inputList;
            }
        }

        // --------------------------------------------------------------------------------------

        public ObservableCollection<IOPoint> AsObservableCollection
        {
            get { return new ObservableCollection<IOPoint>(Points); }
        }

        // --------------------------------------------------------------------------------------

        public ObservableCollection<IDigitalPoint> DigitalPointsAsObservableCollection
        {
            get
            {
                ObservableCollection<IDigitalPoint> collection = new ObservableCollection<IDigitalPoint>();
                foreach (IOPoint point in Points)
                    if (point.IsDigital)
                    {
                        IDigitalPoint digital = point as IDigitalPoint;
                        collection.Add(digital);
                    }
                return collection;
            }
        }

        // --------------------------------------------------------------------------------------

        public ObservableCollection<IAnalogPoint> AnalogPointsAsObservableCollection
        {
            get
            {
                ObservableCollection<IAnalogPoint> collection = new ObservableCollection<IAnalogPoint>();
                foreach (IOPoint point in Points)
                    if (point.IsAnalog)
                    {
                        IAnalogPoint analog = point as IAnalogPoint;
                        collection.Add(analog);
                    }
                return collection;
            }
        }

        // --------------------------------------------------------------------------------------

        public string PLCComment
        {
            get { return m_IOPoints.PLCComment; }
            set { m_IOPoints.PLCComment = value; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Restore System

        // ================================================================================================

        public void RestoreBehaviorLinks()
        {
            foreach (IOPoint aPoint in Points)
            { 
            aPoint.Restore(Points);
        }
            CountBehaviorsAndDefaults();
        }

        // --------------------------------------------------------------------------------------

        public void ModifiedNow()
        {
            m_modifyTime = DateTime.Now;
            CountBehaviorsAndDefaults();
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Each point gets a chance to duplicate itself or add any poart of itself to the IOList.  It is applicable especially to complex Axis Structures
        /// </summary>
        void CompleteInitialization()
        {
            foreach (IOPoint point in m_IOPoints.AllPointsCopy)
                point.CompleteInitialization(m_IOPoints);
        }

        // --------------------------------------------------------------------------------------

        public List<IOPoint> OriginalPointOnly
        {
            get
            {
                List<IOPoint> list = new List<IOPoint>();
                foreach (IOPoint point in Points)
                {
                    if (point.IsOriginalPoint)
                        list.Add(point);
                }
                return list;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Behaviors

        // ================================================================================================

        [NonSerialized]
        int m_numberOfBehaviors = 0;
        public int NumberOfBehaviors
        {
            get { return m_numberOfBehaviors; }
        }

        // --------------------------------------------------------------------------------------

        [NonSerialized]
        int m_numberOfDefaults = 0;
        public int NumberOfDefaults
        {
            get { return m_numberOfDefaults; }
        }

        // --------------------------------------------------------------------------------------

        public void CountBehaviorsAndDefaults()
        {
            m_numberOfBehaviors = 0;
            m_numberOfDefaults = 0;

            foreach (IOPoint aPoint in Inputs)
            {
                if (aPoint.BehaviorAdded)
                    m_numberOfBehaviors++;
                if (aPoint.HasDefault)
                    m_numberOfDefaults++;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region XML Support

        // ================================================================================================

        void LoadAttributes(XMLNode node)
        {
            string time = node.AttributeAt("CreationTime");
            m_creationTime = DateTime.ParseExact(time, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            time = node.AttributeAt("LastModified");
            m_modifyTime = DateTime.ParseExact(time, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
            PLCComment = node.AttributeAt("PLCComment");
            ConfigurationComment = node.AttributeAt("ConfigurationComment");
        }

        // --------------------------------------------------------------------------------------

        void LoadIOPoints(XMLNode points)
        {

            foreach (XMLNode node in points.Nodes())
            {
                IOPoint aPoint = null;
                string dataType = node.AttributeAt("RevasumDataType");
                switch (dataType)
                {
                    case "DigitalInput":
                    case "DigitalOutput": aPoint = new IOPoint_Digital(node); break;
                    case "AnalogInput":
                    case "AnalogOutput": aPoint = new IOPoint_Analog(node); break;
                    case "MotorStructureToMC": aPoint = new IOPoint_AxisData2NC40(node); break;
                    case "MotorStructureFromMC": aPoint = new IOPoint_AxisDataNC2PLC(node); break;
                    case "AxisPosition": aPoint = new IOPoint_AxisPosition(node); break;
                    case "EncoderInput": aPoint = new IOPoint_EncoderInput(node); break;
                    case "DoubleAnalogInput":
                    case "DoubleAnalogOutput": aPoint = new IOPoint_DoubleInteger(node); break;//new IOPoint_DoubleAnalog(node); break;
                    case "None": aPoint = new IOPoint_AxisPosition(node); break;
                    default: throw new Exception("Unsuported Data Type : " + dataType);
                }
                m_IOPoints.Add(aPoint);
            }
        }

        // --------------------------------------------------------------------------------------

        void LoadBehaviors(XMLNode parentNode, List<Behavior> behaviors)
        {
            Behavior aBehavior = null;
            foreach (XMLNode node in parentNode.Nodes())
            {
                string function = node.AttributeAt("Function");
                switch (function)
                {
                    case "AND": aBehavior = new Behavior_AND(node); break;
                    case "OR": aBehavior = new Behavior_OR(node); break;
                    case "NOT": aBehavior = new Behavior_NOT(node); break;
                    case "LINEAR": aBehavior = new Behavior_LINEAR(node); break;
                    case "THRESHOLD": aBehavior = new Behavior_THRESHOLD(node); break;
                    case "Script": aBehavior = new Behavior_Script(node); break;
                    case "PI_Control": aBehavior = new Behavior_PI_Control(node); break;
                    //case "Script": break;


                    default: throw new Exception("Could not identify Behavior: " + function);
                }

                if (behaviors != null)
                behaviors.Add(aBehavior);


            }

        }

        // --------------------------------------------------------------------------------------

        void LinkBehaviorsToTargets(List<Behavior> behaviors)
        {
            foreach (Behavior behavior in behaviors)
            {
                bool found = false;
                foreach (IOPoint point in Points)
                    if (point.QualifiedName == behavior.TargetName)
                    {
                        point.Behavior = behavior;
                        behavior.Restore(Points, point);
                        found = true;
                        break;
                    }
                if (!found)
                    throw new Exception("Cound not find Target for behavior " + behavior.TargetName);
            }

        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
