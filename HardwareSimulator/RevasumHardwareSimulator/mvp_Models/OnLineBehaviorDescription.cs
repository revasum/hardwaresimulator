﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.mvp_Models
{
    class OnLineBehaviorDescription
    {
        public string BehaviorFunction;
        public string View;
        public string Presenter;

        // --------------------------------------------------------------------------------------------------

        public OnLineBehaviorDescription(string b, string v, string p)
        {
            BehaviorFunction = b;
            View = v;
            Presenter = p;
        }

        // --------------------------------------------------------------------------------------------------

        public override string ToString()
        {
            return "(OnLine Behavior) " + BehaviorFunction;
        }
        // --------------------------------------------------------------------------------------------------
    }

}