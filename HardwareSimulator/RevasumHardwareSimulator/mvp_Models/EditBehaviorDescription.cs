﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RevasumHardwareSimulator.mvp_Models
{

    /// <summary>
    /// Structure containing a  a description of a model object and a triplet of strings containing the class names of the behavior, its view and its presenter
    /// </summary>
    public struct EditBehaviorDescription
    {

        public string Description;
        public string Model;
        public string View;
        public string Presenter;

        // --------------------------------------------------------------------------------------------------

        public EditBehaviorDescription(string d, string b, string v, string p)
        {
            Description = d;
            Model = b;
            View = v;
            Presenter = p;
        }

        // --------------------------------------------------------------------------------------------------

        public override string ToString()
        {
            return "(MVP) " + Description;
        }
        // --------------------------------------------------------------------------------------------------
    }

}
