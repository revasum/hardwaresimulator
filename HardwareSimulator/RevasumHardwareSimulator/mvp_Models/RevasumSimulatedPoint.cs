﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.Datalog;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Models
{
    /// <summary>
    /// common superclass to IOPoint and Behavior
    /// </summary>
    [Serializable]
    public abstract class RevasumSimulatedPoint : RevasumBaseObject
    {
        // HACK: Fix the multiple interface problem with the digital IO point. This fix will change a lot of files.
        //       
        private static readonly object m_DependentsLock = new object();

        // =======================================================================================================================

        #region Initialization

        // =======================================================================================================================

        public RevasumSimulatedPoint()
        {
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Basic Properties

        // =======================================================================================================================

        protected string m_name = "";
        public string Name
        {
            get { return m_name; }
            set
            {
                m_name = value;
                OnPropertyChanged("Name");
            }
        }

        // -------------------------------------------------------------------

        public virtual string QualifiedName { get; set; }

        // -------------------------------------------------------------------

        public virtual bool IsDigital
        {
            get { return false; }
        }

        // -------------------------------------------------------------------

        public virtual bool IsAnalog
        {
            get { return false; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Online

        // =======================================================================================================================

        public abstract void OnLine();

        // -------------------------------------------------------------------

        public abstract void OffLine();

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Local Dependency

        // =======================================================================================================================

        [NonSerialized]
        private List<ILocalNotifier> m_Dependents;

        // -------------------------------------------------------------------

        public virtual void ResetDependents()
        {
            m_Dependents = null;
        }

        // -------------------------------------------------------------------

        public void AddDependent(ILocalNotifier aDependent)
        {
            lock (m_DependentsLock)
            {
                if (m_Dependents == null)
                {
                    m_Dependents = new List<ILocalNotifier>();
                }

                m_Dependents.Add(aDependent);
            }
        }

        // -------------------------------------------------------------------

        public virtual void LocalIOChanged()
        {
            Console.WriteLine("LocalIOChanged Not Implemented : " + QualifiedName);
        }

        // -------------------------------------------------------------------

        public void Changed()
        {
            int maxIndex = 0;

            lock (m_DependentsLock)
            {
                if (m_Dependents != null)
                {
                    maxIndex = m_Dependents.Count;
                }
                else
                {
                    return;
                }
            }

            for (int index = 0; index < maxIndex; index++)
            {
                lock (m_DependentsLock)
                {
                    if (m_Dependents != null && (maxIndex == m_Dependents.Count))
                    {
                        if(m_Dependents[index] != null)
                        {
                            m_Dependents[index].LocalIOChanged();
                        }
                    }
                    else if(m_Dependents == null)
                    {
                        throw new ArgumentNullException("m_Dependents is null");
                    }
                    else //if maxIndex != m_Dependents.Count
                    {
                        throw new InvalidOperationException("The number of m_Dependents items has changed.");
                    }
                }
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================





    }
}
