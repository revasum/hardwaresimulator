﻿using RevasumHardwareSimulator.mvp_Models.IOPoints;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;
using System.Windows;
using TwinCAT;
using TwinCAT.Ads;
using TwinCAT.Ads.TypeSystem;
using TwinCAT.TypeSystem;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Utilities;

namespace RevasumHardwareSimulator.mvp_Models
{
    /// <summary>
    ///     Our connection to the PLC, via ADS protocol
    /// </summary>
    public sealed class PlcAccessor : RevasumBaseObject
    {
        private readonly string PLC_IP_ADDRESS = "127.0.0.1.1.1";
        private readonly int PLC_PORT_NUMBER = 851;

        private TcAdsClient m_ADSClient;

        private Dictionary<int, IOPoint> m_notificationHandles;

        private static readonly PlcAccessor _singletonPlcAccessor = new PlcAccessor();

        public Diagnostics WriteDiagnostics { get; private set; } = new Diagnostics();

        public Diagnostics ReadDiagnostics { get; private set; } = new Diagnostics();

        public Diagnostics UpdateDiagnostics { get; private set; } = new Diagnostics();

        /// <summary>
        /// For logging purposes
        /// </summary>
        private StackTrace m_StackTrace = new StackTrace();

        /// <summary>
        ///     Not sure why this is here, I venture to guess this belongs somewhere else
        /// </summary>
        public IOPoint SelectedPoint
        {
            get
            {
                return m_selectedPoint;
            }
            set
            {
                if (m_selectedPoint != null)
                {
                    m_selectedPoint.IsSelected = false;
                }

                if (m_selectedPoint == value)
                {
                    m_selectedPoint = null;
                }
                else
                {
                    m_selectedPoint = value;

                    if (m_selectedPoint != null)
                    {
                        m_selectedPoint.IsSelected = true;
                    }
                }
            }
        }
        private IOPoint m_selectedPoint;

        public IOPointList PLCPoints
        {
            get
            {
                return m_plcPoints;
            }
        }
        private IOPointList m_plcPoints = new IOPointList();


        /// <summary>
        ///     This is a singleton, use the 'Instance' property instead
        /// </summary>
        private PlcAccessor()
        {
            // NOTE: Enable logging by uncommenting out the line below
            //StartLogging();

            // hide the ctor to prevent people from instantiating more than one
        }

        ~PlcAccessor()
        {
            // NOTE: Enable logging by uncommenting out the line below
            //StopLogging();
        }

        /// <summary>
        ///     Use this property
        /// </summary>
        /// <returns></returns>
        public static PlcAccessor Instance()
        {
            return _singletonPlcAccessor;
        }

        /// <summary>
        ///     Connect to...and load up the I/O points and the symbols
        /// </summary>
        /// <returns></returns>
        public bool LoadPLC()
        {
            return (ConnectToPLC(PLC_IP_ADDRESS, PLC_PORT_NUMBER));
        }

        /// <summary>
        ///     Load up the I/O points and the symbols
        /// </summary>
        /// <param name="ipAddress"></param>
        /// <param name="port"></param>
        /// <returns></returns>
        private bool ConnectToPLC(string ipAddress, int port)
        {
            Boolean result = false;


            try
            {
                m_plcPoints = new IOPointList();

                TcAdsClient tcAdsClient = new TcAdsClient();

                tcAdsClient.Connect(ipAddress, port);

                List<ITcAdsSymbol> allSymbols = new List<ITcAdsSymbol>();

                // warning, this is deprecated. new loader will require tree recursion
                TcAdsSymbolInfoLoader symbolLoader = tcAdsClient.CreateSymbolInfoLoader();

                var symbols = symbolLoader.GetSymbols(true);

                foreach (TcAdsSymbolInfo symbol in symbols)
                {
                    Console.WriteLine(symbol.Name);

                    IOPoint aPoint = null;

                    allSymbols.Add(symbol);

                    if (symbol.Name.Contains(".stModuleIO.") || symbol.Name.Equals("Global_Variables_HMI.diGlobalHardwareSimulator") || (symbol.Name.Contains("MAIN.fbModule") && symbol.Name.Contains("stAxisParametersForSimu")))
                    {
                        aPoint = IOPoint.FromAdsSymbolInfo(symbol);
                    }
                    else if (symbol.Name.Contains("PLCVersion"))
                    {
                        m_plcPoints.PLCComment = symbol.Comment;
                    }

                    if (aPoint != null)
                    {
                        aPoint.Position = allSymbols.Count;
                        m_plcPoints.Add(aPoint);
                    }
                }

                // were we able to load any symbols??
                result = (allSymbols.Count > 0);

                m_ADSClient = tcAdsClient;

                Logger.LogMessage(ToString(), m_StackTrace.GetFrame(1).GetMethod().Name, $"Successfully connected to PLC.");
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("could not be found"))
                {
                    MessageBox.Show(String.Format("Could not connect to the PLC socket {0}:{1} {2}{2}{3}", ipAddress, port, Environment.NewLine, ex.ToString()), "ADS Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    MessageBox.Show(String.Format("An error occurred while retrieving the Symbols from the ADS Server. Ensure the 'Master_PLC' project 'AutoStart' property is checked. {0}{0}{1}", Environment.NewLine, ex.ToString()), "ADS Symbol Access Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }

                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    ex);
            }

            return result;
        }

        /// <summary>
        ///     Generate I/O handles and connect notification event handlers
        /// </summary>
        /// <param name="ioPoints"></param>
        /// <returns></returns>
        public bool ConnectToConfiguration(List<IOPoint> ioPoints)
        {
            bool result = false;
            Stopwatch stopwatch = Stopwatch.StartNew();

            Console.WriteLine($"ConnectToConfiguration Started: {stopwatch.Elapsed}");

            Console.WriteLine($"Before result = EstablishADSHandles(ioPoints): {stopwatch.Elapsed}");

            result = EstablishADSHandles(ioPoints);

            Console.WriteLine($"After result = EstablishADSHandles(ioPoints): {stopwatch.Elapsed}");

            // 800 ms
            if (result)
            {
                Console.WriteLine($"Before result = SetupADSNotification(ioPoints): {stopwatch.Elapsed}");

                // 1,400 ms
                result = SetupADSNotification(ioPoints);

                Console.WriteLine($"After result = SetupADSNotification(ioPoints): {stopwatch.Elapsed}");
            }

            Console.WriteLine($"ConnectToConfiguration Completed: {stopwatch.Elapsed}");

            return result;
        }

        /// <summary>
        ///     Generate I/O handles
        /// </summary>
        /// <param name="pointList"></param>
        /// <returns></returns>
        private bool EstablishADSHandles(List<IOPoint> pointList)
        {
            List<string> good = new List<string>();
            List<string> bad = new List<string>();

            System.Threading.Tasks.Parallel.ForEach(pointList, aPoint =>
            {
                if (aPoint.IsAnalog || aPoint.IsDigital || aPoint.IsAxisCommandStructure)
                {
                    bool goodRead = true;
                    string variableName = aPoint.ADSFullName;

                    try
                    {
                        ITcAdsSymbol aSymbolInfo = m_ADSClient.ReadSymbolInfo(variableName);

                        var handle = m_ADSClient.CreateVariableHandle(variableName);

                        aPoint.ADSHandle = handle;

                        //Console.WriteLine(String.Format("{0} = {1}", aPoint.ADSHandle, aPoint.QualifiedName));
                    }
                    catch
                    {
                        goodRead = false;
                    }

                    if (goodRead)
                    {
                        good.Add(variableName);
                    }
                    else
                    {
                        bad.Add(variableName);
                    }
                }
            });

            if(bad.Count == 0)
            {
                Logger.LogMessage(ToString(),
                                  m_StackTrace.GetFrame(1).GetMethod().Name,
                                  $"Successfully Establish ADS Handles.");
            }
            else
            {
                Logger.LogMessage(ToString(),
                                  m_StackTrace.GetFrame(1).GetMethod().Name,
                                  $"Failed to Establish ADS Handles.");

            }

            return bad.Count == 0;
        }


        /// <summary>
        ///     Connect notification event handlers
        /// </summary>
        /// <param name="pointList"></param>
        /// <returns></returns>
        public bool SetupADSNotification(List<IOPoint> pointList)
        {
            int totalHandles = 0;
            m_notificationHandles = new Dictionary<int, IOPoint>();
            List<IOPoint> tmp = new List<IOPoint>();
            try
            {


                // NOTE: can't make this one a parallel task because the ADS Client is not thread safe when it comes to Notification Events (they are N + 1)
                foreach (IOPoint aPoint in pointList)
                {
                    string variableName = aPoint.ADSFullName;
                    int handle = 0;

                    if (aPoint.IsOutput)
                    {
                        if (aPoint.IsDigital)
                        {
                            handle = m_ADSClient.AddDeviceNotificationEx(variableName, AdsTransMode.OnChange, 100, 0, null, typeof(byte));
                        }
                        else if (aPoint.IsAnalog)
                        {
                            handle = m_ADSClient.AddDeviceNotificationEx(variableName, AdsTransMode.OnChange, 100, 0, null, typeof(ushort));
                        }
                        else if (aPoint.IsOutputMotorControlStructure)
                        {
                            handle = m_ADSClient.AddDeviceNotificationEx(variableName, AdsTransMode.OnChange, 100, 0, null, typeof(MemoryLayoutPLC2NC40));
                        }
                    }
                    else if (aPoint.IsInputMotorControlStructure)  // to capture Position update
                    {
                        handle = m_ADSClient.AddDeviceNotificationEx(variableName, AdsTransMode.OnChange, 100, 0, null, typeof(MemoryLayoutNC2PLC));
                    }

                    if (handle != 0)
                    {
                        //Console.WriteLine(String.Format("{0} = {1}", handle, aPoint.QualifiedName));
                        if (aPoint.QualifiedName.Contains("axis") || aPoint.QualifiedName.ToUpper().Contains("HOME"))
                        {
                            tmp.Add(aPoint);
                        }
                        m_notificationHandles.Add(handle, aPoint);

                        m_ADSClient.AdsNotificationEx += new AdsNotificationExEventHandler(ADSStatusUpdate);
                        m_ADSClient.AdsNotificationError += new AdsNotificationErrorEventHandler(ADSNotificationErrorEvent);

                        totalHandles++;
                    }
                    else
                    {
                        // TODO: Determine how an invalid handle should be addressed
                    }
                }
            }
            catch
            {
                return false;
            }

            if (totalHandles > 0)
            {
                Logger.LogMessage(ToString(),
                                  m_StackTrace.GetFrame(1).GetMethod().Name,
                                  $"Successfully Setup ADS Notification.");

            }
            else
            {
                Logger.LogMessage(ToString(),
                                  m_StackTrace.GetFrame(1).GetMethod().Name,
                                  $"Failed to Setup ADS Notification.");
            }

            return totalHandles > 0;
        }

        /// <summary>
        ///     ADS event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ADSStatusUpdate(object sender, AdsNotificationExEventArgs e)
        {
            int handle = e.NotificationHandle;

            if (m_notificationHandles.TryGetValue(handle, out IOPoint aPoint))
            {
                aPoint.PLCValueChanged(e.Value);
            }

            UpdateDiagnostics.UpdateCount++;
        }

        /// <summary>
        /// Handles the ADS Notification Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ADSNotificationErrorEvent(object sender, AdsNotificationErrorEventArgs e)
        {
            Logger.LogException(ToString(),
                                m_StackTrace.GetFrame(1).GetMethod().Name,
                                e.Exception);

            UpdateDiagnostics.ErrorCount++;
        }

        const int NumberOfRetries = 5;

        /// <summary>
        /// Writes the given byte value for the given ADS Handle
        /// </summary>
        /// <param name="adsHandle"></param>
        /// <param name="value"></param>
        public void Write(int adsHandle, byte value)
        {
            AdsStream adsStream = new AdsStream();

            AdsBinaryWriter binaryWriter = new AdsBinaryWriter(adsStream);

            try
            {
                binaryWriter.Write(value);
            }
            catch (IOException e)
            {
                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    e);

                WriteDiagnostics.ErrorCount++;
            }

            TryWrite(adsHandle, adsStream);
        }

        /// <summary>
        /// Writes the given short value for the given ADS Handle
        /// </summary>
        /// <param name="adsHandle"></param>
        /// <param name="value"></param>
        public void Write(int adsHandle, short value)
        {
            AdsStream adsStream = new AdsStream();

            AdsBinaryWriter binaryWriter = new AdsBinaryWriter(adsStream);

            try
            {
                binaryWriter.Write(value);
            }
            catch (IOException e)
            {
                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    e);

                WriteDiagnostics.ErrorCount++;
            }

            TryWrite(adsHandle, adsStream);
        }

        /// <summary>
        /// Writes the given memory layout for the given ADS Handle
        /// </summary>
        /// <param name="adsHandle"></param>
        /// <param name="memory"></param>
        public void Write(int adsHandle, MemoryLayoutNC2PLC memory)
        {
            AdsStream adsStream = new AdsStream();

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            try
            {
                binaryFormatter.Serialize(adsStream, memory);
            }
            catch (SerializationException serializationException)
            {
                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    serializationException);

                WriteDiagnostics.ErrorCount++;
            }
            catch (Exception e)
            {
                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    e);

                WriteDiagnostics.ErrorCount++;
            }

            TryWrite(adsHandle, adsStream);

        }

        /// <summary>
        /// Attempts to write the given ADS Stream for the given ADS Handle
        /// </summary>
        /// <param name="adsHandle"></param>
        /// <param name="adsStream"></param>
        private void TryWrite(int adsHandle, AdsStream adsStream)
        {
            int retryCount = 0;
            bool ok = false;
            AdsErrorCode errorCode = AdsErrorCode.NoError;

            while (!ok && retryCount < NumberOfRetries)
            {
                WriteDiagnostics.StartRecording();

                errorCode = m_ADSClient.TryWrite(adsHandle, adsStream, 0, (int)adsStream.Length);

                WriteDiagnostics.StopRecording();

                if (errorCode == AdsErrorCode.NoError)
                {
                    ok = true;
                }
                else
                {
                    retryCount++;

                    Logger.LogMessage(ToString(),
                                      m_StackTrace.GetFrame(1).GetMethod().Name,
                                      $"Failed to write to ADS Client. Error Code: {errorCode}. Retry #{retryCount}...");

                    WriteDiagnostics.RetryCount++;
                }
            }

            if (retryCount == NumberOfRetries)
            {
                Logger.LogMessage(ToString(),
                                  m_StackTrace.GetFrame(1).GetMethod().Name,
                                  $"Failed to write to ADS Client after {NumberOfRetries} retries. Last Error Code: {errorCode}.");

                WriteDiagnostics.ErrorCount++;
            }
        }

        /// <summary>
        /// Reads the data stream for the given ADS Handle
        /// </summary>
        /// <param name="adsHandle"></param>
        /// <param name="dataStream"></param>
        public void Read(int adsHandle, AdsStream dataStream)
        {
            try
            {
                ReadDiagnostics.StartRecording();

                m_ADSClient.Read(adsHandle, dataStream);

                ReadDiagnostics.StopRecording();
            }
            catch (AdsErrorException e)
            {
                Logger.LogException(ToString(),
                                    m_StackTrace.GetFrame(1).GetMethod().Name,
                                    e);
            }
        }
    }
}
