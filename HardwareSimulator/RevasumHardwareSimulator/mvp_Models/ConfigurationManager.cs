﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TwinCAT.Ads;
using RevasumHardwareSimulator.mvp_Presenters.ConfigurationManagementPresenters;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using System.Windows;

namespace RevasumHardwareSimulator.mvp_Models
{
    public class ConfigurationManager : RevasumBaseObject, 
        ISubscriber<ConfigurationModified>, 
        ISubscriber<BehaviorCreated>, 
        ISubscriber<BehaviorDeleted>
    {

        private readonly string m_currentFileName;

        static ConfigurationManager s_current;
        IOConfiguration m_currentConfiguration;


        // --------------------------------------------------------------------------------------

        private ConfigurationManager()
        {
            m_currentFileName = Path.Combine(
                FileManager.ConfigurationFilesFolderName,
                "IOConfiguration.current");

            m_currentConfiguration = new IOConfiguration();

            Register<ConfigurationModified>(this);
            Register<BehaviorCreated>(this);
            Register<BehaviorDeleted>(this);

        }

        // --------------------------------------------------------------------------------------

        public static ConfigurationManager Current
        {
            get
            {
                if (s_current == null)
                    s_current = new ConfigurationManager();
                return s_current;
            }
        }

        // --------------------------------------------------------------------------------------

        public static string DateToString(DateTime timeStamp)
        {
            string answer = String.Format("on {0} at {1}", timeStamp.ToString("MM/dd/yy"), timeStamp.ToString("HH:mm:ss"));
            return answer;
        }

        // --------------------------------------------------------------------------------------

        #region IO Configuration

        // --------------------------------------------------------------------------------------

        public void CreateNewConfiguration(IOPointList points, string projectName)
        {
            m_currentConfiguration = new IOConfiguration(points, projectName);
            Save();
        }

        // --------------------------------------------------------------------------------------

        public bool ConfigurationExists
        {
            get { return m_currentConfiguration != null; }
        }

        // --------------------------------------------------------------------------------------

        public IOConfiguration CurrentConfiguration
        {
            get  { return m_currentConfiguration; }
            set { 
                m_currentConfiguration = value;
                FileTabPresenter.Current.NewConfigurationLoaded();
            }
        }

        // --------------------------------------------------------------------------------------

        public List<IOPoint> CurrentConfigurationPoints
        {
            get { return m_currentConfiguration.Points; }
        }

        // --------------------------------------------------------------------------------------

        public int NumberOfBehaviors
        {
            get { return m_currentConfiguration.NumberOfBehaviors; }
        }

        // --------------------------------------------------------------------------------------

        public int NumberOfDefaults
        {
            get { return m_currentConfiguration.NumberOfDefaults; }
        }

        // --------------------------------------------------------------------------------------

        public DateTime CurrentConfigurationCreationTime
        {
            get { return m_currentConfiguration.CreationTIme; }
        }

        // --------------------------------------------------------------------------------------

        public DateTime CurrentConfigurationModificationTime
        {
            get { return m_currentConfiguration.ModifyTime; }
        }

        // --------------------------------------------------------------------------------------

        #endregion

        // --------------------------------------------------------------------------------------

        #region Save / Restore

        // --------------------------------------------------------------------------------------

        string CreateFileName(DateTime creationTime, string projectName)
        {

            return Path.Combine(
                FileManager.ConfigurationFilesFolderName,
                projectName + "_" +
                creationTime.ToString("yyMMdd_HHmmss") +
                ".configuration");
        }

        // --------------------------------------------------------------------------------------

        private void SaveConfigurationName(IOConfiguration config)
        {

            using (FileStream stream =
                File.Open(m_currentFileName, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, config.CreationTIme);
                formatter.Serialize(stream, config.ProjectName);
            }
        }

        // --------------------------------------------------------------------------------------

        private void GetCurrentFileCreationTime(out DateTime creationTime, out string projectName)
        {
            if (File.Exists(m_currentFileName))
            {
                using (FileStream stream = File.Open(m_currentFileName, FileMode.Open))
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    creationTime = (DateTime)formatter.Deserialize(stream);
                    projectName = (string)formatter.Deserialize(stream);
                    return;
                }
            }
            creationTime = DateTime.MinValue;
            projectName = "can't read";
        }

        // --------------------------------------------------------------------------------------

        public void SetCurrentFile(IOConfiguration config)
        {
            m_currentConfiguration = config;
            SaveConfigurationName(config);
        }

        // --------------------------------------------------------------------------------------

        public void Save()
        {
            SaveConfigurationName(m_currentConfiguration);
            string fileName = CreateFileName(m_currentConfiguration.CreationTIme, m_currentConfiguration.ProjectName);

            m_currentConfiguration.ModifiedNow();
            using (FileStream stream =
                File.Open(fileName, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, m_currentConfiguration);
            }
        }

        // --------------------------------------------------------------------------------------

        public bool Restore()
        {
            GetCurrentFileCreationTime(out DateTime dateTime, out string project);

            if (dateTime != DateTime.MinValue)
            {
                string fileName = CreateFileName(dateTime, project);
                if (File.Exists(fileName) == false)
                {
                    MessageBox.Show("Error: Could not find latest file: " + fileName);
                    return false;
                }
                else
                { 
                	IOConfiguration config = Restore(fileName);
                	if (config == null) return false;
                	SetCurrentFile(config);
                	return true;
                }
            }

            return false;
        }

        // --------------------------------------------------------------------------------------

        public IOConfiguration Restore(string fileName)
        {
            IOConfiguration config;
            FileStream stream = File.Open(fileName, FileMode.OpenOrCreate);
            if (stream.Length == 0)
            {
                MessageBox.Show("Selected file is empty: " + fileName + ". Please select a new one.");
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.Filter = "Configuration Files (*.configuration)|*.configuration";
                dlg.InitialDirectory = FileManager.ConfigurationFilesFolderName;
                Nullable<bool> result = dlg.ShowDialog();
                fileName = dlg.FileName;
                if (fileName == "") return null;
                stream = File.Open(fileName, FileMode.OpenOrCreate);
            }
            using (new WaitCursor())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                config = (IOConfiguration)formatter.Deserialize(stream);
                config.RestoreBehaviorLinks();
            }

            return config;
        }

        // --------------------------------------------------------------------------------------

        public void Backup()
        {

            DateTime currentTime = DateTime.Now;
            string fileName = CreateFileName(currentTime, m_currentConfiguration.ProjectName);

            // temporarily backup the original dates
            string tempComment = m_currentConfiguration.ConfigurationComment;
            DateTime tempCreated = m_currentConfiguration.CreationTIme;
            DateTime tempModified = m_currentConfiguration.ModifyTime;

            // replace
            m_currentConfiguration.ConfigurationComment = "Backup from a configuration created " + DateToString(tempCreated);
            m_currentConfiguration.CreationTIme = currentTime;
            m_currentConfiguration.ModifyTime = currentTime;

            using (FileStream stream =
                File.Open(fileName, FileMode.OpenOrCreate))
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, m_currentConfiguration);
            }

            // restore
            m_currentConfiguration.ConfigurationComment = tempComment;
            m_currentConfiguration.CreationTIme = tempCreated;
            m_currentConfiguration.ModifyTime = tempModified;
        }

        // --------------------------------------------------------------------------------------

        #endregion

        // ================================================================================================

        #region ISubscriber

        // ================================================================================================

        public void Receive(ConfigurationModified message)
        {
           // this.Save();
            XMLConfiguration.Save(this.CurrentConfiguration.ProjectName, this.CurrentConfiguration);
        }

        // --------------------------------------------------------------------------------------

        public void Receive(BehaviorCreated message)
        {
            m_currentConfiguration.CountBehaviorsAndDefaults();
        }

        // --------------------------------------------------------------------------------------

        public void Receive(BehaviorDeleted message)
        {
            m_currentConfiguration.CountBehaviorsAndDefaults();
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
