﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models
{
    /// <summary>
    /// Defines GUI notification, event aggregator and logging interface 
    /// </summary>
    [Serializable]
    public abstract class RevasumBaseObject : INotifyPropertyChanged
    {
        private bool m_LoggingEnabled = false;

        // -------------------------------------------------------------------

        public RevasumBaseObject()
        {

        }

        // -------------------------------------------------------------------

        #region Event Aggregator

        // -------------------------------------------------------------------

        [NonSerialized]
        protected IEventAggregator m_eventAggregator;

        // -------------------------------------------------------------------

        // made it public so other objects can register me as well
        public void Register<T>(ISubscriber<T> anObject)
        {
            if (m_eventAggregator == null)
                m_eventAggregator = EventAggregator.Current;

            m_eventAggregator.Subscribe<T>(anObject);
        }

        // -------------------------------------------------------------------

        /// <summary>
        /// All objects that registered with this class will be notified
        /// </summary>
        protected void Publish<T>(T message)
        {
            if (m_eventAggregator == null)
                m_eventAggregator = EventAggregator.Current;

            m_eventAggregator.Publish(message);
        }

        // -------------------------------------------------------------------

        #endregion

        // -------------------------------------------------------------------

        #region GUI Notification

        // -------------------------------------------------------------------

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;

        // -------------------------------------------------------------------

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        // -------------------------------------------------------------------

        #endregion

        // -------------------------------------------------------------------

        #region Logging

        // ------------------------------------------------------------------- 

        /// <summary>
        /// Starts logging within the given object
        /// </summary>
        protected void StartLogging()
        {
            m_LoggingEnabled = true;

            Logger.LogMessage();
        }

        /// <summary>
        /// Stops logging within the given object
        /// </summary>
        protected void StopLogging()
        {
            m_LoggingEnabled = false;

            Logger.LogMessage();
        }

        /// <summary>
        /// Logs the message with the given information
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="message"></param>
        protected void LogMessage(string className, string methodName, string message)
        {
            if (m_LoggingEnabled)
            {
                Logger.LogMessage(className, methodName, message);
            }
        }

        /// <summary>
        /// Logs the exception with the given information
        /// </summary>
        /// <param name="className"></param>
        /// <param name="methodName"></param>
        /// <param name="ex"></param>
        protected void LogException(string className, string methodName, Exception ex)
        {
            if (m_LoggingEnabled)
            {
                LogException(className, methodName, ex);
            }
        }

        protected string GetSenderMethod()
        {
            // stack frame 0 = GetSenderInfo (this method)
            // stack frame 1 = Log
            // stack frame 2 = the one we want (who called Log())
            System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace();
            System.Diagnostics.StackFrame frame = trace.GetFrame(3);
            return frame.GetMethod().Name;
        }
        // ------------------------------------------------------------------- 


        #endregion

        // ------------------------------------------------------------------- 

        #region To do

        // ------------------------------------------------------------------- 

        /// <summary>
        /// call this method with information of any code that needs to be revisited.
        /// </summary>
        public static void ToDo(string aMessage, string className)
        {
            Console.WriteLine("To DO : " + className + "\t" + aMessage);
        }
        // ------------------------------------------------------------------- 

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public double DoubleValueAtAttribute(XMLNode node, string attribute, double defaultValue)
        {
            string sValue = node.AttributeAt(attribute);

            if (!Double.TryParse(sValue, out double dValue))
            {
                dValue = defaultValue;
            }

            return dValue;
        }

        // --------------------------------------------------------------------------------------------------

        public int IntValueAtAttribute(XMLNode node, string attribute, int defaultValue)
        {
            string sValue = node.AttributeAt(attribute);

            if (!Int32.TryParse(sValue, out int iValue))
            {
                iValue = defaultValue;
            }

            return iValue;
        }

        // --------------------------------------------------------------------------------------------------

        public bool BoolValueAtAttribute(XMLNode node, string attribute)
        {
            string sValue = node.AttributeAt(attribute).ToUpper();
            return (sValue == "TRUE");
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================



    }
}
