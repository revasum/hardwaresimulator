﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

namespace RevasumHardwareSimulator.mvp_Models
{


    /// <summary>
    /// a List of IOPoints as read from PLC with additional PLC comments
    /// </summary>
    [Serializable]
    public class IOPointList
    {

        // --------------------------------------------------------------------------------------

        List<IOPoint> m_list = new List<IOPoint>();

        // --------------------------------------------------------------------------------------

        public void Add(IOPoint point)
        {
            m_list.Add(point);
        }

        // --------------------------------------------------------------------------------------

        public int Count
        {
            get { return m_list.Count; }
        }

        // --------------------------------------------------------------------------------------

        /// <summary>
        /// Read-Only Comment Read from PLC
        /// </summary>
        public string PLCComment { get; set; }

        // --------------------------------------------------------------------------------------

        public List<IOPoint> AllPoints
        {
            get { return m_list; }
        }

        // --------------------------------------------------------------------------------------

        public List<IOPoint> AllPointsCopy
        {
            get
            {
                List<IOPoint> copy = new List<IOPoint>();
                foreach (IOPoint point in AllPoints)
                    copy.Add(point);
                return copy;
            }
        }

        // --------------------------------------------------------------------------------------

    }
}
