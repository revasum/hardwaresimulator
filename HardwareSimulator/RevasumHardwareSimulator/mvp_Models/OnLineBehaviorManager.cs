﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters;
using System.Windows.Controls;

//using RevasumHardwareSimulator.Model;


namespace RevasumHardwareSimulator.mvp_Models
{
    public class OnLineBehaviorManager
    {


        // ================================================================================================

        #region Constants

        // ================================================================================================

        static readonly string ViewNamespace = "RevasumHardwareSimulator.mvp_Views.OnLineBehaviorViews.";
        static readonly string PresenterNamespace = "RevasumHardwareSimulator.mvp_Presenters.OnLineBehaviorPresenters.";

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Construction

        // ================================================================================================

        static List<OnLineBehaviorDescription> s_mvpNames;

        // --------------------------------------------------------------------------------------------------

        static OnLineBehaviorManager()
        {

            s_mvpNames = new List<OnLineBehaviorDescription>();

            // for each of the points below, enter:
            //      1. the text that should appear in the combo box (asking for behavior)
            //      2. the name of the Behavior class that should be instantiated based on the selection (must be a subclass of Behavior)
            //      3. the name of view class representing the behavior (must be a subclass of UserControl)
            //      4. the name of presenter that should serve the view, based on the behavior data (must be a subclass of EditBehaviorPresenter)

            // See below for the Namespaces where the classes are expected to be found


            s_mvpNames.Add(new OnLineBehaviorDescription(
                "AND",
                "OnLineView_AND",
                "OnLineBehaviorPresenter_AND"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
               "OR",
               "OnLineView_OR",
               "OnLineBehaviorPresenter_OR"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
               "NOT",
               "OnLineView_NOT",
               "OnLineBehaviorPresenter_NOT"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
               "LINEAR",
               "OnLineView_LINEAR",
               "OnLineBehaviorPresenter_LINEAR"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
               "THRESHOLD",
               "OnLineView_THRESHOLD",
               "OnLineBehaviorPresenter_THRESHOLD"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
               "AXIS",
               "OnLineView_AXIS",
               "OnLineBehaviorPresenter_AXIS"));

            s_mvpNames.Add(new OnLineBehaviorDescription(
                "PI_Control",
                "OnLineView_PI_Control",
                "OnLineBehaviorPresenter_PI_Control"
                ));

            s_mvpNames.Add(new OnLineBehaviorDescription(
                "Script",
                "OnLineView_Script",
                "OnLineBehaviorPresenter_Script"
                ));
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Create Presenter

        // ================================================================================================

        public static OnLineBehaviorPresenter GetPresenter(IOPoint target)
        {
            string behaviorType = target.BehaviorTypeName;
            OnLineBehaviorDescription triplet = s_mvpNames.Find(x => x.BehaviorFunction == behaviorType);
            OnLineBehaviorPresenter presenter = GetPresenter(triplet.Presenter);
            UserControl view = GetView(triplet.View);
            presenter.Target = target;
            //presenter.View = view;
            presenter.SetView(view);
            return presenter;
        }

        // --------------------------------------------------------------------------------------------------

        static OnLineBehaviorPresenter GetPresenter(string presenterName)
        {
            var handle = Activator.CreateInstance(null, PresenterNamespace + presenterName);
            OnLineBehaviorPresenter presenter = (OnLineBehaviorPresenter)handle.Unwrap();
            return presenter;
        }

        // --------------------------------------------------------------------------------------------------

        static UserControl GetView(string viewName)
        {
            var handle = Activator.CreateInstance(null, ViewNamespace + viewName);
            UserControl view = (UserControl)handle.Unwrap();
            return view;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
