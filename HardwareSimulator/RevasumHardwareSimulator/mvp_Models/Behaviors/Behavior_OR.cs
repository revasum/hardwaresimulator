﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    [Serializable]
    public class Behavior_OR : Behavior_ListInputs
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public Behavior_OR()
        {
            m_imageSource = "add2.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_OR(XMLNode node)
            : base(node)
        {
            m_imageSource = "add2.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("To turn the output on:\n");
                sb.Append("\t1.  Wait for ANY input to go on\n");
                sb.Append("\t2. Wait for an optional delay (ON Delay)\n\n");

                sb.Append("To turn the output off:\n");
                sb.Append("\t1. Wait for ALL the inputs to go off\n");
                sb.Append("\t2. Wait for an optional delay (OFF Delay)\n\n");

                sb.Append("Valid Inputs:\n");
                sb.Append("\t Must be Digital");

                return sb.ToString();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "OR"; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Operation

        // ================================================================================================

        public override void PerformOnLineUpdate()
        {
            bool digitalValue = false;
            foreach (SelectedIDigitalPointTemplate digital in Inputs)
            {
                digital.ValueChanged();

                if (digital.IsSet)
                {
                    digitalValue = true;
                    break;
                }
            }
            SetTarget(digitalValue);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================





    }
}
