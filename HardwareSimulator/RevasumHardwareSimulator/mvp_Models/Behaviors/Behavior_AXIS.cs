﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    [Serializable]
    public class Behavior_AXIS : Behavior
    {

        // =======================================================================================================================

        #region Initialize

        // =======================================================================================================================

        public Behavior_AXIS()
        {
            m_imageSource = "trapezoid.png";
        }

        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            if (InputName != "")
                Input = FindPointByName(InputName, points);
        }

        // --------------------------------------------------------------------------------------------------

        public override void OnLine()
        {
            base.OnLine();
            m_input.AddDependent(this);
            PerformOnLineUpdate();
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Properties

        // =======================================================================================================================

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("NOTE: This Behavior is Obsolete!\n\n");
                sb.Append("   We need to use Beckhoff simulator because\n");
                sb.Append("      we don't know how to enable an axis\n\n\n");
                sb.Append("Use this Behavior only to test Axis Position\n\n");
                return sb.ToString();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "AXIS"; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region New Move Request

        // =======================================================================================================================


        public override void PerformOnLineUpdate()
        {

            IAxisPoint axisPoint = m_input as IAxisPoint;

            int direction = axisPoint.Direction;
            AxisDirectionsEnum directionEnum = (AxisDirectionsEnum)direction;
            CommandedDirection = directionEnum.ToString();

            int command = axisPoint.Command;
            AxisCommandsEnum commandEnum = (AxisCommandsEnum)command;
            CommandedCommand = commandEnum.ToString();

            CommandedVelocity = axisPoint.Velocity;
            CommandedAcceleration = axisPoint.Acceleration;
            CommandedPosition = axisPoint.AxisPosition;

            IAxisPoint target = Target as IAxisPoint;
            ActualPosition = target.AxisPosition;
            m_calc_Position_Absolute_Start = ActualPosition;

            switch (commandEnum)
            {
                case AxisCommandsEnum.eCommand_MoveAbsolute_Axis:
                    m_calc_Position_Absolute_Target = axisPoint.AxisPosition;
                    PrepareForPositionMove();
                    break;
                case AxisCommandsEnum.eCommand_MoveRelative_Axis:
                    m_calc_Position_Absolute_Target = axisPoint.AxisPosition + target.AxisPosition;
                    PrepareForPositionMove();
                    break;
                //case AxisCommandsEnum.eCommand_MoveVelocity_Axis:
                //    PrepareForVelocityMove(directionEnum);
                //    break;
                //default:
                //    if (m_timing)
                //    {
                //        m_startStopMsec = -1;
                //        m_stopping = true;
                //    }
                //    break;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Input Interface

        // =======================================================================================================================

        protected string m_inputName;
        public string InputName
        {
            get { return m_inputName; }
            set { m_inputName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        protected IOPoint m_input;

        public IOPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                if (m_input != null)
                    InputName = value.QualifiedName;
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_commandedPosition;
        public double CommandedPosition
        {
            get { return m_commandedPosition; }
            set
            {
                m_commandedPosition = value;
                OnPropertyChanged("CommandedPosition");
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_commandedVelocity;
        public double CommandedVelocity
        {
            get { return m_commandedVelocity; }
            set
            {
                m_commandedVelocity = value;
                OnPropertyChanged("CommandedVelocity");
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_commandedAcceleration;
        public double CommandedAcceleration
        {
            get { return m_commandedAcceleration; }
            set
            {
                m_commandedAcceleration = value;
                OnPropertyChanged("CommandedAcceleration");
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        string m_commandedDirection;
        public string CommandedDirection
        {
            get { return m_commandedDirection; }
            set
            {
                m_commandedDirection = value;
                OnPropertyChanged("CommandedDirection");
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        string m_commandedCommand;
        public string CommandedCommand
        {
            get { return m_commandedCommand; }
            set
            {
                m_commandedCommand = value;
                OnPropertyChanged("CommandedCommand");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Target Interface

        // =======================================================================================================================

        [NonSerialized]
        double m_actualPosition;
        public double ActualPosition
        {
            get { return m_actualPosition; }
            set
            {
                m_actualPosition = value;
                OnPropertyChanged("ActualPosition");
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_actualVelocity;
        public double ActualVelocity
        {
            get { return m_actualVelocity; }
            set
            {
                m_actualVelocity = value;
                OnPropertyChanged("ActualVelocity");
            }
        }

        // --------------------------------------------------------------------------------------------------


        [NonSerialized]
        string m_movePhase = "";
        public string MovePhase
        {
            get { return m_movePhase; }
            set
            {
                m_movePhase = value;
                OnPropertyChanged("MovePhase");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Move - Comments and Variables

        // =======================================================================================================================

        #region Comments

        /* Considerations during a move:
         
                0. We are being given the total distanceto travel S (mm), the move velocity Vel (mm/sec), and aceleration (same as deceleration) Acc, Dec (mm/(sec * sec) )
         
                1. Assuming Acc = Dec, we have Time to Acc plus Time to Dec, 
                    Ta = Vel / Acc
                    Tad = 2 * Ta             
          
                2. Distance travelled during Acc + Dec
                    Sa = Vel * Vel / ( 2 * Acc)
                    Sad = Sa * 2
         
                3. Remaining distance travelled during constant velocity move
                    Sv = S - Sad
         
                4. Time during constant velocity move
                    Tv = Sv / Vel
         
                5. Total time travelled
                    T = Tv + Tad
                    
         */

        /* Implementation
         
            We make a little simplification to calculate position update during accel / decel: we replacing it with a not accelerated move of half the velocity.
         
                assuming Sc = current distance travelled, Vc = current velocity, Tc = current time expired,  we have:
         
                1. we create two points:
                    S1 = Sa                    
                    S2 = S1 + Sv
         
                2. Velocity increase pseudocode
         
                if (Sc < S1)
                    ratio = Sc / S1
                else if (Sc < S2)
                    ratio = 1
                else
                    ratio = (S - Sc) / (S - S2)
          
                Vc = Vel * ratio
                   
                3. We create two points
                    T1 = Ta
                    T2 = T1 + Tv
         
                4. position increments
                    during constant move
                        Sic = (Ta / 1000) * Vel      for each expired millisecond 
                    during accel / decel
                        Sia = (Ta / 1000) * (Vel / 2) = Sic / 2    for each expired millisecond
         
                5. Position increment pseudocode
         
                deltaT = thisTime - lastTime
                lastTime = thisTime
                if (Tc < T1)
                    Sc += Sia
                else if (Tc < T2)
                    Sc += Sic
                else
                    Sc += Sia  
                
         */

        #endregion

        // --------------------------------------------------------------------------------------------------

        #region Variables used for move calculatios

        // --------------------------------------------------------------------------------------------------

        double m_calc_Position_Absolute_Start;
        double m_calc_Position_Absolute_Target;
        double m_calc_TotalDistance;

        double m_calc_Position_Relative_Current;
        double m_calc_Position_IncrementPer1Msec;

        double m_calc_CurrentVelocity;

        double m_calc_Time_EndOfAcceleration;
        double m_calc_Time_StartOfDeceleration;
        double m_calc_Total_Time;

        AxisStatesEnum m_axisState;
        int m_calc_Direction;
        bool m_isVelocityMove;
        bool m_stopping = false;

        //double m_calc_Position_Relative_EndOfAcceleration;
        //double m_calc_Position_Relative_StartOfDeceleration;

        // --------------------------------------------------------------------------------------------------

        #endregion

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Position Move

        // =======================================================================================================================

        #region Prepare for Position Move

        // --------------------------------------------------------------------------------------------------

        void PrepareForPositionMove()
        {
            PrepareForPositionMove_AbsoluteUnits();
            PrepareForPositionMove_ZeroOrigin();

            m_calc_Position_Relative_Current = 0;
            m_calc_CurrentVelocity = 0;
            m_isVelocityMove = false;

            StartTimer();
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// total distance travelled will need to translated by the actual starting point and direction
        /// </summary>
        void PrepareForPositionMove_AbsoluteUnits()
        {
            if (m_calc_Position_Absolute_Target > ActualPosition)
            {
                m_calc_TotalDistance = m_calc_Position_Absolute_Target - ActualPosition;
                m_calc_Direction = 1;
            }
            else
            {
                m_calc_TotalDistance = ActualPosition - m_calc_Position_Absolute_Target;
                m_calc_Direction = -1;
            }
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// the calculations assume that move starts at 0 and goes in positive direction through the entire travel distance
        /// </summary>
        void PrepareForPositionMove_ZeroOrigin()
        {
            double distanceToTravelDuringAccel = CommandedVelocity * CommandedVelocity / CommandedAcceleration / 2.0;

            double distanceToTravelDuringAccelDecel = 2.0 * distanceToTravelDuringAccel;
            double distanceToTravelDuringAtSpeed = m_calc_TotalDistance - distanceToTravelDuringAccelDecel;

            double timeToAccel = CommandedVelocity / CommandedAcceleration;
            double timeToAccDec = 2.0 * timeToAccel;
            double timeToTravelDuringAtSpeed = distanceToTravelDuringAtSpeed / CommandedVelocity;
            m_calc_Total_Time = (timeToTravelDuringAtSpeed + timeToAccDec) * 1000;

            m_calc_Time_EndOfAcceleration = 1000.0 * timeToAccel;
            m_calc_Time_StartOfDeceleration = 1000.0 * (timeToAccel + timeToTravelDuringAtSpeed);
            m_calc_Position_IncrementPer1Msec = CommandedVelocity * timeToAccel / 1000.0;


            // present time in milliseconds
            //m_calc_Position_Relative_EndOfAcceleration = distanceToTravelDuringAccel;
            //m_calc_Position_Relative_StartOfDeceleration = distanceToTravelDuringAccel + distanceToTravelDuringAtSpeed;


        }

        // --------------------------------------------------------------------------------------------------

        #endregion

        // --------------------------------------------------------------------------------------------------

        #region Execute Position Move

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// this function assumes that we move from zero in positive direction through total desired distance
        /// </summary>
        void UpdatePosition_ZeroOrigin_PositionMove(long totalDelta, long m_currentDelta)
        {
            double positionIncrement;
            double velocityRatio;
            if (totalDelta < m_calc_Time_EndOfAcceleration)
            {
                positionIncrement = m_calc_Position_IncrementPer1Msec / 2.0;
                velocityRatio = totalDelta / m_calc_Time_EndOfAcceleration;
                m_axisState = AxisStatesEnum.Acceleration;
            }
            else if (totalDelta < m_calc_Time_StartOfDeceleration)
            {
                positionIncrement = m_calc_Position_IncrementPer1Msec;
                velocityRatio = 1;
                m_axisState = AxisStatesEnum.AtSpeed;
            }
            else
            {
                positionIncrement = m_calc_Position_IncrementPer1Msec / 2.0;
                velocityRatio = (totalDelta - m_calc_Time_StartOfDeceleration) / (m_calc_Total_Time - m_calc_Time_StartOfDeceleration);
                m_axisState = AxisStatesEnum.Deceleration;
            }

            m_calc_Position_Relative_Current += (positionIncrement * m_currentDelta);
            m_calc_CurrentVelocity = CommandedVelocity * velocityRatio;
        }

        // --------------------------------------------------------------------------------------------------

        void UpdatePosition_PositionMove(long totalDelta, long m_currentDelta)
        {
            UpdatePosition_ZeroOrigin_PositionMove(totalDelta, m_currentDelta);

            IAxisPoint axisPoint = Target as IAxisPoint;

            if (m_calc_Position_Relative_Current >= m_calc_TotalDistance)
            {
                m_calc_Position_Relative_Current = m_calc_TotalDistance;
                m_calc_CurrentVelocity = 0;
                m_axisState = AxisStatesEnum.Idle;
                StopTImer();
            }

            ActualVelocity = m_calc_CurrentVelocity * m_calc_Direction;
            //axisPoint.Velocity = ActualVelocity;

            ActualPosition = m_calc_Position_Relative_Current * m_calc_Direction + m_calc_Position_Absolute_Start;
            axisPoint.AxisPosition = ActualPosition;

            MovePhase = m_axisState.ToString();

        }

        // --------------------------------------------------------------------------------------------------

        #endregion

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Velocity Move

        // =======================================================================================================================

        void PrepareForVelocityMove(AxisDirectionsEnum direction)
        {

            if (direction == AxisDirectionsEnum.Negative_Direction)
                m_calc_Direction = -1;
            else
                m_calc_Direction = 1;

            m_calc_Time_EndOfAcceleration = CommandedVelocity / CommandedAcceleration;

            //m_calc_Position_Relative_EndOfAcceleration = CommandedVelocity * CommandedVelocity / CommandedAcceleration / 2.0;

            m_calc_Position_IncrementPer1Msec = CommandedVelocity * m_calc_Time_EndOfAcceleration / 1000;

            m_calc_Position_Relative_Current = 0;
            m_calc_CurrentVelocity = 0;
            m_isVelocityMove = true;

            StartTimer();

        }

        // --------------------------------------------------------------------------------------------------

        void UpdatePosition_VelocityMove(long totalDelta, long m_currentDelta)
        {

            if (totalDelta < m_calc_Time_EndOfAcceleration)
            {
                m_calc_Position_Relative_Current += (m_currentDelta * m_calc_Position_IncrementPer1Msec / 2.0);
                m_calc_CurrentVelocity = CommandedVelocity * totalDelta / m_calc_Time_EndOfAcceleration;
                m_axisState = AxisStatesEnum.Acceleration;
            }
            else
            {
                m_calc_Position_Relative_Current += (m_currentDelta * m_calc_Position_IncrementPer1Msec);
                m_calc_CurrentVelocity = CommandedVelocity;
                m_axisState = AxisStatesEnum.AtSpeed;
            }

            ActualVelocity = m_calc_CurrentVelocity * m_calc_Direction;
            ActualPosition = m_calc_Position_Relative_Current * m_calc_Direction + m_calc_Position_Absolute_Start;
            MovePhase = m_axisState.ToString();

            IAxisPoint axisPoint = Target as IAxisPoint;
            axisPoint.Velocity = ActualVelocity;
            axisPoint.AxisPosition = ActualPosition;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Stopping

        // =======================================================================================================================

        void StopRequested()
        {
            //m_calc_Position_Relative_Current = 0;
            //m_calc_Position_Absolute_Start = ActualPosition;
            //m_calc_Position_Relative_EndOfAcceleration = CommandedVelocity * CommandedVelocity / CommandedAcceleration / 2.0;
            //m_calc_Position_IncrementPer1Msec = m_calc_Position_Relative_EndOfAcceleration / 500;
            //m_axisState = AxisStatesEnum.Deceleration;
            //MovePhase = m_axisState.ToString();
        }

        // --------------------------------------------------------------------------------------------------

        void UpdateStopping(long totalDelta, long m_currentDelta)
        {

            //m_calc_Position_Relative_Current += (m_currentDelta * m_calc_Position_IncrementPer1Msec / 2.0);
            //if (m_calc_Position_Relative_Current > m_calc_Position_Relative_EndOfAcceleration)
            //{
            //    m_axisState = AxisStatesEnum.Idle;
            //    MovePhase = m_axisState.ToString();
            //    m_calc_CurrentVelocity = 0;
            //    StopTImer();

            //}
            //else
            //    m_calc_CurrentVelocity = CommandedVelocity * (m_calc_Position_Relative_EndOfAcceleration - totalDelta) / m_calc_Position_Relative_EndOfAcceleration;

            //IAxisPoint axisPoint = Target as IAxisPoint;

            //ActualVelocity = m_calc_CurrentVelocity * m_calc_Direction;
            //axisPoint.Velocity = ActualVelocity;

            //ActualPosition = m_calc_Position_Relative_Current * m_calc_Direction + m_calc_Position_Absolute_Start;
            //axisPoint.AxisPosition = ActualPosition;

        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Timer

        // =======================================================================================================================

        long m_lastCountMsec = -1;
        long m_startCountMsec = -1;
        long m_startStopMsec = -1;

        // --------------------------------------------------------------------------------------------------

        protected override void TimerTick(long tickNumber)
        {
            if (m_timing)
            {
                if (m_lastCountMsec > 0)
                {
                    long currentDelta;
                    long totalDelta;
                    if (m_stopping)
                    {
                        if (m_startStopMsec < 0)
                        {
                            m_startStopMsec = tickNumber;
                            StopRequested();
                        }
                        else
                        {
                            currentDelta = tickNumber - m_lastCountMsec;
                            totalDelta = tickNumber - m_startStopMsec;
                            UpdateStopping(totalDelta, currentDelta);
                        }
                    }
                    else
                    {
                        currentDelta = tickNumber - m_lastCountMsec;
                        totalDelta = tickNumber - m_startCountMsec;
                        Console.WriteLine(tickNumber.ToString() + " : " + totalDelta);
                        if (m_isVelocityMove)
                            UpdatePosition_VelocityMove(totalDelta, currentDelta);
                        else
                            UpdatePosition_PositionMove(totalDelta, currentDelta);
                    }
                }
                else
                    m_startCountMsec = tickNumber;
                m_lastCountMsec = tickNumber;
            }
        }

        // --------------------------------------------------------------------------------------------------

        void StartTimer()
        {
            m_lastCountMsec = -1;
            m_timing = true;
        }

        // --------------------------------------------------------------------------------------------------

        void StopTImer()
        {
            m_timing = false;
            m_stopping = false;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {

        }

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================
    }
}
