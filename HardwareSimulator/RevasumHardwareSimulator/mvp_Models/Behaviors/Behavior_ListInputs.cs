﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    [Serializable]
    public abstract class Behavior_ListInputs : Behavior_Digital
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public Behavior_ListInputs()
        {
            m_inputs = new List<SelectedIDigitalPointTemplate>();
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_ListInputs(XMLNode rootNode)
            : base(rootNode)
        {
            XMLNode parentNode = rootNode.NodeAt("Inputs");
            m_inputs = new List<SelectedIDigitalPointTemplate>();
            foreach (XMLNode node in parentNode.Nodes())
            {
                m_inputs.Add(new SelectedIDigitalPointTemplate(node));
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            foreach (SelectedIDigitalPointTemplate point in Inputs)
            {
                point.RestoreInput(points);
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Properties

        // ================================================================================================

        protected List<SelectedIDigitalPointTemplate> m_inputs;
        public List<SelectedIDigitalPointTemplate> Inputs
        {
            get { return m_inputs; }
            set { m_inputs = value; }
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Add / Remove input

        // ================================================================================================

        public void AddInput(SelectedIDigitalPointTemplate anInput)
        {
            Inputs.Add(anInput);
        }

        // --------------------------------------------------------------------------------------------------

        public void RemoveInput(SelectedIDigitalPointTemplate template)
        {
            Inputs.Remove(template);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Online

        // ================================================================================================

        public override void OnLine()
        {
            base.OnLine();

            try
            {
                foreach (SelectedIDigitalPointTemplate template in m_inputs)
                {
                    IOPoint point = template.Input as IOPoint;
                    point.AddDependent(this);
                }
            PerformOnLineUpdate();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void OffLine()
        {
            base.OffLine();
        }

        // ================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            foreach (SelectedIDigitalPointTemplate template in m_inputs)
            {
                SaveDigitalInputTemplate(writer, tab, template);
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
