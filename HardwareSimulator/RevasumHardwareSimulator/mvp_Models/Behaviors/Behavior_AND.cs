﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    [Serializable]
    public class Behavior_AND : Behavior_ListInputs
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public Behavior_AND()
        {
            m_imageSource = "andSymbol.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_AND(XMLNode node)
            : base(node)
        {
            m_imageSource = "andSymbol.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("To turn the output on:\n");
                sb.Append("\t1.  Wait for ALL the inputs to go on\n");
                sb.Append("\t2. Wait for an optional delay (ON Delay)\n\n");

                sb.Append("To turn the output off:\n");
                sb.Append("\t1. Wait for ANY input to go off\n");
                sb.Append("\t2. Wait for an optional delay (OFF Delay)\n\n");

                sb.Append("Valid Inputs:\n");
                sb.Append("\t Must be Digital");
                return sb.ToString();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "AND"; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Operation

        // ================================================================================================

        public override void PerformOnLineUpdate()
        {
            bool digitalValue = true;
            foreach (SelectedIDigitalPointTemplate digital in Inputs)
            {
                digital.ValueChanged();

                if (digital.IsClear)
                {
                    digitalValue = false;
                    break;
                }
            }
            SetTarget(digitalValue);
        }

        // ================================================================================================

        #endregion




    }
}
