﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    [Serializable]
    public class Behavior_LINEAR : Behavior
    {

        // =======================================================================================================================

        #region Initialize

        // =======================================================================================================================

        public Behavior_LINEAR()
        {
            m_imageSource = "linear.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_LINEAR(XMLNode node): base(node)
        {
            m_imageSource = "linear.jpg";
            Slope = DoubleValueAtAttribute(node, "Slope", 0);
            Maximum = DoubleValueAtAttribute(node, "Maximum", 0);
            Minimum = DoubleValueAtAttribute(node, "Minimum", 0);
            HoldLastValue = BoolValueAtAttribute(node, "HoldLastValue");
            ReverseActing = BoolValueAtAttribute(node, "ReverseActing");

            InputName = node.NodeAt("Inputs").NodeAt("Input").AttributeAt("Name");
        }

        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            if (InputName != "")
            {
                IOPoint point = FindPointByName(InputName, points);
                Input = point as IDigitalPoint;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Properties

        // =======================================================================================================================

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Implement function y = mx + b, where\n");
                sb.Append("   X is elapsed time\n");
                sb.Append("   Slope m is configurable\n");
                sb.Append("   Min and Max are configurable offsets, dependent on sign of m\n");
                sb.Append("   Reverse Acting changes required state of input\n");
                sb.Append("   Hold Last Value determines what happens to Output\n");
                sb.Append("        when input goes off\n");
                sb.Append("Input On\n");
                sb.Append("   Output travels between Min and Max\n");
                sb.Append("Input Off\n");
                sb.Append("   Output returns to Min\n");
                sb.Append("Valid Input:\n");
                sb.Append("   Must be Digital");
                return sb.ToString();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "LINEAR"; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsAnalog
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_actualValue;
        public double ActualValue
        {
            get { return m_actualValue; }
            set
            {
                m_actualValue = value;
                ((IAnalogPoint)Target).AnalogValue = value;
                OnPropertyChanged("ActualValue");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Input

        // =======================================================================================================================

        protected string m_inputName;
        public string InputName
        {
            get { return m_inputName; }
            set { m_inputName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        protected IDigitalPoint m_input;
        public IDigitalPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                if (m_input != null)
                    InputName = value.QualifiedName;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Specific Properties

        // =======================================================================================================================

        private double m_slope;
        public double Slope
        {
            get { return m_slope; }
            set
            {
                m_slope = value;
                OnPropertyChanged("Slope");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private double m_maximum = 32767;
        public double Maximum
        {
            get { return m_maximum; }
            set
            {
                m_maximum = value;
                OnPropertyChanged("Maximum");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private double m_minimum = 0;
        public double Minimum
        {
            get { return m_minimum; }
            set
            {
                m_minimum = value;
                OnPropertyChanged("Minimum");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private bool m_holdLastValue = false;
        public bool HoldLastValue
        {
            get { return m_holdLastValue; }
            set
            {
                m_holdLastValue = value;
                OnPropertyChanged("HoldLastValue");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private bool m_reverseActing = false;
        public bool ReverseActing
        {
            get { return m_reverseActing; }
            set
            {
                m_reverseActing = value;
                OnPropertyChanged("ReverseActing");
            }
        }

        // --------------------------------------------------------------------------------------------------

        string m_ConfigurationDescription;
        public string ConfigurationDescription
        {
            get { return m_ConfigurationDescription; }

            set
            {
                m_ConfigurationDescription = value;
                OnPropertyChanged("ConfigurationDescription");
            }
        }
        
        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Online

        // =======================================================================================================================

        public override void OnLine()
        {
            base.OnLine();
            IOPoint point = m_input as IOPoint;
            point.AddDependent(this);
            SetDescription();
            PerformOnLineUpdate();
        }

        // --------------------------------------------------------------------------------------------------

         public override void OffLine()
        {
           ConfigurationDescription = "";
        }

       // --------------------------------------------------------------------------------------------------

        void SetDescription()
        {
            double min = Slope < 0 ? Maximum : Minimum;
            double max = Slope < 0 ? Minimum : Maximum;
            string outputNormalActing = ReverseActing ? "OFF" : "ON";
            string outputReverseActing = ReverseActing ? "ON" : "OFF";
            string holdValue = HoldLastValue ? "hold last value" : "go to " + min.ToString();

            String result = String.Format("The Slope is {0}.\nOutput = {1}: -> Will travel between {2} and {3}.\n Output = {4}: -> will {5}",
                Slope, outputNormalActing, min, max, outputReverseActing, holdValue);

            ConfigurationDescription = result;

        }

        // --------------------------------------------------------------------------------------------------

        public override void PerformOnLineUpdate()
        {
            bool goOff = (Input.IsClear() ^ ReverseActing);
            if (goOff)
            {
                m_timing = false;
                if (HoldLastValue)
                    ActualValue = ((IAnalogPoint)Target).AnalogValue;
                else
                    if (Slope >= 0)
                        ActualValue = Minimum;
                    else
                        ActualValue = Maximum;
            }
            else
            {
                m_lastCountMsec = -1;
                m_timing = true;
            }
        }

        // --------------------------------------------------------------------------------------------------

        private void UpdateState(long expiredMilliseconds)
        {
            double output;
            if (Slope >= 0)
            {
                output = expiredMilliseconds * Slope + Minimum;
                if (output > Maximum)
                {
                    output = Maximum;
                    m_timing = false;
                }
            }
            else
                output = expiredMilliseconds * Slope + Maximum;
            {
                if (output < Minimum)
                {
                    output = Minimum;
                    m_timing = false;
                }
            }
            ActualValue = output;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Timer

        // =======================================================================================================================

        long m_lastCountMsec = -1;

        // --------------------------------------------------------------------------------------------------

        protected override void TimerTick(long tickNumber)
        {
            if (m_timing)
            {
                if (m_lastCountMsec < 0)
                    m_lastCountMsec = RestoreLastTick(tickNumber);
                else
                    UpdateState(tickNumber - m_lastCountMsec);
            }
        }

        // --------------------------------------------------------------------------------------------------

        long RestoreLastTick(long currentTick)
        {
            if (Slope == 0 || !HoldLastValue)
                return currentTick;
            else
            {
                double outp = ((IAnalogPoint)Target).AnalogValue;
                double diff = Slope > 0 ? outp - Minimum : outp - Maximum;
                double expired = diff / Slope;
                return currentTick - (long) expired;
            }

        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {
            SaveAttribute(writer, tab, "Slope", Slope.ToString());
            SaveAttribute(writer, tab, "Minimum", Minimum.ToString());
            SaveAttribute(writer, tab, "Maximum", Maximum.ToString());
            string stringValue = HoldLastValue ? "true" : "false";
            SaveAttribute(writer, tab, "HoldLastValue", stringValue);
            stringValue = ReverseActing ? "true" : "false";
            SaveAttribute(writer, tab, "ReverseActing", stringValue);
        }

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            SaveInput(writer, tab, Input);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
