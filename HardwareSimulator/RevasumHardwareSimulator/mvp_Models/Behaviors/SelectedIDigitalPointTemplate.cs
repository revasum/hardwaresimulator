﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    [Serializable]
    public class SelectedIDigitalPointTemplate : RevasumBaseObject
    {

        public SelectedIDigitalPointTemplate()
        {

        }

        // --------------------------------------------------------------------------------------------------

        public SelectedIDigitalPointTemplate(XMLNode node)
        {
            QualifiedName = node.AttributeAt("Name");
            IsReverseActing = BoolValueAtAttribute(node, "ReverseActing");
            IOPoint point = IOConfiguration.m_IOPoints2.AllPoints.Find(x => x.QualifiedName.Contains(QualifiedName));
            Input = point as IDigitalPoint;

        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        IDigitalPoint m_input;

        public IDigitalPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                Name = m_input.Name;
                QualifiedName = m_input.QualifiedName;
            }
        }

        // --------------------------------------------------------------------------------------------------

        string m_name;
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }

        // --------------------------------------------------------------------------------------------------

        string m_qualifiedName;
        public string QualifiedName
        {
            get { return m_qualifiedName; }
            set { m_qualifiedName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        bool m_reverseActing;
        public bool IsReverseActing
        {
            get { return m_reverseActing; }
            set
            {
                m_reverseActing = value;
                OnPropertyChanged("IsReverseActing");
                if (m_reverseActing)
                    ReverseActingVisible = "Visible";
                else
                    ReverseActingVisible = "Collapsed";
            }
        }

        // --------------------------------------------------------------------------------------------------

        public void RestoreInput(List<IOPoint> points)
        {
            foreach (IOPoint aPoint in points)
                if (aPoint.QualifiedName == QualifiedName)
                {
                    IDigitalPoint digital = aPoint as IDigitalPoint;
                    m_input = digital;
                    return;
                }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        bool m_isSelected;
        public bool IsSelected
        {
            get { return m_isSelected; }

            set
            {
                m_isSelected = value;
                OnPropertyChanged("IsSelected");
                if (m_isSelected)
                    SelectedInputBackground = "CornflowerBlue";
                else
                    SelectedInputBackground = "White";
            }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        string m_SelectedInputBackground;
        public string SelectedInputBackground
        {
            get { return m_SelectedInputBackground; }
            set
            {
                m_SelectedInputBackground = value;
                OnPropertyChanged("SelectedInputBackground");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string ToString()
        {
            return "Template on " + Name;
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsSet
        {
            get { return m_input.IsSet() ^ m_reverseActing; }
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsClear
        {
            get { return m_input.IsClear() ^ m_reverseActing; }
        }

        // --------------------------------------------------------------------------------------------------

        public string ImageSource
        {
            get { return m_input.ImageSource; }
        }

        // --------------------------------------------------------------------------------------------------

        public string ValueAsSymbol
        {
            get { return m_input.ValueAsSymbol; }
        }

        // --------------------------------------------------------------------------------------------------

        string m_ReverseActingVisible;
        public string ReverseActingVisible
        {
            get { return m_ReverseActingVisible; }
            set
            {
                m_ReverseActingVisible = value;
                OnPropertyChanged("ReverseActingVisible");
            }
        }

        // --------------------------------------------------------------------------------------------------

        public void ValueChanged()
        {
            OnPropertyChanged("ValueAsSymbol");
        }

    }
}
