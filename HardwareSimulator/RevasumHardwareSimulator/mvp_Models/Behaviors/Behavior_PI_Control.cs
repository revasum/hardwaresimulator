﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.Global;
using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    [Serializable]
    public class Behavior_PI_Control : Behavior
    {
        // =======================================================================================================================

        #region Initialize

        // =======================================================================================================================
        
        public Behavior_PI_Control()
        {
            m_imageSource = "pid.jpg";
        }

        public Behavior_PI_Control(XMLNode rootNode) : base(rootNode)
        {
            m_imageSource = "pid.jpg";

            try
            {
                Kp = Double.Parse(rootNode.AttributeAt("Kp"));
                Ki = Double.Parse(rootNode.AttributeAt("Ki"));
                OnOffThreshold = Double.Parse(rootNode.AttributeAt("OnOffThreshold"));

                XMLNode inputNode = rootNode.NodeAt("Inputs");
                InputName = (inputNode.Nodes().ToArray())[0].AttributeAt("Name");
            }
            catch(Exception)
            {
                Console.WriteLine("Failed to deserialize XML node for Behavior_PI_Control " + QualifiedName);
            }
        }
        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            if (InputName != "")
            {
                IOPoint point = FindPointByName(InputName, points);
                Input = point as IAnalogPoint;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Properties

        // =======================================================================================================================

        public override string Description
        {
            get
            {
                return
                    "Map analog output to analog input with simple PI response.\n" +
                    "\tKp = Proportional constant\n" +
                    "\tKi = Integral constant\n" +
                    "\t Set Kp = 1 and Ki = 0 for instant response" +
                    "Descrete implementation pseudocode:\n" +
                    "\terror = setpoint - measured_value\n" +
                    "\tintegral = integral + error * dt\n" +
                    "\toutput = Kp * error + Ki * integral\n" +
                    "\twait(dt)";
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "PI_Control"; }
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsAnalog
        {
            get { return true; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        double m_actualValue;
        public double ActualValue
        {
            get { return m_actualValue; }
            set
            {
                m_actualValue = value;
                ((IAnalogPoint)Target).AnalogValue = value;
                OnPropertyChanged("ActualValue");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Input

        // =======================================================================================================================

        protected string m_inputName;
        public string InputName
        {
            get { return m_inputName; }
            set { m_inputName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        protected IAnalogPoint m_input;
        public IAnalogPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                if (m_input != null)
                    InputName = value.QualifiedName;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Specific Properties

        // =======================================================================================================================

        private double m_Kp;
        public double Kp
        {
            get { return m_Kp; }
            set
            {
                m_Kp = value;
                OnPropertyChanged("Kp");
            }
        }

        private double m_Ki;
        public double Ki
        {
            get { return m_Ki; }
            set
            {
                m_Ki = value;
                OnPropertyChanged("Ki");
            }
        }

        private double m_OnOffThreshold;
        public double OnOffThreshold
        {
            get { return m_OnOffThreshold; }
            set
            {
                m_OnOffThreshold = value;
                OnPropertyChanged("OnOffThreshold");
            }
        }

        // --------------------------------------------------------------------------------------------------

        string m_ConfigurationDescription;
        public string ConfigurationDescription
        {
            get { return m_ConfigurationDescription; }

            set
            {
                m_ConfigurationDescription = value;
                OnPropertyChanged("ConfigurationDescription");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Online

        // =======================================================================================================================

        public override void OnLine()
        {
            base.OnLine();
            IOPoint point = m_input as IOPoint;
            point.AddDependent(this);
            SetDescription();
            PerformOnLineUpdate();
        }

        // --------------------------------------------------------------------------------------------------

        public override void OffLine()
        {
            ConfigurationDescription = "";
        }

        // --------------------------------------------------------------------------------------------------

        void SetDescription()
        {
            String result = $"PI Controller.\nKp = {Kp}.\nKi = {Ki}.\nPeriod = {GlobalTimer.Instance.TimerPeriod}";
            
            ConfigurationDescription = result;
        }

        // --------------------------------------------------------------------------------------------------


        public override void PerformOnLineUpdate()
        {
            if (Input.AnalogValue < OnOffThreshold)
            {
                ResetPI();
                m_timing = false;
                ActualValue = 0;
            }
            else
            {
                m_timing = true;
            }
        }

        // --------------------------------------------------------------------------------------------------
        
        private double m_integral = 0;
        private void UpdatePI()
        {
            /*
                previous_error = 0
                integral = 0
                loop:
                  error = setpoint - measured_value
                  integral = integral + error * dt
                  output += Kp * error + Ki * integral
                  wait(dt)
                  goto loop 
            */
            double error = Input.AnalogValue - ((IAnalogPoint)Target).AnalogValue;
            m_integral += error * (GlobalTimer.Instance.TimerPeriod / 1000);
            if (error == 0)
                return;
            ActualValue = ActualValue + (Kp * error) + (Ki * m_integral);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Timer

        // =======================================================================================================================
        
            
        protected override void TimerTick(long tickNumber)
        {
            if (m_timing)
                UpdatePI();
            else
                ResetPI();
        }

        // --------------------------------------------------------------------------------------------------

        void ResetPI()
        {
            m_integral = 0;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {
            SaveAttribute(writer, tab, "Kp", Kp.ToString());
            SaveAttribute(writer, tab, "Ki", Ki.ToString());
            SaveAttribute(writer, tab, "OnOffThreshold", OnOffThreshold.ToString());
        }

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            SaveInput(writer, tab, Input);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================


    }
}
