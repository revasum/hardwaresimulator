﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    //Visit https://github.com/dotnet/roslyn/wiki/Scripting-API-Samples for examples of CodeAnalysis.CSharp.Scripting use


    //A 'global' class that will be used an an input source for the script.
    [Serializable]
    public class ScriptInputs
    {
        public List<Object> Inputs = new List<object>();
    }

    [Serializable]
    public class Behavior_Script : Behavior
    { 
        #region Script Descriptions
        public Behavior_Script()
        {
            m_imageSource = "csharp.png";
        }
        public Behavior_Script(XMLNode rootNode) : base(rootNode)
        {
            m_imageSource = "csharp.png";
            XMLNode inputNode = rootNode.NodeAt("Inputs");
            foreach(XMLNode node in inputNode.Nodes())
            {
                InputNames.Add(node.AttributeAt("Name"));
            }
            Script = FormatXMLToString(rootNode.AttributeAt("Script"));
        }

        public override string Description =>
            "Create a custom behavior based off a c# script.\n" +
            "Any inputs added will be availible to the script\n" +
            "  via the Inputs[] array.\n" +
            "Inputs accessed in the array will need to be cast\n" +
            "  to the correct type. (IDigitalPoint, IAnalogPoint).\n" +
            "For IDigitalPoint, access .BooleanValue (bool).\n" +
            "For IAnalogPoint, access .AnalogValue (double).\n" +
            "Use a return statement to pass the value back. ";

        public override string Function => "Script";
        #endregion


        #region Inputs
        //Serialized list of input names used by this behavior. 
        //1 to 1 correspondance with IOPoint list
        public List<String> InputNames { get; set; } = new List<string>();


        [NonSerialized]
        private List<IOPoint> ioPoints = new List<IOPoint>();
        /// <summary>
        /// List of input IOPoints selected for behavior
        /// </summary>
        public List<IOPoint> IOPoints
        {
            get
            {
                if (ioPoints == null)
                {
                    ioPoints = new List<IOPoint>();
                }

                return ioPoints;
            }
            set 
            { 
                ioPoints = value; 
            }
        }
        
        /// <summary>
        /// Restore IOPoints from InputNames
        /// </summary>
        /// <param name="points"></param>
        public override void RestoreInputs(List<IOPoint> points)
        {
            IOPoints?.Clear();

            foreach (var inputName in InputNames)
            {
                IOPoint point = FindPointByName(inputName, points);

                if (point != null)
                {
                    IOPoints.Add(point);
                }
            }
        }

        #endregion
    

        #region Online Function overrides
        public override void OnLine()
        {
            base.OnLine();

            foreach(var io in IOPoints)
            {
                io.AddDependent(this);
            }

            PerformOnLineUpdate();
        }
        
        /// <summary>
        ///     Try to run the script behavior
        /// </summary>
        public override void PerformOnLineUpdate()
        {
            //the script gets compiled on a seperate worker thread on run if it hasn't been compiled already.
            if (isCompiling)
            {
                //return if the script is still compiling
                return;
            }
            else if (scriptObject == null)
            {
                //compile the script if it isn't compiled and we are not currently compiling
                CompileScript(ScriptCompileCallback);
                return;
            }
            try
            {
                //Find the correct thread for the callback method to run on
                TaskScheduler syncContextScheduler;
                if (SynchronizationContext.Current != null)
                {
                    syncContextScheduler = TaskScheduler.FromCurrentSynchronizationContext();
                }
                else
                {
                    // If there is no SyncContext for this thread (e.g. we are in a unit test
                    // or console scenario instead of running in an app), then just use the
                    // default scheduler because there is no UI thread to sync with.
                    syncContextScheduler = TaskScheduler.Current;
                }

                //if (this.Name.Equals("Script.Bridge.diLeftGrindSpindleWaterAtFlow"))
                //{
                //    Console.WriteLine();
                //}

                //Run the precompiled script. Pass scriptInputs in as the 'globals'
                var task = scriptObject.RunAsync(globals: scriptInputs);

                //Setup Callback method to be called when script finishes running, since this is not an async program. 
                task.ContinueWith(ScriptRunCallback, syncContextScheduler);
            }
            catch(Exception e)
            {
                //Error message throws an event the GUI should be responding to.
                //The exception should be visible to the user
                ErrorMessage = e.Message;
            }
        }
        #endregion

        #region Scripting Support

        //User entered script. Is serialized
        private string script;
        public string Script
        {
            get { return script; }
            set
            {
                script = value;
                OnPropertyChanged("Script");
                scriptObject = null;
            }
        }
        
        //compiled script object
        [NonSerialized]
        private Script scriptObject;

        //input variables for the script
        [NonSerialized]
        private ScriptInputs scriptInputs;

        //keep track of compiling state
        [NonSerialized]
        private bool isCompiling = false;

        /// <summary>
        /// Compile the script on a seperate worker thread.
        /// Supply a callback method to run on finish.
        /// Sets isCompiling to true. Callback must set isCompiling to false.
        /// </summary>
        /// <param name="callbackMethod"></param>
        public void CompileScript(Action<Task> callbackMethod)
        {
            isCompiling = true;

            // add a task scheduler context to make it a parallelizable task
            TaskScheduler syncContextScheduler;

            if (SynchronizationContext.Current != null)
            {
                syncContextScheduler = TaskScheduler.FromCurrentSynchronizationContext();
            }
            else
            {
                // If there is no SyncContext for this thread (e.g. we are in a unit test
                // or console scenario instead of running in an app), then just use the
                // default scheduler because there is no UI thread to sync with.
                syncContextScheduler = TaskScheduler.Current;
            }


            var compileTask = Task.Factory.StartNew(() =>
            {
                //Create local copy of default script options.
                ScriptOptions options = ScriptOptions.Default;

                /* //Overkill -add all referenced assemblies of this project to script
                var asms = AppDomain.CurrentDomain.GetAssemblies();
                foreach (var asm in asms)
                {
                    options = options.AddReferences(asm);
                }*/

                //Just add RevasumHardwareSimulator assembly to script
                options = options.AddReferences(System.Reflection.Assembly.GetAssembly(typeof(Behavior_Script)));

                //Add some default 'using' imports to scripts.
                //Add RHS.Global and RHS.mvp_Models.IOPoints to every script so user can access IOPoint classes/interfaces
                //Note that AddImports returns a new copy of the object; it does not modify the current obejct
                options = options.AddImports("System");
                options = options.AddImports("RevasumHardwareSimulator.Global");
                options = options.AddImports("RevasumHardwareSimulator.mvp_Models.IOPoints");

                //Create and compile
                scriptObject = CSharpScript.Create(Script,
                    globalsType: typeof(ScriptInputs),
                    options: options);
                scriptObject.Compile();

                //create the input array that will be passed to the script on script.RunAsync()
                scriptInputs = new ScriptInputs();
                scriptInputs.Inputs.AddRange(IOPoints);
            }).ContinueWith(callbackMethod, syncContextScheduler);
        }

        /// <summary>
        ///     script compile callback for main use
        /// </summary>
        
        private void ScriptCompileCallback(Task obj)
        {
            isCompiling = false; 

            if (obj.Exception != null)
            {
                Dialogs.Dialog_ErrorMessage.Show($"Exception occured when compiling {this.Name}.\n{obj.Exception.Message}");
            }
            else
            {
                // NOTE: this was a bug that script behavior would never work the first time the RUN button was pressed
                // since the script was not read (needed to be compiled), recall the Online Update now that it is ready
                this.PerformOnLineUpdate();
            }            
        }

        //callback from script run
        private void ScriptRunCallback(Task<ScriptState> obj)
        {
            var result = obj.Result.ReturnValue;

            if (result == null)
            {
                return;
            }

            Result = result.ToString();

            //cast target io. set value
            if(Target is IOPoint_Analog analogPoint)
            {
                analogPoint.AnalogValue = Convert.ToDouble(result); 
            }
            else if(Target is IOPoint_Digital digitalPoint)
            {
                digitalPoint.BooleanValue = Convert.ToBoolean(result);            
            }
            else if(Target is IOPoint_AxisPosition position)
            {
                position.AnalogValue = Convert.ToDouble(result);
            }
        }
        #endregion

        #region Script Testing
        //async/await sure would make this mess cleaner and easier

        //the serializer attemped to serialze all object attached to event. watch out!
        [field: NonSerialized]
        public event EventHandler<string> ErrorMessageEvent;


        [NonSerialized]
        private string errorMessage;
        /// <summary>
        /// Error message from exceptions on compile/run of script. 
        /// Throws event on ErrorMessageEvent on change.
        /// </summary>
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                if (errorMessage == value) return;
                errorMessage = value;
                ErrorMessageEvent?.Invoke(this, value);
            }
        }

        /// <summary>
        /// To be used for compiling/running script offline with default values for inputs.
        /// Throws exceptions messages thru ErrorMessages
        /// </summary>
        public void TestScriptRun()
        {
            if (scriptObject == null)
                CompileScript(ScriptTestRunCompileCallback);
            else
                TestScriptRunContinuation();
        }
        private void ScriptTestRunCompileCallback(Task obj)
        {
            if (obj.Exception != null)
                ErrorMessage = $"Exception occurred when compiling {this.Name}.\n{obj.Exception.Message}";
            else
            {
                isCompiling = false;
                TestScriptRunContinuation();
            }
        }
        private void TestScriptRunContinuation()
        {
            ScriptInputs testInputs = new ScriptInputs
            {
                Inputs = new List<object>(IOPoints)
            };

            try
            {
                //Run the pre-compiled script. Pass testInputs in as the 'globals'
                var task = scriptObject.RunAsync(globals: testInputs);
                //Setup Callback method to be called when script finishes running, since this is not an async program. 
                task.ContinueWith(TestScriptRunCallback, TaskScheduler.FromCurrentSynchronizationContext());
            }
            catch(Exception e)
            {
                ErrorMessage = e.Message;
            }
        }

        private void TestScriptRunCallback(Task<ScriptState> obj)
        {
            if(obj.Exception != null)
                ErrorMessage = $"Exception occurred when running {this.Name}.\n{obj.Exception.InnerException.Message}";
        }
        #endregion


        #region Props/Methods for GUI
        /// <summary>
        /// Add IO Point to IOPoints and InputNames
        /// </summary>
        /// <param name="aPoint"></param>
        public void AddPoint(IOPoint aPoint)
        {
            IOPoints.Add(aPoint);
            InputNames.Add(aPoint.QualifiedName);
        }

        /// <summary>
        /// Remove IO Point to IOPoints and InputNames
        /// </summary>
        /// <param name="aPoint"></param>
        public void RemovePoint(IOPoint aPoint)
        {
            IOPoints.Remove(aPoint);
            InputNames.Remove(aPoint.QualifiedName);
        }

        [NonSerialized]
        private string result;
        /// <summary>
        /// Result of last script execution
        /// </summary>
        public string Result
        {
            get { return result; }
            set
            {
                if (result == value) return;
                result = value;
                OnPropertyChanged("Result");
            }
        }
        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {
            string xmlCompatibleScript = FormatStringToXML(Script);

            SaveAttribute(writer, tab, "Script", xmlCompatibleScript);
        }

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            foreach (var inputName in InputNames)
            {
                SaveInput(writer, tab, inputName);
            }
        }


        private static readonly Dictionary<string, string> stringToXMLReplacements = new Dictionary<string, string>
        {
            {"&", "&amp;"},
            {"<", "&lt;"},
            {">", "&gt;"},
            {"\"", "&quot;"},
            {"\'", "&apos;"}
        };

        private string FormatStringToXML(string orignalString)
        {
            string xmlCompatibleString = orignalString;
            foreach (var replacement in stringToXMLReplacements)
            {
                xmlCompatibleString = xmlCompatibleString.Replace(replacement.Key, replacement.Value);
            }
            return xmlCompatibleString;
        }

        private string FormatXMLToString(string xmlString)
        {
            string newString = xmlString;
            foreach (var replacement in stringToXMLReplacements.Reverse())
            {
                newString = newString.Replace(replacement.Value, replacement.Key);
            }
            return newString;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
