﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using System.Threading;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{
    [Serializable]
    public class Behavior_NOT : Behavior_Digital
    {

        // ================================================================================================

        #region Initialization

        // ================================================================================================

        public Behavior_NOT()
        {
            m_imageSource = "yin-yang3.png";
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_NOT(XMLNode node)
            : base(node)
        {
            m_imageSource = "yin-yang3.png";
        }

        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            if (InputName != "")
            {
                IOPoint point = FindPointByName(InputName, points);
                Input = point as IDigitalPoint;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Properties

        // ================================================================================================

        protected string m_inputName;
        public string InputName
        {
            get { return m_inputName; }
            set { m_inputName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        protected IDigitalPoint m_input;
        public IDigitalPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                if (m_input != null)
                    InputName = value.QualifiedName;
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();

                sb.Append("To turn the output on:\n");
                sb.Append("\t1. Wait for the sole input to go off\n");
                sb.Append("\t2. Wait for an optional delay (ON Delay)\n\n");

                sb.Append("To turn the output off:\n");
                sb.Append("\t1. Wait for the input to go on\n");
                sb.Append("\t2. Wait for an optional delay (OFF Delay)\n\n");

                sb.Append("Valid Input:\n");
                sb.Append("\t Must be Digital");

                return sb.ToString();

            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "NOT"; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Operation

        // ================================================================================================

        public override void OnLine()
        {
            base.OnLine();
            IOPoint point = m_input as IOPoint;
            point.AddDependent(this);
            PerformOnLineUpdate();
        }

        // --------------------------------------------------------------------------------------------------

        public override void PerformOnLineUpdate()
        {
            SetTarget(Input.IsClear());
        }

        // ================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        //public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        //{

        //}

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            SaveInput(writer, tab, Input);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================
  
    }
}
