﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    [Serializable]
    public abstract class Behavior : RevasumSimulatedPoint, ILocalNotifier
    {

        // =======================================================================================================================

        #region Initialization

        // =======================================================================================================================

        public Behavior()
        {
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior(XMLNode node)
        {
            TargetName = node.AttributeAt("Target");
        }

        // --------------------------------------------------------------------------------------------------

        public void Restore(List<IOPoint> allPoints, IOPoint target)
        {
            Target = target;
            RestoreInputs(allPoints);
        }

        // --------------------------------------------------------------------------------------------------

        public abstract void RestoreInputs(List<IOPoint> points);


        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Properties

        // =======================================================================================================================

        [NonSerialized]
        IOPoint m_target;
        public IOPoint Target
        {
            get { return m_target; }
            set
            {
                m_target = value;
                TargetName = m_target.QualifiedName;
                Name = Function + "." + TargetName;
            }
        }

        // --------------------------------------------------------------------------------------------------

        string m_targetName;
        public string TargetName
        {
            get { return m_targetName; }
            set { m_targetName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        public abstract string Description { get; }

        // --------------------------------------------------------------------------------------------------

        public abstract string Function { get; }

        // --------------------------------------------------------------------------------------------------

        protected string m_imageSource = "";
        public string ImageSource
        {
            get 
            {
                return FileManager.GetPathForBitmap(m_imageSource);
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Searching for Inputs

        // =======================================================================================================================

        protected IOPoint FindPointByName(string aName, List<IOPoint> aList)
        {
            foreach (IOPoint aPoint in aList)
                if (aPoint.QualifiedName == aName)
                    return aPoint;
            return null;
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Online

        // =======================================================================================================================

        public abstract void PerformOnLineUpdate();

        [NonSerialized]
        protected bool m_valueInSync = false;

        // --------------------------------------------------------------------------------------------------

        public override void LocalIOChanged()
        {
            PerformOnLineUpdate();
        }

        // --------------------------------------------------------------------------------------------------

        public override void OnLine()
        {
            GlobalTimer.Instance.Register(TimerTick);

            CurrentOperationResult = "";
        }

        // --------------------------------------------------------------------------------------------------

        public override void OffLine()
        {
            m_valueInSync = false;
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        string m_currentOperationResult = "";
        public string CurrentOperationResult
        {
            get { return m_currentOperationResult; }
            set
            {
                m_currentOperationResult = value;
                OnPropertyChanged("CurrentOperationResult");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Timer Interface

        // =======================================================================================================================

        long m_endTime = -1;
        protected bool m_timing = false;
        long m_duration;

        // --------------------------------------------------------------------------------------------------

        protected virtual void TimerTick(long tickNumber)
        {
            if (m_timing)
            {
                if (m_endTime < 0)
                {
                    m_endTime = tickNumber + m_duration;
                }
                else if (tickNumber > m_endTime)
                {
                    m_timing = false;
                    TimeExpired();
                }
            }
        }

        // --------------------------------------------------------------------------------------------------

        protected virtual void StartTiming(long durationMilliseconds)
        {
            if (!m_timing)
            {
                m_endTime = -1;
                m_duration = durationMilliseconds;
                m_timing = true;
            }
        }

        // --------------------------------------------------------------------------------------------------

        protected virtual void TimeExpired()
        {
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Prinitng

        // =======================================================================================================================

        public override string ToString()
        {
            return String.Format("({0}) {1}", Function, QualifiedName);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public abstract void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab);

        // --------------------------------------------------------------------------------------------------

        public abstract void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab);

        // --------------------------------------------------------------------------------------

        protected void Tab(System.IO.StreamWriter writer, int tabs)
        {
            for (int i = 0; i < tabs; i++)
                writer.Write("  ");
        }

        // --------------------------------------------------------------------------------------

        protected void SaveAttribute(System.IO.StreamWriter writer, int tabs, string atrName, string atrValue, string finish = "")
        {
            Tab(writer, tabs);
            writer.WriteLine(atrName + "=\"" + atrValue + "\"" + finish);
        }

        // --------------------------------------------------------------------------------------

        protected void SaveTagAndAttribute(System.IO.StreamWriter writer, int tabs, string tagName, string atrName, string atrValue, string finish = "")
        {
            Tab(writer, tabs);
            writer.WriteLine(tagName + " " + atrName + "=\"" + atrValue + "\"" + finish);
        }

        // --------------------------------------------------------------------------------------------------

        protected void SaveDigitalInputTemplate(System.IO.StreamWriter writer, int tabs, SelectedIDigitalPointTemplate template)
        {
            string finish = template.IsReverseActing ? "" : " />";
            SaveTagAndAttribute(writer, tabs, "<Input", "Name", template.Input.QualifiedName, finish);
            if (template.IsReverseActing)
                SaveAttribute(writer, tabs + 1, "ReverseActing", "true", " />");
        }

        // --------------------------------------------------------------------------------------------------

        protected void SaveInput(System.IO.StreamWriter writer, int tabs, IDigitalPoint input)
        {
            SaveTagAndAttribute(writer, tabs, "<Input", "Name", input.QualifiedName, " />");
        }

        // --------------------------------------------------------------------------------------------------

        protected void SaveInput(System.IO.StreamWriter writer, int tabs, IAnalogPoint input)
        {
            SaveTagAndAttribute(writer, tabs, "<Input", "Name", input.QualifiedName, " />");
        }
        
        // --------------------------------------------------------------------------------------------------

        protected void SaveInput(System.IO.StreamWriter writer, int tabs, string qualifiedName)
        {
            SaveTagAndAttribute(writer, tabs, "<Input", "Name", qualifiedName, " />");
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
