﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    /// <summary>
    /// An abstract superclass of Behaviors with Digital Target
    /// </summary>
    [Serializable]
    public abstract class Behavior_Digital : Behavior
    {

        public Behavior_Digital()
        {
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_Digital(XMLNode node)
            : base(node)
        {
            OnDelayTime = IntValueAtAttribute(node, "OnDelayTime", 0);
            OffDelayTime = IntValueAtAttribute(node, "OffDelayTime", 0);
        }

        // ================================================================================================

        #region BooleanValue

        // ================================================================================================

        [NonSerialized]
        protected bool m_booleanValue = false;

        /// <summary>
        /// Perform the update process only when value changed.  (But make sure that the value is also correctly reflected when first going online)
        /// </summary>
        public bool BooleanValue
        {
            get { return m_booleanValue; }
            set
            {
                if (m_booleanValue != value || !m_valueInSync)
                {
                    m_booleanValue = value;
                    m_valueInSync = true;
                    IDigitalPoint digitalTarget = Target as IDigitalPoint;
                    digitalTarget.BooleanValue = m_booleanValue;
                    if (m_booleanValue)
                        GoingOn();
                    else
                        GoingOff();
                }
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void OnLine()
        {
            m_valueInSync = false;
            base.OnLine();
        }

        // --------------------------------------------------------------------------------------------------

        public override bool IsDigital
        {
            get { return true; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Timing

        // ================================================================================================

        bool m_onDelayInProgress = false;

        bool m_offDelayInProgress = false;

        // --------------------------------------------------------------------------------------------------

        public int OnDelayTime { get; set; }

        public int OffDelayTime { get; set; }

        // --------------------------------------------------------------------------------------------------

        protected void SetTarget(bool newTargetValue)
        {
            //Logger.LogMessage(nameof(Behavior_Digital), nameof(SetTarget), $"Name: {this.Name}; BooleanValue: {BooleanValue}; newTargetValue: {newTargetValue}; m_valueInSync: {m_valueInSync}");

            if (BooleanValue != newTargetValue || !m_valueInSync)
            {
                if (m_valueInSync)
                {
                    if (newTargetValue && (OnDelayTime > 0))
                    {
                        //Logger.LogMessage(nameof(Behavior_Digital), nameof(SetTarget), $"m_offDelayInProgress = true; DelayingOn");

                        m_onDelayInProgress = true;
                        DelayingOn(OnDelayTime);
                        StartTiming(OnDelayTime);
                        return;
                    }
                    if (!newTargetValue && (OffDelayTime > 0))
                    {
                        //Logger.LogMessage(nameof(Behavior_Digital), nameof(SetTarget), $"m_offDelayInProgress = true; DelayingOff");

                        m_offDelayInProgress = true;
                        DelayingOff(OffDelayTime);
                        StartTiming(OffDelayTime);
                        return;
                    }
                }

                BooleanValue = newTargetValue;
            }
        }

        // --------------------------------------------------------------------------------------------------

        protected override void TimeExpired()
        {
            if (m_onDelayInProgress)
            {
                m_onDelayInProgress = false;
                BooleanValue = true;
            }
            else if (m_offDelayInProgress)
            {
                m_offDelayInProgress = false;
                BooleanValue = false;
            }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region IDigitalPoint compliance - value

        // ================================================================================================

        public void Set()
        {
            SetTarget(true);
        }

        // --------------------------------------------------------------------------------------------------

        public void Clear()
        {
            SetTarget(false);
        }

        // --------------------------------------------------------------------------------------------------

        public void Toggle()
        {
            SetTarget(!BooleanValue);
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsSet()
        {
            return BooleanValue;
        }

        // --------------------------------------------------------------------------------------------------

        public bool IsClear()
        {
            return !BooleanValue;
        }

        // --------------------------------------------------------------------------------------------------

        public string ValueAsSymbol
        {
            get { return ImageSource; }
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region IDigitalPoint compliance - Default Values

        // ================================================================================================

        public string InitialValueSymbol
        {
            get { return FileManager.GetPathForBitmap("NA.jpg"); }
        }

        // --------------------------------------------------------------------------------------------------

        public void ToggleInitialValue()
        {
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Status

        // ================================================================================================

        protected void GoingOn()
        {
            CurrentOperationResult = "ON";
        }

        // --------------------------------------------------------------------------------------------------

        protected void GoingOff()
        {
            CurrentOperationResult = "OFF";
        }

        // --------------------------------------------------------------------------------------------------

        protected void DelayingOn(int duration)
        {
            CurrentOperationResult = String.Format("On Delay for {0} msec", duration);
        }

        // --------------------------------------------------------------------------------------------------

        protected void DelayingOff(int duration)
        {
            CurrentOperationResult = String.Format("Off Delay for {0} msec", duration);
        }

        // ================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {
            if (OnDelayTime > 0)
                SaveAttribute(writer, tab, "OnDelayTime", OnDelayTime.ToString());
            if (OffDelayTime > 0)
                SaveAttribute(writer, tab, "OffDelayTime", OffDelayTime.ToString());
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
