﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.Notifiers;
using RevasumHardwareSimulator.mvp_Models.IOPoints;

using XMLSupport;

namespace RevasumHardwareSimulator.mvp_Models.Behaviors
{

    [Serializable]
    public class Behavior_THRESHOLD : Behavior_Digital
    {

        // =======================================================================================================================

        #region Initialize

        // =======================================================================================================================

        public Behavior_THRESHOLD()
        {
            m_imageSource = "Threshold.jpg";
        }

        // --------------------------------------------------------------------------------------------------

        public Behavior_THRESHOLD(XMLNode node)
            : base(node)
        {
            m_imageSource = "Threshold.jpg";
            ThresholdValue = DoubleValueAtAttribute(node, "ThresholdValue", 0);
            ReverseActing = BoolValueAtAttribute(node, "ReverseActing");
            InputName = node.NodeAt("Inputs").NodeAt("Input").AttributeAt("Name");
        }

        // --------------------------------------------------------------------------------------------------

        public override void RestoreInputs(List<IOPoint> points)
        {
            if (InputName != "")
            {
                IOPoint point = FindPointByName(InputName, points);
                Input = point as IAnalogPoint;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Properties

        // =======================================================================================================================

        public override string Description
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Set digital point based on analog value\n");
                sb.Append("Input at or above threshold\n");
                sb.Append("\tTurn target point on\n");
                sb.Append("Input below threshold\n");
                sb.Append("\tTurn target point off\n");
                sb.Append("Valid Input:\n");
                sb.Append("   Must be Analog");
                return sb.ToString();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override string Function
        {
            get { return "THRESHOLD"; }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Input

        // =======================================================================================================================

        protected string m_inputName;
        public string InputName
        {
            get { return m_inputName; }
            set { m_inputName = value; }
        }

        // --------------------------------------------------------------------------------------------------

        [NonSerialized]
        protected IAnalogPoint m_input;
        public IAnalogPoint Input
        {
            get { return m_input; }
            set
            {
                m_input = value;
                if (m_input != null)
                    InputName = value.QualifiedName;
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Specific Properties

        // =======================================================================================================================

        private bool m_reverseActing = false;
        public bool ReverseActing
        {
            get { return m_reverseActing; }
            set
            {
                m_reverseActing = value;
                OnPropertyChanged("ReverseActing");
            }
        }

        // --------------------------------------------------------------------------------------------------

        private double m_thresholdValue;
        public double ThresholdValue
        {
            get { return m_thresholdValue; }
            set
            {
                m_thresholdValue = value;
                OnPropertyChanged("ThresholdValue");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region Operation

        // =======================================================================================================================

        public override void OnLine()
        {
            base.OnLine();
            IOPoint point = m_input as IOPoint;
            if (point != null)
            { 
                point.AddDependent(this);
                PerformOnLineUpdate();
            }
        }

        // --------------------------------------------------------------------------------------------------

        public override void PerformOnLineUpdate()
        {
            InputValue = Input.AnalogValue;
            bool overThreshold = Input.AnalogValue > ThresholdValue;
            bool signalOver = overThreshold ^ ReverseActing;
            SetTarget(signalOver);
        }

        // --------------------------------------------------------------------------------------------------

        double m_inputValue;
        public double InputValue
        {
            get { return m_inputValue; }
            set
            {
                m_inputValue = value;
                OnPropertyChanged("InputValue");
            }
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

        #region XML

        // =======================================================================================================================

        public override void SaveSpecificsAsXML(System.IO.StreamWriter writer, int tab)
        {
            base.SaveSpecificsAsXML(writer, tab);
            SaveAttribute(writer, tab, "ThresholdValue", ThresholdValue.ToString());
            if (ReverseActing)
                SaveAttribute(writer, tab, "ReverseActing", "true");
        }

        // --------------------------------------------------------------------------------------------------

        public override void SaveBehaviorInputs(System.IO.StreamWriter writer, int tab)
        {
            SaveInput(writer, tab, Input);
        }

        // =======================================================================================================================

        #endregion

        // =======================================================================================================================

    }
}
