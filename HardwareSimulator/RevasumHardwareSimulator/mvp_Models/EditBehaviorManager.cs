﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using RevasumHardwareSimulator.Model;
using RevasumHardwareSimulator.Global;
using System.Windows.Controls;
using RevasumHardwareSimulator.mvp_Presenters;
using RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters;
using RevasumHardwareSimulator.mvp_Models.Behaviors;
using RevasumHardwareSimulator.mvp_Models.IOPoints;


namespace RevasumHardwareSimulator.mvp_Models
{

    /// <summary>
    /// Given a description of a particular behavior, BehaviorManager using reflections, instantiates the behavior, its view, and its presenter
    /// </summary>
    public class EditBehaviorManager
    {

        // ================================================================================================

        #region Constants

        // --------------------------------------------------------------------------------------------------

        static readonly string Name_DigitalAnd = "digital points AND";
        static readonly string Name_DigitalOr = "digital points OR";
        static readonly string Name_DigitalNot = "digital point NOT";
        static readonly string Name_LinearFunction = "analog point LINEAR";
        static readonly string Name_AnalogThreshold = "analog THRESHOLD";
        static readonly string Name_AxisStructure = "AXIS structure from PLC";
        static readonly string Name_AnalogPIControl = "analog point PI Control";
        static readonly string Name_Script = "script control";

        static readonly string BehaviorNamespace = "RevasumHardwareSimulator.mvp_Models.Behaviors.";
        static readonly string ViewNamespace = "RevasumHardwareSimulator.mvp_Views.EditBehaviorViews.";
        static readonly string PresenterNamespace = "RevasumHardwareSimulator.mvp_Presenters.EditBehaviorPresenters.";

        // --------------------------------------------------------------------------------------------------

        #endregion

        // ================================================================================================

        #region Construction

        // ================================================================================================

        static List<EditBehaviorDescription> s_mvpNames;

        // --------------------------------------------------------------------------------------------------

        static EditBehaviorManager()
        {

            s_mvpNames = new List<EditBehaviorDescription>();

            // for each of the points below, enter:
            //      1. the text that should appear in the combo box (asking for behavior)
            //      2. the name of the Behavior class that should be instantiated based on the selection (must be a subclass of Behavior)
            //      3. the name of view class representing the behavior (must be a subclass of UserControl)
            //      4. the name of presenter that should serve the view, based on the behavior data (must be a subclass of EditBehaviorPresenter)

            // See below for the Namespaces where the classes are expected to be found


            s_mvpNames.Add(new EditBehaviorDescription(
                Name_DigitalAnd,
                "Behavior_AND",
                "BehaviorView_MultipleDigitals",
                "EditMultipleBehaviorPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
               Name_DigitalOr,
               "Behavior_OR",
               "BehaviorView_MultipleDigitals",
               "EditMultipleBehaviorPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
               Name_DigitalNot,
               "Behavior_NOT",
               "BehaviorView_SingleDigital",
               "EditNotBehaviorPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
               Name_LinearFunction,
               "Behavior_LINEAR",
               "BehaviorView_Linear",
               "EditLinearBehaviorPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
               Name_AnalogThreshold,
               "Behavior_THRESHOLD",
               "BehaviorViiew_Threshold",
               "EditThresholdPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
               Name_AxisStructure,
               "Behavior_AXIS",
               "BehaviorView_Axis",
               "EditAxisPresenter"));

            s_mvpNames.Add(new EditBehaviorDescription(
                Name_AnalogPIControl,
                "Behavior_PI_Control",
                "BehaviorView_PI_Control",
                "EditPIControlBehaviorPresenter"
                ));

            s_mvpNames.Add(new EditBehaviorDescription(
                Name_Script,
                "Behavior_Script",
                "BehaviorView_Script",
                "EditScriptBehaviorPresenter"
                ));
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Options

        // --------------------------------------------------------------------------------------------------

        public static List<string> AllBehaviorNamesSupportingTarget(IOPoint aPoint)
        {
            switch (aPoint.RevasumDataType)
            {
                case RevasumDataTypes.DigitalInput:
                    return AllNamesSupportingDigitalTarget();
                case RevasumDataTypes.AnalogInput:
                    return AllNamesSupportingAnalogTarget();
                case RevasumDataTypes.MotorStructureFromMC:
                    return AllNamesSupportingAxisStructureTarget();
                    
            }

            return new List<string>();
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        ///  what behaviors should be available when the user have chosen a digital input?
        /// </summary>
        /// <returns></returns>
        private static List<string> AllNamesSupportingDigitalTarget()
        {
            return new List<string>() { 
                Name_DigitalAnd, 
                Name_DigitalOr, 
                Name_DigitalNot,
                Name_AnalogThreshold,
                Name_Script
            };
        }

        private static List<string> AllNamesSupportingAnalogTarget()
        {
            return new List<string>() { 
                Name_LinearFunction,
                Name_AnalogPIControl,
                Name_Script
            };
        }

        private static List<string> AllNamesSupportingAxisStructureTarget()
        {
            return new List<string>() { 
                Name_AxisStructure
            };
        }

        // --------------------------------------------------------------------------------------------------

        #endregion

        // ================================================================================================

        #region Get Triplets

        // ================================================================================================

        /// <summary>
        /// retrieve the tripple - behavior + view + presenter, based on user selection
        /// </summary>
        public static EditBehaviorPresenter GetPresenter(string descriptionName, IOPoint target)
        {
            EditBehaviorDescription triplet = s_mvpNames.Find(x => x.Description == descriptionName);
            Behavior behavior = GetBehavior(triplet.Model);
            behavior.Target = target;
            UserControl view = GetView(triplet.View);
            EditBehaviorPresenter editPresenter = GetPresenter(triplet.Presenter);
            editPresenter.Behavior = behavior;
            editPresenter.View = view;
            return editPresenter;
        }

        // --------------------------------------------------------------------------------------------------

        /// <summary>
        /// retrieve the tripple - behavior + view + presenter, based on already created behavior
        /// </summary>
        public static EditBehaviorPresenter Activatepresenter(IOPoint target)
        {
            Behavior aBehavior = target.Behavior;
            string behaviorClass = aBehavior.GetType().Name;
            EditBehaviorDescription triplet = s_mvpNames.Find(x => x.Model == behaviorClass);
            UserControl view = GetView(triplet.View);
            EditBehaviorPresenter editPresenter = GetPresenter(triplet.Presenter);
            editPresenter.Behavior = aBehavior;
            editPresenter.View = view;
            return editPresenter;
        }

        // --------------------------------------------------------------------------------------------------

        static Behavior GetBehavior(string behaviorName)
        {
            var handle = Activator.CreateInstance(null, BehaviorNamespace + behaviorName);
            Behavior behavior = (Behavior)handle.Unwrap();
            return behavior;
        }

        // --------------------------------------------------------------------------------------------------

        static UserControl GetView(string viewName)
        {
            var handle = Activator.CreateInstance(null, ViewNamespace + viewName);
            UserControl view = (UserControl)handle.Unwrap();
            return view;
        }

        // --------------------------------------------------------------------------------------------------

        static EditBehaviorPresenter GetPresenter(string presenterName)
        {
            var handle = Activator.CreateInstance(null, PresenterNamespace + presenterName);
            EditBehaviorPresenter presenter = (EditBehaviorPresenter)handle.Unwrap();
            return presenter;
        }

        // ================================================================================================

        #endregion

        // ================================================================================================


    }
}
