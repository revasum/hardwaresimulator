﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Win32;
using System.IO;

using XMLSupport;
using RevasumHardwareSimulator.Global;
using RevasumHardwareSimulator.mvp_Models.IOPoints;
using RevasumHardwareSimulator.mvp_Models.Behaviors;

namespace RevasumHardwareSimulator.mvp_Models
{
    public class XMLConfiguration
    {

        // ================================================================================================

        #region Saving

        // ================================================================================================

        public static void Save(IOConfiguration configuration)
        {
            SaveFileDialog dlg = new SaveFileDialog()
            {
                Filter = "XML Files | *.xml",
                InitialDirectory = FileManager.XMLConfigurationFilesFolderName
            };

            if ((bool)dlg.ShowDialog())
                Save(dlg.FileName, configuration);
        }

        // --------------------------------------------------------------------------------------

        public static void Save(string file, IOConfiguration configuration)
        {
            StreamWriter writer = new StreamWriter(file);
            SaveTagAndAttribute(writer, 0, "<IOConfiguration", "File", Path.GetFileNameWithoutExtension(file));
            WriteConfigurationData(1, writer, configuration);
            SaveName(writer, 1, ">");
            List<Behavior> behaviors = new List<Behavior>();
            SavePoints(1, writer, configuration, behaviors);
            SaveBehaviors(1, writer, behaviors);
            SaveName(writer, 0, "</IOConfiguration>");
            writer.Close();
        }

        // --------------------------------------------------------------------------------------

        static void WriteConfigurationData(int tabs, StreamWriter writer, IOConfiguration configuration)
        {
            SaveAttribute(writer, tabs, "CreationTime", configuration.CreationTIme.ToString("yyyy-MM-dd HH:mm:ss"));
            SaveAttribute(writer, tabs, "LastModified", configuration.ModifyTime.ToString("yyyy-MM-dd HH:mm:ss"));
            string comment = configuration.PLCComment;
            if (comment != "")
            {
                // sorry guys.  I cannot have it in XML
                comment = comment.Replace("<", "");
                comment = comment.Replace(">", "");
                SaveAttribute(writer, tabs, "PLCComment", comment);
            }
            comment = configuration.ConfigurationComment;
            if (comment == null)
                comment = "";
            
            // sorry guys.  I cannot have it in XML
            comment = comment.Replace("<", "");
            comment = comment.Replace(">", "");
            SaveAttribute(writer, tabs, "ConfigurationComment", comment);
            
        }

        // --------------------------------------------------------------------------------------

        static void SavePoints(int tab, StreamWriter writer, IOConfiguration configuration, List<Behavior> behaviors)
        {
            SaveName(writer, tab, "<IOPoints>");
            SavePointComments(tab, writer);
            foreach (IOPoint point in configuration.Points)
            {
                Behavior behavior = SavePoint(tab + 1, writer, point);
                if (behavior != null)
                    behaviors.Add(behavior);
            }
            SaveName(writer, tab, "</IOPoints>");

        }

        // --------------------------------------------------------------------------------------

        static void SavePointComments(int tab, StreamWriter writer)
        {
            SaveName(writer, tab+1, "<!--");
            SaveName(writer, tab+2, "In order to shorten this document, the following data was not saved:");
            SaveName(writer, tab + 3, "Point.RevasumDataType = DigitalInput implies");
            SaveName(writer, tab + 4, "ADSType = BYTE");
            SaveName(writer, tab + 4, "IsDigital = true");
            SaveName(writer, tab + 4, "IsInput = true");
            SaveName(writer, tab + 4, "BeckhoffSize = 1");
            SaveName(writer, tab + 4, "BeckhoffDataType = ADST_UINT8");
            SaveName(writer, tab + 3, "Point.RevasumDataType = DigitalOutput implies");
            SaveName(writer, tab + 4, "IsDigital = true");
            SaveName(writer, tab + 4, "IsOutput = true");
            SaveName(writer, tab + 4, "ADSType = BYTE");
            SaveName(writer, tab + 4, "BeckhoffSize = 1");
            SaveName(writer, tab + 4, "BeckhoffDataType = ADST_UINT8");
            SaveName(writer, tab + 3, "Point.RevasumDataType = AnalogInput implies");
            SaveName(writer, tab + 4, "IsAnalog = true");
            SaveName(writer, tab + 4, "IsInput = true");
            SaveName(writer, tab + 4, "BeckhoffSize = 2");
            SaveName(writer, tab + 4, "(types are not implied: could be signed or unsigned)");
            SaveName(writer, tab + 3, "Point.RevasumDataType = MotorStructureToMC implies");
            SaveName(writer, tab + 4, "IsOutput = true");
            SaveName(writer, tab + 4, "ADSType = ST_AxisParametersForSimu");
            SaveName(writer, tab + 4, "BeckhoffSize = 40");
            SaveName(writer, tab + 4, "BeckhoffDataType = ADST_BIGTYPE");
            SaveName(writer, tab + 3, "Point.RevasumDataType = MotorStructureFromMC implies");
            SaveName(writer, tab + 4, "IsInput = true");
            SaveName(writer, tab + 4, "ADSType = MC.NCTOPLC_AXIS_REF");
            SaveName(writer, tab + 4, "BeckhoffSize = 256");
            SaveName(writer, tab + 4, "BeckhoffDataType = ADST_BIGTYPE");
            SaveName(writer, tab + 3, "Point.RevasumDataType = EncoderInput implies");
            SaveName(writer, tab + 4, "IsInput = true");
            SaveName(writer, tab + 4, "IsDoubleInteger = true");
            SaveName(writer, tab + 4, "IsEncoderInput = true");
            SaveName(writer, tab + 4, "BeckhoffSize = 4");
            SaveName(writer, tab + 4, "BeckhoffDataType = ADST_INT32");
            SaveName(writer, tab + 4, "ADSType = DINT");
            SaveName(writer, tab + 3, "Point.RevasumDataType = AxisPosition implies");
            SaveName(writer, tab + 4, "IsInput = true");
            SaveName(writer, tab + 4, "IsAnalog = true");



            SaveName(writer, tab+1, "-->");


        }

        // --------------------------------------------------------------------------------------

        static Behavior SavePoint(int tab, StreamWriter writer, IOPoint point)
        {
            // DID NOT SAVE THE FOLLOWING

            // Name - extract from QualifiedName
            // IsOriginalPoint - all are original except for IOPoint_AxisPosition
            // ADSModuleName - extract from QualifiedName
            // set IsSelected to false for all

            Behavior returnValue = null;
            SaveTagAndAttribute(writer, tab, "<Point", "QualifiedName", point.QualifiedName);
            SaveAttribute(writer, tab + 1, "RevasumDataType", point.RevasumDataType.ToString());
            if (point.RevasumDataType == RevasumDataTypes.EncoderInput)
            {

            }

            if (point.RevasumDataType != RevasumDataTypes.AnalogInput)
                ConditionalSave(writer, tab + 1, point.IsAnalog, "IsAnalog");

            ConditionalSave(writer, tab + 1, point.IsAxisCommandStructure, "IsAxisCommandStructure");
            ConditionalSave(writer, tab + 1, point.IsOutputMotorControlStructure, "IsOutputMotorControlStructure");
            ConditionalSave(writer, tab + 1, point.IsInputMotorControlStructure, "IsInputMotorControlStructure");
            ConditionalSave(writer, tab + 1, point.IsDoubleInteger, "IsDoubleInteger");
            ConditionalSave(writer, tab + 1, point.IsEncoderInput, "IsEncoderInput");
            if (point.HasDefault)
            {
                SaveAttribute(writer, tab + 1, "InitialValue", point.InitialValueAsString);
            }
            ConditionalSave(writer, tab + 1, point.HasDefault, "DefaultValueDefined");

            //ConditionalSave(writer, tab + 1, point.HasDefault, "DefaultValue", point.InitialValueAsString);

            if (point.BehaviorAdded)
            {
                SaveAttribute(writer, tab + 1, "HasBehavior", "true");
                returnValue = point.Behavior;
            }

            if (point.RevasumDataType != RevasumDataTypes.AxisPosition)
            {
                SaveAttribute(writer, tab + 1, "ADSFullName", point.ADSFullName);
                ConditionalSave(writer, tab + 1, point.ADSNameDescription != "", "ADSNameDescription", point.ADSNameDescription);
                ConditionalSave(writer, tab + 1, point.ADSComment != "", "ADSComment", point.ADSComment);
                if (point.RevasumDataType != RevasumDataTypes.DigitalInput &&
                    point.RevasumDataType != RevasumDataTypes.DigitalOutput &&
                    point.RevasumDataType != RevasumDataTypes.MotorStructureToMC &&
                    point.RevasumDataType != RevasumDataTypes.MotorStructureFromMC)
                {
                    SaveAttribute(writer, tab + 1, "BeckhoffDataType", point.BeckhoffDataType);
                    SaveAttribute(writer, tab + 1, "AdsType", point.ADSType);
                    SaveAttribute(writer, tab + 1, "BeckhoffSize", point.BeckhoffSize.ToString());
                }
            }
            SaveAttribute(writer, tab + 1, "Position", point.Position.ToString(), " />");

            return returnValue;
        }

        // --------------------------------------------------------------------------------------

        static void SaveBehaviors(int tab, StreamWriter writer, List<Behavior> Behaviors)
        {
            SaveName(writer, tab, "<Behaviors>");

            foreach (Behavior behavior in Behaviors)
            {
                SaveBehavior(tab + 1, writer, behavior);
            }
            SaveName(writer, tab, "</Behaviors>");

        }
        // --------------------------------------------------------------------------------------

        static void SaveBehavior(int tab, StreamWriter writer, Behavior behavior)
        {
            // Name - assemble from Function + Target.QualifiedName
            SaveTagAndAttribute(writer, tab, "<Behavior", "Target", behavior.Target.QualifiedName);
            ConditionalSave(writer, tab + 1, behavior.IsDigital, "IsDigital");
            ConditionalSave(writer, tab + 1, behavior.IsAnalog, "IsAnalog");
            behavior.SaveSpecificsAsXML(writer, tab + 1);
            SaveAttribute(writer, tab + 1, "Function", behavior.Function, " >");
            SaveBehaviorInputs(tab + 1, writer, behavior);
            SaveName(writer, tab, "</Behavior>");
        }

        // --------------------------------------------------------------------------------------

        static void SaveBehaviorInputs(int tab, StreamWriter writer, Behavior behavior)
        {
            SaveName(writer, tab, "<Inputs>");
            behavior.SaveBehaviorInputs(writer, tab + 1);
            SaveName(writer, tab, "</Inputs>");
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Utilities

        // ================================================================================================

        static void Tab(StreamWriter writer, int tabs)
        {
            for (int i = 0; i < tabs; i++)
                writer.Write("  ");
        }

        // --------------------------------------------------------------------------------------

        static void SaveAttribute(StreamWriter writer, int tabs, string atrName, string atrValue, string finish = "")
        {
            Tab(writer, tabs);
            writer.WriteLine(atrName + "=\"" + atrValue + "\"" + finish);
        }

        // --------------------------------------------------------------------------------------

        static void SaveTagAndAttribute(StreamWriter writer, int tabs, string tagName, string atrName, string atrValue, string finish = "")
        {
            Tab(writer, tabs);
            writer.WriteLine(tagName + " " + atrName + "=\"" + atrValue + "\"" + finish);
        }

        // --------------------------------------------------------------------------------------

        static void SaveName(StreamWriter writer, int tabs, string name)
        {
            Tab(writer, tabs);
            writer.WriteLine(name);
        }

        // --------------------------------------------------------------------------------------

        static void ShowAttribute(XMLNode node, string attributeName)
        {
            Console.WriteLine("Attribute at " + attributeName + " = " + node.AttributeAt(attributeName));
        }

        // --------------------------------------------------------------------------------------

        static void ConditionalSave(StreamWriter writer, int tab, bool condition, string text, string aValue="true")
        {
            if (condition)
                SaveAttribute(writer, tab, text, aValue);
        }

        // ================================================================================================

        #endregion

        // ================================================================================================

        #region Loading

        // ================================================================================================

        public static void Load()
        {
            OpenFileDialog dlg = new OpenFileDialog()
            {
                Filter = "XML Files | *.xml",
                InitialDirectory = FileManager.XMLConfigurationFilesFolderName
            };

            if ((bool)dlg.ShowDialog())
            {
                XMLNode node = XMLNode.FromFile(dlg.FileName);
                IOConfiguration config = new IOConfiguration(node);
                config.ProjectName = dlg.FileName;
                ConfigurationManager.Current.CurrentConfiguration = config;
            }
        }


        // ================================================================================================

        #endregion

        // ================================================================================================

    }
}
